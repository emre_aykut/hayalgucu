<?php 

function text_limitation($val, $limit = 10)
{
    if (strlen($val) > $limit)
    {
        if (function_exists("mb_substr")) $val = mb_substr($val, 0, $limit, "UTF-8").'...';
        else $val = substr($val, 0, $limit).'...';
    }
    return $val;
}

function number_limitation($val, $limit = 10)
{
    if (strlen($val) > $limit)
    {
        if (function_exists("mb_substr")) $val = mb_substr($val, 0, $limit, "UTF-8");
        else $val = substr($val, 0, $limit);
    }
    return $val;
}

function general_date_format($val)
{
    return date('d-m-Y', strtotime($val));
}

function general_datetime_format($val)
{
    return date('d.m.Y H:i', strtotime($val));
}

?>