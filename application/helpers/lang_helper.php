<?php 

function l($key)
{
	$ci =& get_instance();
	$dil = $ci->session->userdata("lang_code");


	if(!$dil)
	{
		$dil = "tr";
	}


	if($dil == "tr")
	{
		$lang['test'] = 'Test';

	}
	elseif($dil == "en")
	{
		$lang['test'] = 'Test';
	}


	return $lang[$key];
}

function session_login_username()
{
	$ci =& get_instance();
	$session_admin = $ci->session->userdata("admin");

	return $session_admin['fullname'];
}

?>