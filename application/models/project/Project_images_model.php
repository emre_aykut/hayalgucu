<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project_images_model extends CI_Model {

	var $table = 'project_images';

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function delete($project_content_id)
	{
		$this->db->where('project_content_id', $project_content_id);
		$this->db->delete($this->table);
	}

	public function img_clear($project_id)
	{
		$this->db->where('project_id', $project_id);
		$this->db->delete($this->table);
	}

	public function get_project_image($project_id)
	{
		$this->db->order_by('lang_id','asc');
		$this->db->where('project_id', $project_id);
		$query = $this->db->get($this->table);
		
		if ($query->num_rows() > 0) 
		{
			return $query->result_array();
		}
	}

	public function get_by_project_content_id($project_content_id)
	{
		$this->db->where('project_content_id', $project_content_id);
		$query = $this->db->get($this->table);
		
		if ($query->num_rows() > 0) 
		{
			return $query->result_array();
		}
	}

	public function get_all($project_content_id)
	{
		$this->db->where('lang_id', $this->lang_id);
		$this->db->where('project_content_id', $project_content_id);
		$query = $this->db->get($this->table);
		
		if ($query->num_rows() > 0) 
		{
			return $query->result_array();
		}	
	}


}