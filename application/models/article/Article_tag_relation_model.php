<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article_tag_relation_model extends CI_Model {

	var $table = 'article_tag_relations';
	var $tag_table = 'article_tags';

	public function __construct()
	{
		parent::__construct();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_tags($article_id)
	{
		$this->db->from($this->table);
		$this->db->order_by('id', 'desc');
		$this->db->where('article_id',$article_id);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function delete_by_id($article_id)
	{
		$this->db->where('article_id', $article_id);
		$this->db->delete($this->table);
	}

	public function delete_by_content_id($content_id)
	{
		$this->db->where('article_content_id', $content_id);
		$this->db->delete($this->table);
	}

	public function get_tag($article_content_id)
	{	
		$this->db->from($this->table);
		$this->db->order_by($this->table.'.id', 'asc');
		$this->db->where('article_content_id', $article_content_id);
		$this->db->where($this->table.'.lang_id', $this->lang_id);
		$this->db->join($this->tag_table, $this->tag_table.'.id = '.$this->table.'.tag_id');
		$query = $this->db->get();

		if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
	}

}
