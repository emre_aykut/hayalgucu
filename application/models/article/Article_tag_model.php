<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article_tag_model extends CI_Model {

	var $table = 'article_tags';

	public function __construct()
	{
		parent::__construct();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function check_tag($title, $lang_id)
	{
		$this->db->select('id');
		$this->db->where('title', $title);
		$this->db->where('lang_id', $lang_id);
		$query = $this->db->get($this->table);

		if($query->num_rows() > 0)
        {
            return $query->row();
        }else{
            return false;
        }
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_all()
	{	
		$this->db->order_by('title', 'asc');
		$this->db->where('lang_id', $this->lang_id);
		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}


}
