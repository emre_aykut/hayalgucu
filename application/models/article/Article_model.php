<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article_model extends CI_Model {

	var $table = 'articles';
	var $article_authors_table = 'article_authors';
	var $article_categories_table = 'article_categories';
	var $article_tag_relations = 'article_tag_relations';
	var $column_order = array('title', 'author_id', 'publish_date', 'status', null);
	var $column_search = array('title', 'author_id', 'publish_date', 'status'); 
	var $order = array('id' => 'desc'); 

	public function __construct()
	{
		parent::__construct();
	}

	private function _get_datatables_query()
	{
		
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) 
					$this->db->group_end(); 
			}
			$i++;
		}
		
		if(isset($_POST['order'])) 
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function get_datatables($lang_id)
	{	
		$author_query = '(select '. $this->article_authors_table .'.fullname from '. $this->article_authors_table .' where '. $this->article_authors_table .'.content_id = ' . $this->table . '.author_id and '. $this->article_authors_table .'.lang_id = ' . $lang_id . ') as author';
		$category_query = '(select '. $this->article_categories_table .'.title from '. $this->article_categories_table .' where '. $this->article_categories_table .'.content_id = ' . $this->table . '.category_id and '. $this->article_categories_table .'.lang_id = ' . $lang_id . ') as category';

		$this->db->select($this->table . '.*, ' . $author_query . ', ' . $category_query);
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->where('lang_id', $lang_id);
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function count_filtered($lang_id)
	{
		$this->_get_datatables_query();
		$this->db->where('lang_id', $lang_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($lang_id)
	{
		$this->db->from($this->table);
		$this->db->where('lang_id', $lang_id);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($content_id)
	{
		$this->db->where('content_id', $content_id);
		$this->db->delete($this->table);
	}

	public function get_by_content_id($content_id)
	{	
		$this->db->select('id');
		$this->db->from($this->table);
		$this->db->where('content_id',$content_id);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_all()
	{
		$author_fullname = '(select '. $this->article_authors_table .'.fullname from '. $this->article_authors_table .' where '. $this->article_authors_table .'.content_id = ' . $this->table . '.author_id and '. $this->article_authors_table .'.lang_id = ' . $this->lang_id . ') as author_fullname';
		$author_title = '(select '. $this->article_authors_table .'.title from '. $this->article_authors_table .' where '. $this->article_authors_table .'.content_id = ' . $this->table . '.author_id and '. $this->article_authors_table .'.lang_id = ' . $this->lang_id . ') as author_title';

		$this->db->select($this->table . '.*, ' . $author_fullname . ', ' . $author_title);
		$this->db->where('lang_id', $this->lang_id);
		$this->db->order_by('publish_date', 'desc');
		$query = $this->db->get($this->table);

		if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
	}

	public function get_detail($id, $public)
	{
		$author_fullname = '(select '. $this->article_authors_table .'.fullname from '. $this->article_authors_table .' where '. $this->article_authors_table .'.content_id = ' . $this->table . '.author_id and '. $this->article_authors_table .'.lang_id = ' . $this->lang_id . ') as author_fullname';
		$author_title = '(select '. $this->article_authors_table .'.title from '. $this->article_authors_table .' where '. $this->article_authors_table .'.content_id = ' . $this->table . '.author_id and '. $this->article_authors_table .'.lang_id = ' . $this->lang_id . ') as author_title';

		$this->db->select($this->table . '.*, ' . $author_fullname . ', ' . $author_title);
		$this->db->where('lang_id', $this->lang_id);
		$this->db->where('content_id', $id);

		if ($public != '0') {
			$this->db->where('status', 1);
		}
		
		$query = $this->db->get($this->table);

		if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
	}

	public function get_category_articles($category_id)
	{
		$author_query = '(select '. $this->article_authors_table .'.fullname from '. $this->article_authors_table .' where '. $this->article_authors_table .'.content_id = ' . $this->table . '.author_id and '. $this->article_authors_table .'.lang_id = ' . $this->lang_id . ') as author';
		$category_query = '(select '. $this->article_categories_table .'.title from '. $this->article_categories_table .' where '. $this->article_categories_table .'.content_id = ' . $this->table . '.category_id and '. $this->article_categories_table .'.lang_id = ' . $this->lang_id . ') as category';

		$this->db->select($this->table . '.*, ' . $author_query . ', ' . $category_query);
		$this->db->where('lang_id', $this->lang_id);
		$this->db->where('category_id', $category_id);
		$this->db->order_by('publish_date', 'desc');
		$query = $this->db->get($this->table);

		if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
	}

	public function get_author_articles($author_id)
	{
		$author_query = '(select '. $this->article_authors_table .'.fullname from '. $this->article_authors_table .' where '. $this->article_authors_table .'.content_id = ' . $this->table . '.author_id and '. $this->article_authors_table .'.lang_id = ' . $this->lang_id . ') as author';
		$category_query = '(select '. $this->article_categories_table .'.title from '. $this->article_categories_table .' where '. $this->article_categories_table .'.content_id = ' . $this->table . '.category_id and '. $this->article_categories_table .'.lang_id = ' . $this->lang_id . ') as category';

		$this->db->select($this->table . '.*, ' . $author_query . ', ' . $category_query);
		$this->db->where('lang_id', $this->lang_id);
		$this->db->where('author_id', $author_id);
		$this->db->order_by('publish_date', 'desc');
		$query = $this->db->get($this->table);

		if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
	}

	public function get_tag_articles($tag_id)
	{
		$author_query = '(select '. $this->article_authors_table .'.fullname from '. $this->article_authors_table .' where '. $this->article_authors_table .'.content_id = ' . $this->table . '.author_id and '. $this->article_authors_table .'.lang_id = ' . $this->lang_id . ') as author';
		$category_query = '(select '. $this->article_categories_table .'.title from '. $this->article_categories_table .' where '. $this->article_categories_table .'.content_id = ' . $this->table . '.category_id and '. $this->article_categories_table .'.lang_id = ' . $this->lang_id . ') as category';

		$this->db->select($this->table . '.*, ' . $author_query . ', ' . $category_query);
		$this->db->where($this->table . '.lang_id', $this->lang_id);
		$this->db->where($this->article_tag_relations . '.tag_id', $tag_id);
		$this->db->join($this->article_tag_relations, $this->article_tag_relations.'.article_id = '.$this->table.'.id');
		$this->db->order_by('publish_date', 'desc');
		$query = $this->db->get($this->table);

		if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
	}

	public function get_total_rows()
    {
        $query = $this->db->where('status', 1)->get($this->table);

        return $query->num_rows();
    }

    public function get_list_limit($limit, $offset)
    {
        $author_fullname = '(select '. $this->article_authors_table .'.fullname from '. $this->article_authors_table .' where '. $this->article_authors_table .'.content_id = ' . $this->table . '.author_id and '. $this->article_authors_table .'.lang_id = ' . $this->lang_id . ') as author_fullname';
		$author_title = '(select '. $this->article_authors_table .'.title from '. $this->article_authors_table .' where '. $this->article_authors_table .'.content_id = ' . $this->table . '.author_id and '. $this->article_authors_table .'.lang_id = ' . $this->lang_id . ') as author_title';

		$this->db->select($this->table . '.*, ' . $author_fullname . ', ' . $author_title);
		$this->db->where('lang_id', $this->lang_id);
		$this->db->where('status', 1);
		$this->db->order_by('publish_date', 'desc');
        $this->db->limit($limit, $offset);
		$query = $this->db->get($this->table);

		if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }

    }

}
