<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
	}

	public function get_lang_id()
	{
		$lang = $this->session->userdata("lang");

		if(!$lang)
		{
			$this->load->model('language/language_model', 'language');
			$default_lang = $this->language->get_lang_default();
			return $default_lang->id;
		}
		else
		{
			return $this->session->userdata("lang");
		}
	}

	public function autorun()
	{	
		// $default_pages = $this->db->where('lang_id', $this->get_lang_id())->where('default_page', 1)->where('status', 1)->where('parent_id !=', 0)->order_by('sort','asc')->get('pages')->result_array();
		// $default_language = $this->db->select('id')->where('default_lang', 1)->get('languages')->row();
		// $languages = $this->session->tempdata('languages');

		// if (!$languages) 
		// {
		// 	$languages = $this->db->where('status', 1)->get('languages')->result_array();
		// 	$this->session->set_tempdata('languages', $languages, 86400);
		// }

		// define('LANGUAGES', serialize($languages));
		// define('DEFAULT_PAGES', serialize($default_pages));
		// define('DEFAULT_LANG_ID', $default_language->id);
	}

}





?>