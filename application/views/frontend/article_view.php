<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?=$page_detail->meta_description?>">
    <meta name="keywords" content="<?=$page_detail->meta_keywords?>">

    <title><?=$page_detail->meta_title?></title>

    <?php include('inc/head.php'); ?>

</head>

<body>
	
	<?php include('inc/nav_top.php'); ?>

	<section class="article-area">
		<div class="container">
			<div class="row">

				<div class="col-md-12">
					<div class="special-area">
						<div class="special-title">MAKALELER</div>
					</div>
				</div>

				<div class="col-md-12">
					<div class="articlepage-border-left"></div>
				</div>

				<div class="col-md-12">
					<div class="content">
						<div class="row">
							

							<div id="output"></div>

							<div class="col-md-12">
								<div class="post-loader-area">
					               <div class="loader">
					                  <div class="chevron"></div>
					                  <div class="chevron"></div>
					                  <div class="chevron"></div>
					                  <span class="text">Aşağı Kaydır</span>
					               </div>
					            </div>
							</div>

						</div>
					</div>
				</div>

				<div class="col-md-12">
					<div class="articlepage-border"></div>
				</div>
				
			</div>
		</div>
	</section>
  
	<?php include('inc/footer.php'); ?>
	<?php include('inc/script.php'); ?>

	<script type="text/javascript">

		$(window).load(function() {

			// articlepage-border-left (start)
            $('.articlepage-border-left').animate({
			    height: "570px"
		  	}, 2000);
		  	// articlepage-border-left (end)
		  	
        });

		$(document).ready(function(){

			load_more_article();

		});

		var header_height = 0;
		var article_height = 0;
 
		$(window).scroll(function()
		{
            // load more article run (start)
            if($(window).scrollTop() + 1 >= $(document).height() - $(window).height()) 
            {
               	var total_pages = parseInt($("#total_pages").val());
               	var page = parseInt($("#page").val())+1;
               	if(page <= total_pages)
               	{
                  load_more_article(page, total_pages);
               	} 
               	else
               	{
               		$('.loader').hide();
               		
		           	if ($(window).width() > 1420)
					{
						header_height = $('header').height();
						article_height = $('.article-area').height();

						// articlepage-border and footer-border (start)
						if($(window).scrollTop() >= header_height + article_height - 500) 
			            {
			            	$('.articlepage-border').animate({
							    width: "670px"
						  	}, 2000);

						  	$('.footer-border').animate({
							    height: "490px"
						  	}, 2000);
			            }
			            // articlepage-border and footer-border (end)
		            }
               	}
            }
            // load more article run (start)
		});

		function load_more_article(page, total_pages) 
		{    
    		$("#total_pages, #page").remove();

    		$.ajax({
	            url: base_url + 'article/ajax_articles/8',
	            type: "POST",
	            data: {page:page},
	            beforeSend: function(){
	               $(".loader").show();
	            },
	            complete: function(){
	               // $('.loader').hide();
	            },
	            success: function(data){
	               $("#output").append(data);
	            },
	            error: function(){
	               $(".loader").html("No data found!");
	            }
     		});
      	}

	</script>


</body>
	
</html>