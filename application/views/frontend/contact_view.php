<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?=$page_detail->meta_description?>">
    <meta name="keywords" content="<?=$page_detail->meta_keywords?>">

    <title><?=$page_detail->meta_title?></title>

    <?php include('inc/head.php'); ?>

</head>

<body>
	
	<?php include('inc/nav_top.php'); ?>

	<section class="contact-area">
		<div class="container">
			<div class="row">

				<div class="col-md-12">
					<div class="special-area">
						<div class="special-title">İLETİŞİM</div>
					</div>
				</div>

				<div class="col-md-12">
					<div class="contactpage-border-left"></div>
				</div>

				<div class="col-md-12">
					<div class="map-area animated bounceInRight">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3061.0841822992106!2d32.69097061539697!3d39.894747479429256!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14d338f4847913e7%3A0x7ad0ad840d6e9ad3!2s%C3%9Cmit+Mahallesi%2C+2546.+Sk.+No%3A21%2C+06810+Yenimahalle%2FAnkara!5e0!3m2!1str!2str!4v1551969820217" width="100%" height="240" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
					<div class="address animated bounceInRight">
						<div class="title"><?=$page_detail->description?></div>
						<div class="detail">
							<?=$page_detail->detail?>
						</div>		
					</div>
				</div>

				<div class="col-md-12">
					<div class="special-area">
						<div class="special-title">BİZİMLE İLETİŞİME GEÇİN</div>
					</div>
				</div>

				<form action="#" id="contact-form" class="form-horizontal">
					
					<div class="col-md-6 float-left">
						<div class="contact-us animated bounceInRight">
							<div class="special-form-group">
								<div class="special-label">İsim Soyisim</div>
								<input type="text" name="fullname" class="special-input" />
							</div>
							<div class="special-form-group">
								<div class="special-label">E-posta</div>
								<input type="email" name="email" class="special-input" />
							</div>
							<div class="special-form-group">
								<div class="special-label">Telefon</div>
								<input type="text" name="phone" class="special-input" />
							</div>
						</div>
					</div>

					<div class="col-md-6 float-left">
						<div class="contact-us animated bounceInRight">
							<div class="special-form-group">
								<div class="special-label">Mesajınız</div>
								<textarea name="message" class="special-textarea"></textarea>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="special-btn-area animated bounceInRight">
							<a href="javascript:void(0)" onclick="contact_form_save()" id="btn-send">Gönder</a>
						</div>
					</div>

				</form>

				<div class="col-md-12">
					<div class="contactpage-border"></div>
				</div>

			</div>
		</div>
	</section>
  
	<?php include('inc/footer.php'); ?>
	<?php include('inc/script.php'); ?>
	<script src="<?php echo base_url()?>assets/plugins/sweetalert/js/sweetalert.min.js"></script>

	<script type="text/javascript">

		$(window).load(function() {

			// contactpage-border-left (start)
            $('.contactpage-border-left').animate({
			    height: "500px"
		  	}, 2000);
		  	// contactpage-border-left (end)
		  	
        });

		$(document).ready(function(){

			var header_height = 0;
			var article_detail_height = 0;

			if ($(window).width() > 1420) {  
				$(window).scroll(function() {

					header_height = $('header').height();
					contact_height = $('.contact-area').height();

					// contactpage-border and footer-border (start)
					if($(window).scrollTop() >= header_height + contact_height - 800) 
		            {
		            	$('.contactpage-border').animate({
						    width: "670px"
					  	}, 2000);

					  	$('.footer-border').animate({
						    height: "490px"
					  	}, 2000);
		            }
		            // contactpage-border and footer-border (end)

				});
			}

		});

		function contact_form_save()
		{
		    $('#btn-send').text('Gönderiliyor...');
		    $('#btn-send').attr('disabled',true);

		    var warning_text = '';
		    var formData = new FormData($('#contact-form')[0]);
		    $.ajax({
		        url : base_url + 'contact/ajax_add',
		        type: "POST",
		        data: formData,
		        contentType: false,
		        processData: false,
		        dataType: "JSON",
		        success: function(data)
		        {
		            if(data.status) 
		            {
		            	$('[name="fullname"], [name="email"], [name="phone"], [name="message"]').val('');

		                swal({
							title: 'Başarılı',
							text: 'Form başarılı bir şekilde gönderildi.',
							icon: "success",
							button: 'Kapat',
						});
		            }
		            else
		            {
		                for (var i = 0; i < data.inputerror.length; i++) 
		                {
		                	warning_text += '-' + data.error_string[i] + '\n'; 
		                }

		                swal({
							title: 'Uyarı',
							text: warning_text,
							icon: "warning",
							button: 'Kapat',
						});
		            }

		            $('#btn-send').text('Gönder'); 
		            $('#btn-send').attr('disabled',false);
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            swal({
						title: 'Başarılı',
						text: 'Form gönderilirken bir hata oluştu.',
						icon: "error",
						button: 'Kapat',
					});

		            $('#btn-send').text('Gönder'); 
		            $('#btn-send').attr('disabled',false);

		        }
		    });
		}

	</script>

</body>
	
</html>