<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?=$page_detail->meta_description?>">
    <meta name="keywords" content="<?=$page_detail->meta_keywords?>">

    <title><?=$page_detail->meta_title?></title>

    <?php include('inc/head.php'); ?>

</head>

<body>
	
	<?php include('inc/nav_top.php'); ?>

	<section class="about-area">
		<div class="container">
			<div class="row">

				<div class="col-md-12">
					<div class="special-area">
						<div class="special-title"><?=$page_detail->title?></div>
					</div>
				</div>

				<div class="col-md-9">
					<div class="content animated bounceInLeft">
						<div class="top-area text-bold">
							<?=$page_detail->description?>
						</div>
						<div class="bottom-area text-thin">
							<?=$page_detail->detail?>
						</div>
					</div>
				</div>

				<div class="col-md-3">
					<div class="icon-area animated bounceInRight">
						<img src="<?=base_url()?><?=UPLOAD_FOLDER.$page_detail->photo?>" class="img-fluid mx-auto d-block">
					</div>
				</div>

			</div>
		</div>
	</section>
  
	<?php include('inc/footer.php'); ?>
	<?php include('inc/script.php'); ?>

	<script type="text/javascript">

		$(window).load(function() {

			// footer-border (start)
            $('.footer-border').animate({
			    height: "490px"
		  	}, 2000);
		  	// footer-border (end)
		  	
        });

	</script>

</body>
	
</html>