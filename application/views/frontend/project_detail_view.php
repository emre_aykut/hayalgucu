<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?=$project->meta_description?>">
    <meta name="keywords" content="<?=$project->meta_keywords?>">
    <title><?=$project->meta_title?></title>

    <?php include('inc/head.php'); ?>

</head>

<body>
	
	<?php include('inc/nav_top.php'); ?>

	<section class="project-detail-area">
		<div class="container">
			<div class="row">

				<div class="col-md-12">
					<div class="project-detailpage-border-left"></div>
				</div>

				<div class="col-lg-4 col-md-12 col-12">
					<div class="main-title animated bounceInLeft">
						<?=$project->title?>
					</div>
					<div class="clear"></div>
					<div class="content animated bounceInLeft">
						<div class="category-icon">
							<img src="<?=base_url()?><?=UPLOAD_FOLDER . $project->category_photo?>">
						</div>
						<div class="category-text">
							<?=$project->category_title?>
						</div>
						<div class="detail">
							<?=$project->detail?>
						</div>
					</div>
				</div>

				<div class="col-lg-8 col-md-12 col-12">

					<div id="project-detail-slider" class="carousel slide animated bounceInRight" data-ride="carousel">

						<div class="carousel-inner">

							<?php $i=0; ?>
						    <?php if ($project_images) { ?>
						    <?php foreach ($project_images as $project_image): ?>
						        
						        <?php if ($i == 0){ ?>
					          	
					          	<div class="carousel-item active">
						            <img src="<?=base_url() . UPLOAD_FOLDER . $project_image['image']?>" class="d-block w-100">
					          	</div> 

						        <?php }else{ ?>

				        		<div class="carousel-item">
						            <img src="<?=base_url() . UPLOAD_FOLDER . $project_image['image']?>" class="d-block w-100">
					          	</div>
						        
						        <?php } $i++; ?>

						    <?php endforeach ?>
						    <?php } ?>

						</div>

						<a class="carousel-control-prev" href="#project-detail-slider" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#project-detail-slider" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>

					</div>
					
				</div>

				<div class="col-md-12">
					<div class="project-nav animated bounceInLeft">
						<a href="">
							<img src="<?=base_url()?>assets/frontend/img/project-left.svg">
						</a>
						<a href="">
							<img src="<?=base_url()?>assets/frontend/img/project-nav.svg">
						</a>
						<a href="">
							<img src="<?=base_url()?>assets/frontend/img/project-right.svg">
						</a>
					</div>
				</div>

				<div class="col-md-12">
					<div class="project-detailpage-border"></div>
				</div>
				
			</div>
		</div>
	</section>
  
	<?php include('inc/footer.php'); ?>
	<?php include('inc/script.php'); ?>

	<script type="text/javascript">

		$(window).load(function() {

			// project-detailpage-border-left (start)
            $('.project-detailpage-border-left').animate({
			    height: "360px"
		  	}, 2000);
		  	// project-detailpage-border-left (end)

		  	// project-detailpage-border (start)
		  	$('.project-detailpage-border').animate({
			    width: "670px"
		  	}, 2000);
		  	// project-detailpage-border (end)

		  	// footer-border (start)
		  	$('.footer-border').animate({
			    height: "490px"
		  	}, 2000);
		  	// footer-border (end)

        });

	</script>

</body>
	
</html>