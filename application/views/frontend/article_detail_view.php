<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?=$article->description?>">
    <meta name="keywords" content="<?=$article_tags?>">
    <title><?=$article->title?></title>

    <?php include('inc/head.php'); ?>

</head>

<body>
	
	<?php include('inc/nav_top.php'); ?>

	<section class="article-detail-area">
		<div class="container">
			<div class="row">

				<div class="col-md-12">
					<div class="article-detailpage-border-left"></div>
				</div>

				<div class="col-md-6">
					<div class="main-title animated bounceInRight">
						<?=$article->title?>
					</div>
				</div>

				<div class="col-md-6 ">
					<div class="box animated bounceInRight">
						<div class="date"><?=general_date_format($article->publish_date)?></div>					
						<div class="author-area">
							<div class="icon-area">
								<i class="far fa-user author-icon"></i>
							</div>
							<div class="text-area">
								<div class="fullname"><?=$article->author_fullname?></div>
								<div class="sector"><?=$article->author_title?></div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-12">
					<div class="content animated bounceInRight">
						<div class="top-area text-bold">
							<?=$article->description?>
						</div>
						<div class="bottom-area text-thin">
							<?=$article->detail?>
						</div>
					</div>
				</div>

				<div class="col-md-12">
					<div class="article-detailpage-border"></div>
				</div>
				
			</div>
		</div>
	</section>
  
	<?php include('inc/footer.php'); ?>
	<?php include('inc/script.php'); ?>

	<script type="text/javascript">

		$(window).load(function() {

			// article-detailpage-border-left (start)
            $('.article-detailpage-border-left').animate({
			    height: "600px"
		  	}, 2000);
		  	// article-detailpage-border-left (end)
		  	
        });

		$(document).ready(function(){

			var header_height = 0;
			var article_detail_height = 0;

			if ($(window).width() > 1420) {  
				$(window).scroll(function() {

					header_height = $('header').height();
					article_detail_height = $('.article-detail-area').height();

					// article-detailpage-border and footer-border (start)
					if($(window).scrollTop() >= header_height + article_detail_height - 700) 
		            {
		            	$('.article-detailpage-border').animate({
						    width: "670px"
					  	}, 2000);

					  	$('.footer-border').animate({
						    height: "490px"
					  	}, 2000);
		            }
		            // article-detailpage-border and footer-border (end)

				});
			}

		});    

	</script>

</body>
	
</html>