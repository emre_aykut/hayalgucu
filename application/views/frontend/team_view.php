<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?=$page_detail->meta_description?>">
    <meta name="keywords" content="<?=$page_detail->meta_keywords?>">

    <title><?=$page_detail->meta_title?></title>

    <?php include('inc/head.php'); ?>

</head>

<body>
	
	<?php include('inc/nav_top.php'); ?>

	<section class="team-area">
		<div class="container">
			<div class="row">

				<div class="col-md-12">
					<div class="special-area">
						<div class="special-title"><?=$page_detail->title?></div>
					</div>
				</div>

				<div class="col-md-12">
					<div class="teampage-border-left"></div>
				</div>

				<div class="col-md-12">
					<div class="content">
						<div class="row">
							
							<?php if ($teams): ?>
							<?php $count = 0; ?>
							<?php foreach ($teams as $key => $value): ?>
							<div class="col-md-3">
								<div class="box animated fadeIn delay-<?=$count?>s">
									<div class="fullname hvr-grow"><?=$value['fullname']?></div>
									<div class="title hvr-grow"><?=$value['title']?></div>
								</div>
							</div>
							<?php $count += 2; ?>
							<?php endforeach ?>
							<?php endif ?>

						</div>
					</div>
				</div>

				<div class="col-md-12">
					<div class="teampage-border"></div>
				</div>

			</div>
		</div>
	</section>
  
	<?php include('inc/footer.php'); ?>
	<?php include('inc/script.php'); ?>

	<script type="text/javascript">

		$(window).load(function() {

			// teampage-border-left (start)
            $('.teampage-border-left').animate({
			    height: "380px"
		  	}, 2000);
		  	// teampage-border-left (end)

		  	// teampage-border (start)
		  	$('.teampage-border').animate({
			    width: "670px"
		  	}, 2000);
		  	// teampage-border (end)

		  	// footer-border (start)
		  	$('.footer-border').animate({
			    height: "490px"
		  	}, 2000);
		  	// footer-border (end)

        });

	</script>

</body>
	
</html>