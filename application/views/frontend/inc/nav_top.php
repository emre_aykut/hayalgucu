<div id="preloader">
    <div class="inner">
        <div class="spinner">
          <div class="rect1"></div>
          <div class="rect2"></div>
          <div class="rect3"></div>
          <div class="rect4"></div>
          <div class="rect5"></div>
        </div>
    </div>
</div>

<div class="burger-area">
	<input id="burger" type="checkbox" class="burger-menu" />

	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<a href="<?=base_url()?>" class="mb-logo">
					<img src="<?=base_url()?>assets/frontend/img/logo.svg">
				</a>		
			</div>
			<div class="col-md-6">
				<label for="burger" class="burger-label">
					<span></span>
					<span></span>
					<span></span>
				</label>
			</div>
		</div>
	</div>

	<nav class="mb-menu">    
		<ul>
			<li><a href="<?=base_url()?>about" class="about-nav">HAKKIMIZDA</a></li>
			<li><a href="<?=base_url()?>project" class="project-nav">PROJELER</a></li>
			<li><a href="<?=base_url()?>team" class="team-nav">TAKIM</a></li>
			<li><a href="<?=base_url()?>article" class="article-nav">MAKALELER</a></li>
			<li><a href="<?=base_url()?>career" class="career-nav">KARİYER</a></li>
			<li><a href="<?=base_url()?>contact" class="contact-nav">İLETİŞİM</a></li>
		</ul>  
	</nav>
</div>

<div class="header-bg"></div>

<header>
	<div class="container">
		<div class="row">
			
			<div class="col-md-3">
				<a href="<?=base_url()?>" class="logo">
					<img src="<?=base_url()?>assets/frontend/img/logo.svg">
				</a>
			</div>

			<div class="col-md-9">
				<div class="right-area">
					<div class="menu">
						<a href="<?=base_url()?>about" class="about-nav">HAKKIMIZDA</a>
						<a href="<?=base_url()?>project" class="project-nav">PROJELER</a>
						<a href="<?=base_url()?>team" class="team-nav">TAKIM</a>
						<a href="<?=base_url()?>article" class="article-nav">MAKALELER</a>
						<a href="<?=base_url()?>career" class="career-nav">KARİYER</a>
						<a href="<?=base_url()?>contact" class="contact-nav">İLETİŞİM</a>
					</div>
					<div class="language">
						<a href="#">TR</a>
					</div>
				</div>
			</div>

		</div>
	</div>
</header>

<div class="header-hidden-area"></div>