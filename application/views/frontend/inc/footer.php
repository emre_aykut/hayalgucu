<footer>

	<div class="footer-bg"></div>
	
	<div class="container">
		<div class="row">

			<div class="col-md-12">
				<div class="footer-border"></div>
			</div>

			<div class="col-md-6">
				<div class="logo-icon">
					<img src="<?=base_url()?>assets/frontend/img/logo-icon.svg">
				</div>
				<div class="copyright-area">
					<b>2019</b> © Hayal Gücü
				</div>
			</div>

			<div class="col-md-6">
				<div class="right-area">
					<div class="phone">
						+90 312 466 15 98 - 99
					</div>
					<div class="address">
						Ümit Mahallesi 2546. Sokak No:21<br />Ümitköy - Ankara - Türkiye
					</div>
					<div class="links">
						<div class="languages">
							<a href="#" class="active">TR</a>
							<a href="#">EN</a>
							<a href="#">RU</a>
							<a href="#">AR</a>
							<a href="#">FR</a>
						</div>
						<div class="social-area">
							<a href="#" class="icon-tw">
								<i class="fab fa-twitter"></i>
							</a>
							<a href="#" class="icon-in">
								<i class="fab fa-linkedin-in"></i>
							</a>
							<a href="#" class="icon-you">
								<i class="fab fa-youtube"></i>
							</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

</footer>