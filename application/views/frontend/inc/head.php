<link rel="shortcut icon" href="<?=base_url()?>assets/frontend/img/fav-icon.png">

<link href="<?php echo base_url()?>assets/plugins/bootstrap-v4/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/frontend/css/animate.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/frontend/css/hover-min.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/frontend/font/style.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/frontend/css/style.css" rel="stylesheet">

<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->