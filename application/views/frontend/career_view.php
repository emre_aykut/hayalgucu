<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?=$page_detail->meta_description?>">
    <meta name="keywords" content="<?=$page_detail->meta_keywords?>">

    <title><?=$page_detail->meta_title?></title>

    <?php include('inc/head.php'); ?>

</head>

<body>
	
	<?php include('inc/nav_top.php'); ?>

	<section class="career-area-page">
		<div class="container">
			<div class="row">

				<div class="col-md-12">
					<div class="special-area">
						<div class="special-title"><?=$page_detail->title?></div>
					</div>
				</div>

				<div class="col-md-12">
					<div class="careerpage-border-left"></div>
				</div>

				<div class="col-md-12">
					<div class="detail animated bounceInRight">
						<?=$page_detail->detail?>
					</div>
				</div>

				<form action="#" id="career-form" class="form-horizontal" enctype="multipart/form-data">
				
					<div class="col-md-6 float-left">
						<div class="content animated bounceInRight">
							<div class="special-form-group">
								<div class="special-label">İsim Soyisim</div>
								<input type="text" name="fullname" class="special-input" />
							</div>
							<div class="special-form-group">
								<div class="special-label">Telefon</div>
								<input type="text" name="phone" class="special-input" />
							</div>
						</div>
					</div>

					<div class="col-md-6 float-left">
						<div class="content animated bounceInRight">
							<div class="special-form-group">
								<div class="special-label">E-posta</div>
								<input type="email" name="email" class="special-input" />
							</div>
							<div class="special-form-group">
								<div class="special-label">Cv</div>
								
								<div class="file-area">
									<input type="file" name="cv" id="file-cv" class="inputfile inputfile-1" />
									<label for="file-cv">
										<img src="<?=base_url()?>assets/frontend/img/cv-upload-icon.svg">
										<span class="file-text"></span>
									</label>
								</div>

							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="special-btn-area animated bounceInRight">
							<a href="javascript:void(0)" onclick="career_form_save()" id="btn-send">Gönder</a>
						</div>
					</div>

				</form>

				<div class="col-md-12">
					<div class="careerpage-border"></div>
				</div>

			</div>
		</div>
	</section>
  
	<?php include('inc/footer.php'); ?>
	<?php include('inc/script.php'); ?>
	<script src="<?php echo base_url()?>assets/plugins/sweetalert/js/sweetalert.min.js"></script>

	<script type="text/javascript">

		$(window).load(function() {

			// careerpage-border-left (start)
            $('.careerpage-border-left').animate({
			    height: "300px"
		  	}, 2000);
		  	// careerpage-border-left (end)

		  	// careerpage-border (start)
		  	$('.careerpage-border').animate({
			    width: "670px"
		  	}, 2000);
		  	// careerpage-border (end)

		  	// footer-border (start)
		  	$('.footer-border').animate({
			    height: "490px"
		  	}, 2000);
		  	// footer-border (end)

        });
		
		$(document).ready(function(){

			$('#file-cv').change(function(e) {
			    var file_name = $(this).val();
			    $('.file-area .file-text').text(file_name);
			});

		});

		function career_form_save()
		{
		    $('#btn-send').text('Gönderiliyor...');
		    $('#btn-send').attr('disabled',true);

		    var warning_text = '';
		    var formData = new FormData($('#career-form')[0]);
		    $.ajax({
		        url : base_url + 'career/ajax_add',
		        type: "POST",
		        data: formData,
		        contentType: false,
		        processData: false,
		        dataType: "JSON",
		        success: function(data)
		        {
		            if(data.status) 
		            {
		            	$('[name="fullname"], [name="email"], [name="phone"], [name="cv"]').val('');
		            	$('.file-area .file-text').text('');

		                swal({
							title: 'Başarılı',
							text: 'Form başarılı bir şekilde gönderildi.',
							icon: "success",
							button: 'Kapat',
						});
		            }
		            else
		            {
		                for (var i = 0; i < data.inputerror.length; i++) 
		                {
		                	warning_text += '-' + data.error_string[i] + '\n'; 
		                }

		                swal({
							title: 'Uyarı',
							text: warning_text,
							icon: "warning",
							button: 'Kapat',
						});
		            }

		            $('#btn-send').text('Gönder'); 
		            $('#btn-send').attr('disabled',false);
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            swal({
						title: 'Başarılı',
						text: 'Form gönderilirken bir hata oluştu.',
						icon: "error",
						button: 'Kapat',
					});

		            $('#btn-send').text('Gönder'); 
		            $('#btn-send').attr('disabled',false);

		        }
		    });
		}

	</script>

</body>
	
</html>