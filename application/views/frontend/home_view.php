<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Hayalgücü Mimarlık">
    <meta name="keywords" content="Hayalgücü, Mimarlık">

    <title>Hayalgücü</title>

    <?php include('inc/head.php'); ?>
    <link rel="stylesheet" href="<?=base_url()?>assets/plugins/owlCarousel2/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/plugins/owlCarousel2/css/owl.theme.default.min.css">

</head>

<body>
	
	<?php include('inc/nav_top.php'); ?>

	<section class="slider-area">
		<div class="container">
			<div class="row">

				<div class="col-md-12">
					
					<div id="homeSlider" class="carousel slide" data-ride="carousel">

					    <!-- Wrapper for slides -->
					    <div class="carousel-inner">
		    					    			
					    	<?php $i=0; ?>
						    <?php if ($sliders) { ?>
						    <?php foreach ($sliders as $slider): ?>
						        
						        <?php if ($i == 0){ ?>

					          	<div class="carousel-item active">
						            <img src="<?=base_url() . UPLOAD_FOLDER . $slider['photo']?>" class="d-block w-100">
						            <div class="carousel-caption">
							        	<p><?=$slider['title']?></p>
							      	</div>
					          	</div> 

						        <?php }else{ ?>

				         		<div class="carousel-item">
						            <img src="<?=base_url() . UPLOAD_FOLDER . $slider['photo']?>" class="d-block w-100">
						            <div class="carousel-caption">
							        	<p><?=$slider['title']?></p>
							      	</div>
					          	</div>
						        
						        <?php } $i++; ?>

						    <?php endforeach ?>
						    <?php } ?>
		    
					    </div>

					    <!-- Left and right controls -->
					    <a class="carousel-control-prev" href="#homeSlider" data-slide="prev">
						    <span class="carousel-control-prev-icon"></span>
					  	</a>
					  	<a class="carousel-control-next" href="#homeSlider" data-slide="next">
						    <span class="carousel-control-next-icon"></span>
					  	</a>
					    
					</div>

					<div class="slider-border"></div>

				</div>

				<div class="col-md-12">
					<div class="homepage-border-right1"></div>
				</div>

			</div>
		</div>
	</section>

	<section class="video-area">
		<div class="container">
			<div class="row">
				
				<div class="col-md-6">
					<div class="video-img-area">
						<div class="play-btn hvr-grow">
							<i class="fas fa-play play-icon"></i>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="company-detail">
						<div class="title">Hayalgücü Mimarlık</div>
						<div class="clear"></div>
						<div class="short-detail">
							Değişik coğrafyalarda farklı ulusal ve uluslar arası norm, standart, yasa ve yönetmeliklere hakim, İngilizce, Fransızca, Rusça ve Arapça proje hazırlama ve sunma yeterliliğine,
						</div>
						<div class="detail">
							inisiyatif alma, hızlı karar verme ve uygulamaya geçebilme esnekliğine, disiplinler arası işbirliği ve koordinasyon konusunda deneyime ve güçlü bir akademik – entelektüel arka plana sahip özelleşmiş tasarım ekiplerinin yarattığı sinerji ve motivasyon Hayalgücü Tasarım’ın ayırt edici özellikleri olarak sıralanabilir
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>

	<section class="project-area">
		<div class="container">
			<div class="row">

				<div class="col-md-12">
					<div class="homepage-border-left1"></div>
				</div>
				
				<div class="col-md-12">
					<div class="special-area">
						<div class="special-title font-weight-100">PROJELER</div>
					</div>
				</div>

				<div class="projects">

					<?php if ($project_categories): ?>
					<?php foreach ($project_categories as $category): ?>
						<div class="col-lg-2 col-md-4 col-6 float-left">
							<a href="<?=base_url() . 'project/' .seo_url($category['title']) . '-' . $category['content_id']?>" class="category">
								<div class="icon hvr-float">
									<img src="<?=base_url()?><?=UPLOAD_FOLDER . $category['photo']?>">
								</div>
								<div class="text"><?=$category['title']?></div>
							</a>
						</div>
					<?php endforeach ?>
					<?php endif ?>

				</div>

			</div>
		</div>
	</section>

	<section class="career-area">
		<div class="container">
			<div class="row">

				<div class="col-md-12">
					<div class="career-border"></div>
				</div>

				<div class="col-md-12">
					<div class="text-area">
						<div class="title">Hayalgücü ekibine katılmak ister misiniz?</div>
						<a href="<?=base_url()?>career" class="btn-area">Kariyer</a>
					</div>
				</div>

			</div>
		</div>
	</section>

	<section class="article-area">
		<div class="container">
			<div class="row">
				
				<div class="col-md-12">
					<div class="special-area">
						<div class="special-title font-weight-100">MAKALELER</div>
					</div>
				</div>

				<div class="col-md-12">
					<div class="article-owl">
						<div class="owl-carousel owl-theme">

							<?php if ($articles): ?>
							<?php foreach ($articles as $article): ?>
							<div class="item">
								<a href="<?=base_url()?>article/detail/<?=seo_url($article['title']) . '-' . $article['content_id']?>" class="box">
									<div class="date"><?=general_date_format($article['publish_date'])?></div>
									<div class="title"><?=text_limitation($article['title'], 80)?></div>
									<div class="author-area">
										<div class="icon-area"></div>
										<div class="text-area">
											<div class="fullname"><?=$article['author_fullname']?></div>
											<div class="sector"><?=$article['author_title']?></div>
										</div>
									</div>
								</a>
							</div>
							<?php endforeach ?>
							<?php endif ?>

			          	</div>

					</div>
				</div>

				<div class="col-md-12">
					<div class="article-border"></div>
				</div>

			</div>
		</div>
	</section>

	<!-- The Video Modal (start) -->
	<div class="modal fade" id="video-modal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">

				<div class="modal-body video-modal" style="padding: 0; margin-bottom: -6px;">
					<iframe width="100%" height="450" src="https://www.youtube.com/embed/8KlpNV5LjeU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>		
				</div>

			</div>
		</div>
	</div>
	<!-- The Video Modal (end) -->

  
	<?php include('inc/footer.php'); ?>
	<?php include('inc/script.php'); ?>

	<script src="<?php echo base_url()?>assets/plugins/owlCarousel2/js/owl.carousel.min.js"></script>

	<script type="text/javascript">

		var header_height = 0;
		var slider_height = 0;
		var video_height = 0;
		var career_height = 0;
		var project_height = 0;

		$(window).load(function() {

			// slider-border (start)
            $('.slider-border').animate({
			    width: "500px"
		  	}, 2000);
		  	// slider-border (end)
		  	
        });

		$(document).ready(function(){

			$('.video-area .video-img-area .play-btn').click(function(){
				$('#video-modal').modal('show');
			});

			$('.owl-carousel').owlCarousel({
		        loop: true,
		        margin: 10,
		        autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
		        responsiveClass: true,
		        responsive: {
					0: {
						items: 1,
						nav: true,
					},
					768: {
						items: 3,
						nav: true
					},
					1000: {
						items: 4,
						nav: true,
					}
		        }
	     	});

		});  

		if ($(window).width() > 1420)
		{  
			$(window).scroll(function() 
			{

				header_height = $('header').height();
				slider_height = $('.slider-area').height();
				video_height = $('.video-area').height();
				career_height = $('.career-area').height();
				project_height = $('.project-area').height();

				// homepage-border-right1 (start)
				if($(window).scrollTop() >= header_height + 100) 
	            {
	            	$('.homepage-border-right1').animate({
					    height: "550px"
				  	}, 2000);
	            }
	            // homepage-border-right1 (end)

	            // homepage-border-left1 (start)
				if($(window).scrollTop() >= header_height + slider_height - 200) 
	            {
	            	$('.homepage-border-left1').animate({
					    height: "660px"
				  	}, 2000);
	            }
	            // homepage-border-left1 (end)

	            // career-border (start)
				if($(window).scrollTop() >= header_height + slider_height + video_height - 100) 
	            {
	            	$('.career-border').animate({
					    width: "500px"
				  	}, 2000);
	            }
	            // career-border (end)

	            // article-border (start)
				if($(window).scrollTop() >= header_height + slider_height + video_height + project_height + career_height - 100) 
	            {
	            	$('.article-border').animate({
					    width: "670px"
				  	}, 2000);
	            }
	            // article-border (end)

	            // footer-border (start)
				if($(window).scrollTop() >= header_height + slider_height + video_height + project_height + career_height + 100) 
	            {
	            	$('.footer-border').animate({
					    height: "490px"
				  	}, 2000);
	            }

			});
		}  

	</script>

</body>
	
</html>