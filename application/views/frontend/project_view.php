<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?=$page_detail->meta_description?>">
    <meta name="keywords" content="<?=$page_detail->meta_keywords?>">

    <title><?=$page_detail->meta_title?></title>

    <?php include('inc/head.php'); ?>

</head>

<body>
	
	<?php include('inc/nav_top.php'); ?>

	<section class="project-area">
		<div class="container">
			<div class="row">
				
				<div class="col-md-12">
					<div class="special-area">
						<div class="special-title">PROJELER</div>
					</div>
				</div>

				<div class="col-md-12">
					<div class="projectpage-border-left"></div>
				</div>

				<div class="col-md-12">
					<div class="projectpage-border-right"></div>
				</div>

				<div class="projects animated fadeInLeftBig">

					<input type="hidden" name="category_id" value="<?=$category_id?>">

					<?php if ($project_categories): ?>
					<?php foreach ($project_categories as $category): ?>
						<div class="col-lg-2 col-md-4 col-6 float-left">
							<a href="<?=base_url() . 'project/' .seo_url($category['title']) . '-' . $category['content_id']?>" class="category <?=$category_id == $category['content_id'] ? 'active' : ''?>">
								<div class="icon hvr-float">
									<img src="<?=base_url()?><?=UPLOAD_FOLDER . $category['photo']?>">
								</div>
								<div class="text"><?=$category['title']?></div>
							</a>
						</div>
					<?php endforeach ?>
					<?php endif ?>

				</div>

				<div class="col-md-12">
					<div class="projectpage-top-border"></div>
				</div>

				<div class="items">
					
					<div id="output"></div>

				</div>

				<div class="col-md-12">
					<div class="post-loader-area">
		               <div class="loader">
		                  <div class="chevron"></div>
		                  <div class="chevron"></div>
		                  <div class="chevron"></div>
		                  <span class="text">Aşağı Kaydır</span>
		               </div>
		            </div>
				</div>

				<div class="col-md-12">
					<div class="projectpage-bottom-border"></div>
				</div>

			</div>
		</div>
	</section>
  
	<?php include('inc/footer.php'); ?>
	<?php include('inc/script.php'); ?>

	<script type="text/javascript">

		$(window).load(function() {

			// projectpage-top-border (start)
            $('.projectpage-top-border').animate({
			    width: "480px"
		  	}, 2000);
		  	// projectpage-top-border (end)

			// projectpage-border-right (start)
            $('.projectpage-border-right').animate({
			    height: "510px"
		  	}, 2000);
		  	// projectpage-border-right (end)
		  	
        });

		$(document).ready(function(){

			load_more_project();

		});

		var header_height = 0;
		var projects_height = 0;
		var items_height = 0;

		$(window).scroll(function()
		{
            // load more article run (start)
            if($(window).scrollTop() + 1 >= $(document).height() - $(window).height()) 
            {
               	var total_pages = parseInt($("#total_pages").val());
               	var page = parseInt($("#page").val())+1;
               	if(page <= total_pages)
               	{
                  load_more_project(page, total_pages);
               	} 
               	else
               	{
               		$('.loader').hide();
               		
		           	if ($(window).width() > 1420)
					{
						header_height = $('header').height();
						projects_height = $('.projects').height();
						items_height = $('.items').height();

						// projectpage-border-left (start)
						if($(window).scrollTop() >= header_height + projects_height + 100) 
			            {
			            	$('.projectpage-border-left').animate({
							    height: "740px"
						  	}, 2000);
			            }
			            // projectpage-border-left (end)

			            // projectpage-bottom-border and footer-border (start)
						if($(window).scrollTop() >= header_height + projects_height + items_height - 500) 
			            {
			            	$('.projectpage-bottom-border').animate({
							    width: "670px"
						  	}, 2000);

						  	$('.footer-border').animate({
							    height: "490px"
						  	}, 2000);
			            }
			            // projectpage-bottom-border and footer-border (end)
		            }
               	}
            }
            // load more article run (start)
		});

		function load_more_project(page, total_pages) 
		{    
    		$("#total_pages, #page").remove();
    		var category_id = $('[name="category_id"]').val();

    		$.ajax({
	            url: base_url + 'project/ajax_projects/8/' + category_id,
	            type: "POST",
	            data: {page:page},
	            beforeSend: function(){
	               $(".loader").show();
	            },
	            complete: function(){
	               // $('.loader').hide();
	            },
	            success: function(data){
	               $("#output").append(data);
	            },
	            error: function(){
	               $(".loader").html("No data found!");
	            }
     		});
      	}

	</script>

</body>
	
</html>