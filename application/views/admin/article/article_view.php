<?php include(dirname(__DIR__) . '/inc/header.php'); ?>
    
    <div class="form-content">   
        
        <div class="row margin-bottom-25">
            <div class="col-md-12">
                <h2 class="modul-title">MAKALE MODÜLÜ</h2>
            </div>
            <div class="col-md-8">
                <button class="btn btn-success" onclick="add_article()"><i class="glyphicon glyphicon-plus"></i> Makale Ekle</button>
                <a href="/admin/articleModule/article_author" class="btn btn-warning" ><i class="glyphicon glyphicon-share"></i> Yazarlar</a>
                <a href="/admin/articleModule/article_category" class="btn btn-warning" ><i class="glyphicon glyphicon-share"></i> Kategoriler</a>
            </div>
            <div class="col-md-4">
                <select class="form-control font-bold" id="lang">
                    
                </select>
            </div>
        </div>
        
        <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Başlık</th>
                        <th>Yazar</th>
                        <th>Tarih</th>
                        <th>Durumu</th>
                        <th class="width-150">İşlem</th>
                    </tr>
                </thead>
                
                <tbody>
                </tbody>

                <tfoot>
                <tr>
                    <th>Başlık</th>
                    <th>Yazar</th>
                    <th>Tarih</th>
                    <th>Durumu</th>
                    <th class="width-150">İşlem</th>
                </tr>
                </tfoot>
            </table>
        </div>
 
    </div>


<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

<script type="text/javascript">

var save_method;
var table;
var language_count = 0;
var default_language;
var languages = [];
var authors = [];
var categories = [];
ajax_languages();
ajax_authors();
ajax_categories();

$(document).ready(function() {

    table = $('#table').DataTable({ 

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Turkish.json"
        },

        "processing": true,
        "serverSide": true, 
        "order": [],

        "ajax": {
            "url": base_url + 'admin/articleModule/article/ajax_list',
            "type": "POST"
        },

        "columnDefs": [
            { 
                "targets": [ -1 ],
                "orderable": false,
            },
            {
                "targets": [ -2 ],
                "render": function ( data, type, row, meta ) {
                    if (data == 1) {
                        return '<span class="label label-success">Aktif</span>'
                    }else{
                        return '<span class="label label-danger">Pasif</span>'
                    }
                }
            },
        ],

    });

    for (var i = 1; i <= languages.length; i++)
    {
        var lang_selected = '';
        var lang_active = '';
        var lang_activeID = '';
        var lang_in_active_defaultActive = '';
        var common_img_status = 0;

        if (default_language == languages[language_count].id) 
        {
            lang_selected = 'selected';
            lang_active = 'active';
            lang_activeID = 'activeID';
            lang_in_active_defaultActive = 'in active defaultActive';
        }

        if (i > 1) 
        {
            common_img_status = 1;
        }

        $('#lang').append('<option value="'+languages[language_count].id+'" '+lang_selected+'>'+languages[language_count].name+'</option>');

        $('#language-nav').append(
            '<li class="'+lang_active+'" id="'+lang_activeID+'">' +
                '<a data-toggle="tab" href="#tab-'+languages[language_count].code+'">'+languages[language_count].name+'</a>' +
            '</li>'
        );

        $('#form-content-lang').append(
            '<div id="tab-'+languages[language_count].code+'" class="tab-pane fade '+lang_in_active_defaultActive+' ">' +
                '<div class="panel-body">' +

                    '<div class="form-body">' +
                        
                        '<input type="hidden" value="" name="id['+i+']"/>' +
                        '<input type="hidden" name="lang_id['+i+']" value="'+languages[language_count].id+'">' +

                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Başlık</label>' +
                            '<div class="col-md-9">' +
                                '<input name="title['+i+']" placeholder="Başlık" class="form-control title_class" data-id="'+i+'" type="text" id="title_'+i+'" >' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Link</label>' +
                            '<div class="col-md-9">' +
                                '<input name="link['+i+']" placeholder="Link" class="form-control link_class" type="text" id="link_'+i+'" >' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Açıklama</label>' +
                            '<div class="col-md-9">' +
                                '<textarea name="description['+i+']" placeholder="Açıklama" class="form-control"></textarea>' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Detay</label>' +
                            '<div class="col-md-9">' +
                                '<textarea name="detail['+i+']" id="detail_'+i+'" placeholder="Detay" class="form-control"></textarea>' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Tarih</label>' +
                            '<div class="col-md-9">' +
                                '<input name="publish_date['+i+']" placeholder="yyyy-mm-dd" class="form-control datepicker datepicker_publish" type="text">' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Yazar</label>' +
                            '<div class="col-md-9">' +
                                '<select class="form-control author_id" id="author_'+i+'" name="author_id['+i+']">' +
                                    '<option value="0" >Seçiniz...</option>' +
                                '</select>' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Kategori</label>' +
                            '<div class="col-md-9">' +
                                '<select class="form-control category_id" id="category_'+i+'" name="category_id['+i+']">' +
                                    '<option value="0" >Seçiniz...</option>' +
                                '</select>' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Etiket</label>' +
                            '<div class="col-md-9">' +
                                '<input name="tag['+i+']" class="form-control tags" type="text" id="tags_'+i+'" >' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-area-border common-img-'+common_img_status+'">' +
                            '<div class="form-group photo-class" id="photo-preview-'+i+'">' +
                                '<label class="control-label col-md-3">Fotoğraf</label>' +
                                '<div class="col-md-9">' +
                                    '(Fotoğraf Yok)' +
                                    '<span class="help-block"></span>' +
                                '</div>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label class="control-label col-md-3" id="label-photo">Fotoğraf Yükle </label>' +
                                '<div class="col-md-9">' +
                                    '<input name="photo_'+i+'" type="file">' +
                                    '<span class="help-block"></span>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group" style="display:none">' +
                            '<div class="col-md-3"></div>' +
                            '<div class="col-md-9">' +
                                '<label class="font-bold common-img ">' +
                                    '<input type="checkbox" name="common_img" value="1">' +
                                    ' Tüm dillerde ortak görsel/görseller kullanılsın' +
                                '</label>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Yayınlama</label>' +
                            '<div class="col-md-9">' +
                                '<label class="switch">' +
                                    '<input type="checkbox" name="status['+i+']" id="status_'+i+'" class="status common-status" value="1" checked>' +
                                    '<span class="slider round"></span>' +
                                    '<span class="status-active"> Aktif</span>' +
                                    '<span class="status-passive"> Pasif</span>' +
                                '</label>' +
                            '</div>' +
                        '</div>' +
                        
                    '</div>' +

                '</div>' +
            '</div>'
        );

            
        for (var m = 0; m < authors.length; m++)
        {
            if (authors[m].lang_id == languages[language_count].id) {
                $('#author_'+i).append('<option value="'+authors[m].content_id+'">'+authors[m].fullname+'</option>');
            }
        }

        for (var m = 0; m < categories.length; m++)
        {
            if (categories[m].lang_id == languages[language_count].id) {
                $('#category_'+i).append('<option value="'+categories[m].content_id+'">'+categories[m].title+'</option>');
            }
        }

        CKEDITOR.replace('detail['+i+']',{
            filebrowserWindowWidth: '900',
            filebrowserWindowHeight: '700',
            filebrowserBrowseUrl: base_url + 'assets/plugins/ckfinder/ckfinder_.php',
            filebrowserImageBrowseUrl: base_url + 'assets/plugins/ckfinder/ckfinder_.php?Type=Images',
            filebrowserUploadUrl: base_url + 'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: base_url + 'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images'
        }); 

        language_count++;
    }

    $('.link_class').prop('readonly', true);
    $('.title_class').on('keyup', function() {
        var data_id = $(this).attr('data-id');
        var str = $(this).val();
        str = replace_special_chars(str);
        str = str.toLowerCase();
        str = str.replace( /\s\s+/g, ' ' ).replace(/[^a-z0-9\s]/gi, '').replace(/[^\w]/ig, '-');
        $('#link_'+data_id).val(str);
    });

    $(document).on('change','#lang',function(){
        var lang_id = $(this).val();
        table.ajax.url(base_url + 'admin/articleModule/article/ajax_list/' + lang_id).load();
        $('#lang_id option').removeAttr('selected')
        $('#lang_id #l_' + lang_id).attr('selected','selected');
    });

    $('.datepicker').datetimepicker({
        autoclose: true,
        format: "yyyy-mm-dd hh:ii",
        todayHighlight: true,
        orientation: "top auto",
        todayBtn: true,
        todayHighlight: true,  
    });

    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $(".author_id").change(function() {
        var new_value = $(this).val();
        $('.author_id').val(new_value);
    });
    $(".category_id").change(function() {
        var new_value = $(this).val();
        $('.category_id').val(new_value);
    });
    $('.datepicker_publish').change(function() {
        var new_value = $(this).val();
        $('.datepicker_publish').val(new_value);
    });
    $('.common-img input[type="checkbox"]').change(function() {
        if($(this).is(":checked")) 
        {
            $('.common-img-1').css('display','none');
            $('.common-img input[type="checkbox"]').click();
        }
        else
        {
            $('.common-img-1').css('display','block');
            $('.common-img input[type="checkbox"]').click();
        }      
    });

    $('.tags').tagsInput();

});

function ajax_authors()
{
    $.ajax({
        url : base_url + 'admin/articleModule/article_author/ajax_author_get',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            authors = data.authors;
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        },
        async: false,
    });
}

function ajax_categories()
{
    $.ajax({
        url : base_url + 'admin/articleModule/article_category/ajax_category_get',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            categories = data.categories;
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        },
        async: false,
    });
}

function add_article()
{
    save_method = 'add';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty(); 
    $('#modal_form').modal('show');
    $('.modal-title').text('Makale Ekle');
    $('#errorMessage').text('');
    $('.photo-class').hide();
    $('.nav-tabs li').removeClass('active'); 
    $('#activeID').addClass('active');
    $('.tab-pane').removeClass('active'); 
    $('.defaultActive').addClass('in active');
    $('.common-status').attr('checked','checked');
    $('.common-img input[type="checkbox"]').removeAttr('checked');
    $('.common-img-1').css('display','block');

    for (var i = 1; i <= languages.length; i++)
    {
        $('#tags_'+i).importTags('');
        $('[name="detail['+i+']"]').val(CKEDITOR.instances['detail_'+i].setData(''));
    }
}

function edit_article(id)
{
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#errorMessage').text('');

    $.ajax({
        url : base_url + 'admin/articleModule/article/ajax_edit/' + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            for (var i = 1; i <= data.total_record; i++) 
            {
                $('[name="id['+i+']"]').val(data[i].id);
                $('[name="content_id"]').val(data[i].content_id);
                $('[name="title['+i+']"]').val(data[i].title);
                $('[name="description['+i+']"]').val(data[i].description);
                CKEDITOR.instances['detail_'+i].setData(data[i].detail);
                $('[name="link['+i+']"]').val(data[i].link);
                $('[name="publish_date['+i+']"]').datetimepicker('update',data[i].publish_date);
                $('[name="lang_id['+i+']"]').val(data[i].lang_id);
                $('[name="author_id['+i+']"]').val(data[i].author_id);
                $('[name="category_id['+i+']"]').val(data[i].category_id);
                $('[name="tag['+i+']').importTags(data.tags[i]);

                if (data[i].status == 0) 
                {
                    $('#status_'+ i).removeAttr('checked');
                }
                else if(data[i].status == 1)
                {
                    $('#status_'+ i).attr('checked','checked');
                }

                if (data[i].common_img == 0) 
                {
                    $('.common-img-1').css('display','block');
                    $('.common-img input[type="checkbox"]').removeAttr('checked');
                }
                else if(data[i].common_img == 1)
                {
                    $('.common-img-1').css('display','none');
                    $('.common-img input[type="checkbox"]').attr('checked', 'checked');
                }

                $('#modal_form').modal('show'); 
                $('.modal-title').text('Makale Düzenle');
                $('.nav-tabs li').removeClass('active'); 
                $('#activeID').addClass('active');
                $('.tab-pane').removeClass('active'); 
                $('.defaultActive').addClass('in active');

                $('#photo-preview-'+i).show(); 

                if(data[i].photo)
                {
                    $('#photo-preview-' +i + ' div').html('<img src="'+base_url+upload_folder+data[i].photo+'" class="img-responsive" width="100">'); 
                    $('#photo-preview-' +i + ' div').append('<input type="checkbox" name="remove_photo_'+i+'" value="'+data[i].photo+'"/> Kaydederken fotoğrafı kaldır'); 
                }
                else
                {
                    $('#photo-preview-' +i + ' div').text('(Fotoğraf Yok)');
                }

            }


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false);
}

function save(preview = '')
{
    var url;

    $('#btnSave').text('kaydediyor...');
    $('#btnSave').attr('disabled',true);

    if (preview == 'preview' && save_method == 'add') 
    {
        $('.common-status').removeAttr('checked');
    }

    for (var i = 1; i <= languages.length; i++)
    {
        $('[name="detail['+i+']"]').val(CKEDITOR.instances['detail_'+i].getData());
    }

    if(save_method == 'add') 
    {
        url = base_url + 'admin/articleModule/article/ajax_add';
        notify_message = 'Kayıt başarılı bir şekilde eklendi.';
        notify_type = 'success';
    } else {
        url = base_url + 'admin/articleModule/article/ajax_update';
        notify_message = 'Kayıt başarılı bir şekilde düzenlendi.';
        notify_type = 'info';
    }

    var formData = new FormData($('#form')[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) 
            {

                if (preview == 'preview') 
                {
                    $('#iframe-area').addClass('iframe-area-class');
                    $('#iframe-area').append('<a class="iframe-area-close btn btn-danger">Kapat</a>');
                    $('#iframe-area').append('<center class="iframe-center"><iframe src="'+base_url+'article/detail/'+data.content_id+'/0" width="90%" height="95%"></iframe></center>');

                    $('.iframe-area-close').click(function(){
                        $('#iframe-area').removeClass('iframe-area-class');
                        $('.iframe-center').remove();
                        $('.iframe-area-close').remove();
                        edit_article(data.content_id);
                    });
                }
                else
                {
                    reload_table();
                    $('#modal_form').modal('hide');
                    $('.nav-tabs li').removeClass('active');
                    $('.tab-pane').removeClass('active'); 
                    $('#activeID').addClass('active');
                    $('.defaultActive').addClass('in active');

                    $.notify({
                        icon: 'glyphicon glyphicon-ok',
                        message: notify_message
                    },{
                        type: notify_type,
                        offset: {
                            x: 60,
                            y: 120
                        },
                        animate: {
                            enter: 'animated fadeInRight',
                            exit: 'animated fadeOutRight'
                        }
                    });
                }

                $('#errorMessage').text('');
                
            }
            else
            {   
                
                for (var i = 0; i < data.inputerror.length; i++) 
                {   
                    $('[name="'+data.inputerror[i][data.lang[i]]+'['+data.lang[i]+']"]').parent().parent().addClass('has-error'); 
                    $('[name="'+data.inputerror[i][data.lang[i]]+'['+data.lang[i]+']"]').next().text(data.error_string[i][data.lang[i]]); 
                }
                
                $('#errorMessage').text('*Gerekli alanları doldurmadan işleme devam edemezsiniz.');
                $('#modal_form').stop().animate({
                    scrollTop:0
                });

            }
           
            $('#btnSave').text('Kaydet'); 
            $('#btnSave').attr('disabled',false);
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('Kaydet'); 
            $('#btnSave').attr('disabled',false);
        }
    });
}

function delete_article(id)
{
    $.ajax({
        url : base_url + 'admin/articleModule/article/ajax_delete/' + id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            $('#modal_form').modal('hide');
            reload_table();

            $.notify({
                icon: 'glyphicon glyphicon-remove',
                message: 'Kayıt silindi.' 
            },{
                type: 'danger',
                offset: {
                    x: 60,
                    y: 120
                },
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                }
            });

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error deleting data');
        }
    });
}

</script>

<div id="iframe-area"></div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Makale Formu</h3>
            </div>
            <div class="modal-body form">

                <div id="errorMessage"></div>

                <ul class="nav nav-tabs" id="language-nav">
                    
                </ul>

                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">

                    <input type="hidden" value="" name="content_id"/> 

                    <div class="tab-content" id="form-content-lang">

                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <!-- <button type="button" id="btnPreview" onclick="save('preview')" class="btn btn-info pull-left">Önizleme</button> -->
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Kaydet</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">İptal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>