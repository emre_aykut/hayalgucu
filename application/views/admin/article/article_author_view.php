<?php include(dirname(__DIR__) . '/inc/header.php'); ?>

    <div class="form-content">   
        
        <div class="row margin-bottom-25">
            <div class="col-md-12">
                <h2 class="modul-title">YAZAR MODÜLÜ</h2>
            </div>
            <div class="col-md-8">
                <a href="/admin/articleModule/article" class="btn btn-primary" ><i class="glyphicon glyphicon-arrow-left"></i> Makaleye Geri Dön</a>
                <button class="btn btn-success" onclick="add_article_author()"><i class="glyphicon glyphicon-plus"></i> Yazar Ekle</button>
            </div>
            <div class="col-md-4">
                <select class="form-control font-bold" id="lang">
                    
                </select>
            </div>
        </div>
        
        <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Ad Soyad</th>
                        <th>Unvan</th>
                        <th>Fotoğraf</th>
                        <th class="width-150">İşlem</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>

                <tfoot>
                <tr>
                    <th>Ad Soyad</th>
                    <th>Unvan</th>
                    <th>Fotoğraf</th>
                    <th class="width-150">İşlem</th>
                </tr>
                </tfoot>
            </table>
        </div>
 
    </div>


<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

<script type="text/javascript">

var save_method;
var table;
var language_count = 0;
var default_language;
var languages = [];
ajax_languages();

$(document).ready(function() {

    table = $('#table').DataTable({ 

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Turkish.json"
        },

        "processing": true,
        "serverSide": true, 
        "order": [],

        "ajax": {
            "url": base_url + 'admin/articleModule/article_author/ajax_list',
            "type": "POST"
        },

        "columnDefs": [
            { 
                "targets": [ -1 ],
                "orderable": false,
            },
            { 
                "targets": [ -2 ],
                "orderable": false,
            },
        ],

    });

    for (var i = 1; i <= languages.length; i++)
    {
        var lang_selected = '';
        var lang_active = '';
        var lang_activeID = '';
        var lang_in_active_defaultActive = '';
        if (default_language == languages[language_count].id) 
        {
            lang_selected = 'selected';
            lang_active = 'active';
            lang_activeID = 'activeID';
            lang_in_active_defaultActive = 'in active defaultActive';
        }

        $('#lang').append('<option value="'+languages[language_count].id+'" '+lang_selected+'>'+languages[language_count].name+'</option>');

        $('#language-nav').append(
            '<li class="'+lang_active+'" id="'+lang_activeID+'">' +
                '<a data-toggle="tab" href="#tab-'+languages[language_count].code+'">'+languages[language_count].name+'</a>' +
            '</li>'
        );

        $('#form-content-lang').append(
            '<div id="tab-'+languages[language_count].code+'" class="tab-pane fade '+lang_in_active_defaultActive+' ">' +
                '<div class="panel-body">' +

                    '<div class="form-body">' +
                        
                        '<input type="hidden" value="" name="id['+i+']"/>' +
                        '<input type="hidden" name="lang_id['+i+']" value="'+languages[language_count].id+'">' +

                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Ad Soyad</label>' +
                            '<div class="col-md-9">' +
                                '<input name="fullname['+i+']" placeholder="Ad Soyad" class="form-control common-fullname" type="text">' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Unvan</label>' +
                            '<div class="col-md-9">' +
                                '<input name="title['+i+']" placeholder="Unvan" class="form-control" type="text">' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group photo-class" id="photo-preview-'+i+'">' +
                            '<label class="control-label col-md-3">Fotoğraf</label>' +
                            '<div class="col-md-9">' +
                                '(Fotoğraf Yok)' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3" id="label-photo">Fotoğraf Yükle </label>' +
                            '<div class="col-md-9">' +
                                '<input name="photo_'+i+'" type="file" >' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        
                    '</div>' +

                '</div>' +
            '</div>'
        );
        
        language_count++;
    }

    $(document).on('change','#lang',function(){
        var lang_id = $(this).val();
        table.ajax.url(base_url + 'admin/articleModule/article_author/ajax_list/' + lang_id).load();
        $('#lang_id option').removeAttr('selected')
        $('#lang_id #l_' + lang_id).attr('selected','selected');
    });

    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    $(".common-fullname").change(function() {
        var new_value = $(this).val();
        $('.common-fullname').val(new_value);
    });

});



function add_article_author()
{
    save_method = 'add';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty(); 
    $('#modal_form').modal('show');
    $('.modal-title').text('Yazar Ekle');
    $('#errorMessage').text('');
    $('.photo-class').hide();

    $('.nav-tabs li').removeClass('active'); 
    $('#activeID').addClass('active');
    $('.tab-pane').removeClass('active'); 
    $('.defaultActive').addClass('in active');
}

function edit_article_author(id)
{
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#errorMessage').text('');

    $.ajax({
        url : base_url + 'admin/articleModule/article_author/ajax_edit/' + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            for (var i = 1; i <= data.total_record; i++) {
                $('[name="id['+i+']"]').val(data[i].id);
                $('[name="content_id"]').val(data[i].content_id);
                $('[name="title['+i+']"]').val(data[i].title);
                $('[name="fullname['+i+']"]').val(data[i].fullname);
                $('[name="lang_id['+i+']"]').val(data[i].lang_id);
                $('#modal_form').modal('show'); 
                $('.modal-title').text('Yazar Düzenle');
                $('.nav-tabs li').removeClass('active'); 
                $('#activeID').addClass('active');
                $('.tab-pane').removeClass('active'); 
                $('.defaultActive').addClass('in active');

                $('#photo-preview-'+i).show(); 

                if(data[i].photo)
                {
                    $('#photo-preview-' +i + ' div').html('<img src="'+base_url+upload_folder+data[i].photo+'" class="img-responsive" width="100">'); 
                    $('#photo-preview-' +i + ' div').append('<input type="checkbox" name="remove_photo_'+i+'" value="'+data[i].photo+'"/> Kaydederken fotoğrafı kaldır'); 
                }
                else
                {
                    $('#photo-preview-' +i + ' div').text('(Fotoğraf Yok)');
                }

            }


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false);
}

function save()
{
    $('#btnSave').text('kaydediyor...');
    $('#btnSave').attr('disabled',true);
    var url;

    if(save_method == 'add') {
        url = base_url + 'admin/articleModule/article_author/ajax_add';
        notify_message = 'Kayıt başarılı bir şekilde eklendi.';
        notify_type = 'success';
    } else {
        url = base_url + 'admin/articleModule/article_author/ajax_update';
        notify_message = 'Kayıt başarılı bir şekilde düzenlendi.';
        notify_type = 'info';
    }

    var formData = new FormData($('#form')[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) 
            {
                $('#modal_form').modal('hide');
                reload_table();
                $('.nav-tabs li').removeClass('active');
                $('.tab-pane').removeClass('active'); 
                $('#activeID').addClass('active');
                $('.defaultActive').addClass('in active');

                $.notify({
                    icon: 'glyphicon glyphicon-ok',
                    message: notify_message
                },{
                    type: notify_type,
                    offset: {
                        x: 60,
                        y: 120
                    },
                    animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                    }
                });

                $('#errorMessage').text('');
                
            }
            else
            {   
                
                for (var i = 0; i < data.inputerror.length; i++) 
                {   
                  
                    $('[name="'+data.inputerror[i][data.lang[i]]+'['+data.lang[i]+']"]').parent().parent().addClass('has-error'); 
                    $('[name="'+data.inputerror[i][data.lang[i]]+'['+data.lang[i]+']"]').next().text(data.error_string[i][data.lang[i]]); 
                }
                
                $('#errorMessage').text('*Gerekli alanları doldurmadan işleme devam edemezsiniz.');
                $('#modal_form').stop().animate({
                    scrollTop:0
                });

            }

            $('#btnSave').text('Kaydet'); 
            $('#btnSave').attr('disabled',false);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('Kaydet'); 
            $('#btnSave').attr('disabled',false);

        }
    });
}

function delete_article_author(id)
{
    if(confirm('Bu kaydı silerken diğer tüm dillerdeki karşılığı da silinecek. Emin misiniz?'))
    {
        $.ajax({
            url : base_url + 'admin/articleModule/article_author/ajax_delete/' + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                $('#modal_form').modal('hide');
                reload_table();

                $.notify({
                    icon: 'glyphicon glyphicon-remove',
                    message: 'Kayıt silindi.' 
                },{
                    type: 'danger',
                    offset: {
                        x: 60,
                        y: 120
                    },
                    animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                    }
                });

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}



</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Yazar Formu</h3>
            </div>
            <div class="modal-body form">

                <div id="errorMessage"></div>

                <ul class="nav nav-tabs" id="language-nav">
                    
                </ul>

                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">

                    <input type="hidden" value="" name="content_id"/> 

                    <div class="tab-content" id="form-content-lang">

                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Kaydet</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">İptal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>