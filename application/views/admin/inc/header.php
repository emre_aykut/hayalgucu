<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>ADMIN PANEL</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/admin/img/fav-icon.png">

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,vietnamese">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/css/font-awesome.css" />

    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/datatables/css/dataTables.bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.min.css')?>" rel="stylesheet">


    <!-- CSS Files -->
    <link href="<?php echo base_url(); ?>assets/admin/css/panel-kit.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/admin/css/slick.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/admin/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/tagsinput/css/jquery.tagsinput.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/demo.css" rel="stylesheet" />

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div class="main-wrap">

    <header class="panel-header">
        <div class="container-fluid">
            
            <div class="panel-logo">
                <h2><a href="#">LOGO</a></h2>
            </div>
    
            <div class="settings-btn">
                <span class="session_username"><i class="fa fa-user"></i> <?=session_login_username()?></span>
                <a href="<?php echo base_url(); ?>admin/auth/logout">
                    <i class="fa fa-sign-out"></i> <span>Çıkış</span>
                </a>
            </div>

        </div>
    </header>

    <main>

        <aside class="sidebar">
            <div id="leftside-navigation" class="nano">
                <ul class="nano-content">
                    <li class="menu-open">
                        <a><i class="fa fa-arrow-right"></i></a>
                    </li>
                    <li class="menu-close">
                        <a><i class="fa fa-arrow-left"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>admin/dashboard" id="-admin-dashboard"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>admin/sliderModule/slider" id="-admin-sliderModule-slider"><i class="fa fa-sliders"></i><span>Slider</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>admin/teamModule/team" id="-admin-teamModule-team"><i class="fa fa-users"></i><span>Team</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>admin/articleModule/article" id="-admin-articleModule-article"><i class="fa fa-th"></i><span>Makale</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>admin/projectModule/project" id="-admin-projectModule-project"><i class="fa fa-database"></i><span>Project</span></a>
                    </li>
                    <li class="sub-menu">
                        <a href="<?php echo base_url(); ?>admin/settings/dashboard" id="-admin-settings-dashboard"><i class="fa fa-cogs"></i><span class="setting-class">Ayarlar</span></a>
                        <ul>
                            <li><a href="<?php echo base_url(); ?>admin/settings/page" id="-admin-settings-page"><i class="fa fa-columns"></i><span>Sayfalar</span></a></li>
                            <!-- <li><a href="<?php echo base_url(); ?>admin/settings/popup" id="-admin-settings-popup"><i class="fa fa-window-restore"></i><span>Popup</span></a></li> -->
                            <li><a href="<?php echo base_url(); ?>admin/settings/language" id="-admin-settings-language"><i class="fa fa-language"></i><span>Diller</span></a></li>
                            <li><a href="<?php echo base_url(); ?>admin/settings/user" id="-admin-settings-user"><i class="fa fa-user"></i><span>Kullanıcılar</span></a></li>
                            <li><a href="<?php echo base_url(); ?>admin/settings/contact" id="-admin-settings-contact"><i class="fa fa-file"></i><span>İletişim</span></a></li>
                            <li><a href="<?php echo base_url(); ?>admin/settings/career" id="-admin-settings-career"><i class="fa fa-file"></i><span>Kariyer</span></a></li>
                        </ul>
                    </li>

                </ul>

                <!-- <div class="customer-panel-logo">
                    <a href="#" target="_blank">
                        <img src="<?php echo base_url(); ?>assets/admin/img/fav-icon.png">   
                    </a>
                </div> -->

            </div>
        </aside>



