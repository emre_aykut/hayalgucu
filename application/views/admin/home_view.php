<?php include('inc/header.php'); ?>

<div class="form-content">

    <div class="row margin-bottom-25">
        <div class="col-md-12">
            <h2 class="modul-title">DASHBOARD</h2>
        </div>
    </div>

    <div class="row">
        
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-sliders fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge" id="slider-total"></div>
                            <div>Kayıt var</div>
                        </div>
                    </div>
                </div>
                <a href="/admin/sliderModule/slider">
                    <div class="panel-footer">
                        <span class="pull-left"><b>SLIDER</b></span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-users fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge" id="team-total"></div>
                            <div>Kayıt var</div>
                        </div>
                    </div>
                </div>
                <a href="/admin/teamModule/team">
                    <div class="panel-footer">
                        <span class="pull-left"><b>TEAM</b></span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-th fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge" id="article-total"></div>
                            <div>Kayıt var</div>
                        </div>
                    </div>
                </div>
                <a href="/admin/articleModule/article">
                    <div class="panel-footer">
                        <span class="pull-left"><b>MAKALE</b></span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="panel panel-blue">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-database fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge" id="project-total"></div>
                            <div>Kayıt var</div>
                        </div>
                    </div>
                </div>
                <a href="/admin/projectModule/project">
                    <div class="panel-footer">
                        <span class="pull-left"><b>PROJECT</b></span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

    </div>

</div>

<?php include('inc/script.php'); ?>

<script type="text/javascript">
	
	$(document).ready(function(){

		all_statistics();

	});

	function all_statistics()
	{
		$.ajax({
			url : base_url + 'admin/dashboard/ajax_statictic',
	        type: "GET",
	        dataType: "JSON",
	        success: function(data)
	        {
	        	$('#slider-total').text(data.slider_total);
	        	$('#article-total').text(data.article_total);
	        	$('#team-total').text(data.team_total);
	        	$('#project-total').text(data.project_total);
	        },
	        error: function(jqXHR, textStatus, errorThrown)
	        {
	        	alert('Error get data from ajax');
	        }
		});
	}

</script>

<?php include('inc/footer.php'); ?>