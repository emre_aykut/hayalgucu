<?php include(dirname(__DIR__) . '/inc/header.php'); ?>
    
    <div class="form-content">   
        
        <div class="row margin-bottom-25">
            <div class="col-md-12">
                <h2 class="modul-title">PROJECT MODÜLÜ</h2>
            </div>
            <div class="col-md-8">
                <button class="btn btn-success" onclick="add_project()"><i class="glyphicon glyphicon-plus"></i> Project Ekle</button>
                <a href="/admin/projectModule/project_category" class="btn btn-warning" ><i class="glyphicon glyphicon-share"></i> Kategoriler</a>
            </div>
            <div class="col-md-4">
                <select class="form-control font-bold" id="lang">
                    
                </select>
            </div>
        </div>
        
        <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Başlık</th>
                        <th>Kategori</th>
                        <th>Durumu</th>
                        <th class="width-150">İşlem</th>
                    </tr>
                </thead>

                <tbody>
                </tbody>

                <tfoot>
                <tr>
                    <th>Başlık</th>
                    <th>Kategori</th>
                    <th>Durumu</th>
                    <th class="width-150">İşlem</th>
                </tr>
                </tfoot>
            </table>
        </div>
 
    </div>


<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

<script type="text/javascript">

var save_method;
var table;
var language_count = 0;
var default_language;
var languages = [];
ajax_languages();
ajax_categories();

$(document).ready(function() {

    table = $('#table').DataTable({ 

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Turkish.json"
        },

        "processing": true,
        "serverSide": true, 
        "order": [],

        "ajax": {
            "url": base_url + 'admin/projectModule/project/ajax_list',
            "type": "POST"
        },

        "columnDefs": [
            { 
                "targets": [ -1 ],
                "orderable": false,
            },
            {
                "targets": [ -2 ],
                "render": function ( data, type, row, meta ) {
                    if (data == 1) {
                        return '<span class="label label-success">Aktif</span>'
                    }else{
                        return '<span class="label label-danger">Pasif</span>'
                    }
                }
            },
        ],

    });

    for (var i = 1; i <= languages.length; i++)
    {
        var lang_selected = '';
        var lang_active = '';
        var lang_activeID = '';
        var lang_in_active_defaultActive = '';
        var common_img_status = 0;
        
        if (default_language == languages[language_count].id) 
        {
            lang_selected = 'selected';
            lang_active = 'active';
            lang_activeID = 'activeID';
            lang_in_active_defaultActive = 'in active defaultActive';
        }

        if (i > 1) 
        {
            common_img_status = 1;
        }

        $('#lang').append('<option value="'+languages[language_count].id+'" '+lang_selected+'>'+languages[language_count].name+'</option>');

        $('#language-nav').append(
            '<li class="'+lang_active+'" id="'+lang_activeID+'">' +
                '<a data-toggle="tab" href="#tab-'+languages[language_count].code+'">'+languages[language_count].name+'</a>' +
            '</li>'
        );

        $('#form-content-lang').append(
            '<div id="tab-'+languages[language_count].code+'" class="tab-pane fade '+lang_in_active_defaultActive+' ">' +
                '<div class="panel-body">' +

                    '<div class="form-body">' +
                        
                        '<input type="hidden" value="" name="id['+i+']"/>' +
                        '<input type="hidden" name="lang_id['+i+']" value="'+languages[language_count].id+'">' +

                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Başlık</label>' +
                            '<div class="col-md-9">' +
                                '<input name="title['+i+']" placeholder="Başlık" class="form-control title_class" data-id="'+i+'" type="text" id="title_'+i+'" >' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Link</label>' +
                            '<div class="col-md-9">' +
                                '<input name="link['+i+']" placeholder="Link" class="form-control link_class" type="text" id="link_'+i+'" >' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Kategori</label>' +
                            '<div class="col-md-9">' +
                                '<select class="form-control category_id" id="category_'+i+'" name="category_id['+i+']">' +
                                    '<option value="0" >Seçiniz...</option>' +
                                '</select>' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Açıklama</label>' +
                            '<div class="col-md-9">' +
                                '<textarea name="description['+i+']" placeholder="Açıklama" class="form-control"></textarea>' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Detay</label>' +
                            '<div class="col-md-9">' +
                                '<textarea name="detail['+i+']" id="detail_'+i+'" placeholder="Detay" class="form-control"></textarea>' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Meta Title</label>' +
                            '<div class="col-md-9">' +
                                '<input name="meta_title['+i+']" placeholder="Meta Title" class="form-control" type="text">' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Meta Keywords</label>' +
                            '<div class="col-md-9">' +
                                '<input name="meta_keywords['+i+']" placeholder="Meta Keywords" class="form-control" type="text">' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Meta Description</label>' +
                            '<div class="col-md-9">' +
                                '<input name="meta_description['+i+']" placeholder="Meta Description" class="form-control" type="text">' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Sıralama</label>' +
                            '<div class="col-md-9">' +
                                '<input name="sort['+i+']" placeholder="Sort" class="form-control common-sort" type="text">' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-area-border common-img-'+common_img_status+'">' +
                            '<div class="form-group">' +
                                '<label class="control-label col-md-3" id="label-image">Galeri</label>' +
                                '<div class="col-md-9">' +
                                    '<input name="image_'+i+'[]" id="image_'+i+'" data-image-id="'+i+'" type="file" class="image-class gallery-img" multiple>' +
                                    '<span class="help-block"></span>' +
                                '</div>' +
                                '<div class="container full-width-class">' +
                                    '<div id="images-area'+i+'" class="images-area">' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-area-border common-img-'+common_img_status+'">' +
                            '<div class="form-group photo-class margin-bottom-0" id="photo-preview-'+i+'">' +
                                '<label class="control-label col-md-3"></label>' +
                                '<div class="col-md-9">' +
                                    '(Fotoğraf Yok)' +
                                    '<span class="help-block"></span>' +
                                '</div>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label class="control-label col-md-3" id="label-photo">Öne Çıkan Görsel </label>' +
                                '<div class="col-md-9">' +
                                    '<input name="default_photo_'+i+'" type="file" class="image-class">' +
                                    '<span class="help-block"></span>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group" style="display:none">' +
                            '<div class="col-md-3"></div>' +
                            '<div class="col-md-9">' +
                                '<label class="font-bold common-img ">' +
                                '   <input type="checkbox" name="common_img" value="1">' +
                                    ' Tüm dillerde ortak görsel/görseller kullanılsın' +
                                '</label>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Yayınlama</label>' +
                            '<div class="col-md-9">' +
                                '<label class="switch">' +
                                    '<input type="checkbox" name="status['+i+']" id="status_'+i+'" class="status common-status" value="1" checked>' +
                                    '<span class="slider round"></span>' +
                                    '<span class="status-active"> Aktif</span>' +
                                    '<span class="status-passive"> Pasif</span>' +
                                '</label>' +
                            '</div>' +
                        '</div>' +
                        
                    '</div>' +

                '</div>' +
            '</div>'
        );

        for (var m = 0; m < categories.length; m++)
        {
            if (categories[m].lang_id == languages[language_count].id) {
                $('#category_'+i).append('<option value="'+categories[m].content_id+'">'+categories[m].title+'</option>');
            }
        }

        CKEDITOR.replace('detail['+i+']',{
            filebrowserWindowWidth: '900',
            filebrowserWindowHeight: '700',
            filebrowserBrowseUrl: base_url + 'assets/plugins/ckfinder/ckfinder_.php',
            filebrowserImageBrowseUrl: base_url + 'assets/plugins/ckfinder/ckfinder_.php?Type=Images',
            filebrowserUploadUrl: base_url + 'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: base_url + 'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images'
        }); 

        language_count++;
    }

    $('.link_class').prop('readonly', true);
    $('.title_class').on('keyup', function() {
        var data_id = $(this).attr('data-id');
        var str = $(this).val();
        str = replace_special_chars(str);
        str = str.toLowerCase();
        str = str.replace( /\s\s+/g, ' ' ).replace(/[^a-z0-9\s]/gi, '').replace(/[^\w]/ig, '-');
        $('#link_'+data_id).val(str);
    });

    $('.gallery-img').change(function(){

        var gallery_id = $(this).attr('data-image-id');

        var formData = new FormData($('#form')[0]);
    
        $.ajax({
            url : base_url + 'admin/image_upload_ajax/images_upload/' + 'image_' + gallery_id,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data)
            {
                for(var x = 0; x < data.length; x++)
                {
                    var imgClass = data[x].file_name.split('.');
                    $('#images-area'+gallery_id).append(
                        '<div class="col-md-6 col-sm-6 col-xs-6 margin-bottom-10 image-area-class '+imgClass[0]+'">' +
                            '<div class="img-area">' +
                                '<div class="img-close close'+gallery_id+'" id="'+imgClass[0]+'" >X</div>' +
                                '<div class="img-class">' +
                                    '<img src="'+base_url+upload_folder+data[x].file_name+'" class="center-block" width="120" />' +
                                    '<input type="hidden" name="image_src['+gallery_id+']['+imgClass[0]+']" value="'+data[x].file_name+'" />' +
                                '</div>' +
                                '<div class="image-text">' +
                                    '<input type="text" name="image_text['+gallery_id+']['+imgClass[0]+']" placeholder="Görsel içeriği" class="form-control" />' +
                                '</div>' +
                                '<div class="image-sort">' +
                                    '<input type="text" name="image_sort['+gallery_id+']['+imgClass[0]+']" placeholder="Sıra" class="form-control" />' +
                                '</div>' +
                            '</div>' +
                        '</div>'
                    );
                }

                $('.close'+gallery_id).click(function(){
                    var imgId = $(this).attr('id');
                    $('.'+imgId).remove();
                });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    });

    $(document).on('change','#lang',function(){
        var lang_id = $(this).val();
        table.ajax.url(base_url + 'admin/projectModule/project/ajax_list/' + lang_id).load();
        $('#lang_id option').removeAttr('selected')
        $('#lang_id #l_' + lang_id).attr('selected','selected');
    });

    $('input').change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $('textarea').change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $('select').change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $('.category_id').change(function() {
        var new_value = $(this).val();
        $('.category_id').val(new_value);
    });
    $('.common-sort').change(function() {
        var new_value = $(this).val();
        $('.common-sort').val(new_value);
    });
    $('.common-img input[type="checkbox"]').change(function() {
        if($(this).is(":checked")) 
        {
            $('.common-img-1').css('display','none');
            $('.common-img input[type="checkbox"]').click();
        }
        else
        {
            $('.common-img-1').css('display','block');
            $('.common-img input[type="checkbox"]').click();
        }      
    });


});

function ajax_categories()
{
    $.ajax({
        url : base_url + 'admin/projectModule/project_category/ajax_category_get',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            categories = data.categories;
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        },
        async: false,
    });
}

function add_project()
{
    save_method = 'add';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty(); 
    $('#modal_form').modal('show');
    $('.modal-title').text('Project Ekle');
    $('#errorMessage').text('');
    $('.photo-class').hide();
    $('.image-area-class').remove();
    $('.nav-tabs li').removeClass('active'); 
    $('#activeID').addClass('active');
    $('.tab-pane').removeClass('active'); 
    $('.defaultActive').addClass('in active');
    $('.common-status').attr('checked','checked');
    $('.common-img input[type="checkbox"]').removeAttr('checked');
    $('.common-img-1').css('display','block');

    for (var i = 1; i <= languages.length; i++)
    {
        $('[name="detail['+i+']"]').val(CKEDITOR.instances['detail_'+i].setData(''));
    }
}

function edit_project(id)
{
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#errorMessage').text('');
    $('.image-area-class').remove();

    $.ajax({
        url : base_url + 'admin/projectModule/project/ajax_edit/' + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            for (var i = 1; i <= data.total_record; i++) 
            {   
                $('[name="id['+i+']"]').val(data[i].id);
                $('[name="content_id"]').val(data[i].content_id);
                $('[name="title['+i+']"]').val(data[i].title);
                $('[name="link['+i+']"]').val(data[i].link);
                $('[name="description['+i+']"]').val(data[i].description);
                CKEDITOR.instances['detail_'+i].setData(data[i].detail);
                $('[name="sort['+i+']"]').val(data[i].sort);
                $('[name="meta_title['+i+']"]').val(data[i].meta_title);
                $('[name="meta_description['+i+']"]').val(data[i].meta_description);
                $('[name="meta_keywords['+i+']"]').val(data[i].meta_keywords);
                $('[name="category_id['+i+']"]').val(data[i].category_id);
                $('[name="lang_id['+i+']"]').val(data[i].lang_id);

                if (data[i].status == 0) 
                {
                    $('#status_'+ i).removeAttr('checked');
                }
                else if(data[i].status == 1)
                {
                    $('#status_'+ i).attr('checked','checked');
                }

                if (data[i].common_img == 0) 
                {
                    $('.common-img-1').css('display','block');
                    $('.common-img input[type="checkbox"]').removeAttr('checked');
                }
                else if(data[i].common_img == 1)
                {
                    $('.common-img-1').css('display','none');
                    $('.common-img input[type="checkbox"]').attr('checked', 'checked');
                }

                $('#modal_form').modal('show'); 
                $('.modal-title').text('Project Düzenle');
                $('.nav-tabs li').removeClass('active'); 
                $('#activeID').addClass('active');
                $('.tab-pane').removeClass('active'); 
                $('.defaultActive').addClass('in active');
                $('.photo-class').show(); 

                if(data[i].default_photo)
                {
                    $('#photo-preview-' +i + ' div').html('<img src="'+base_url+upload_folder+data[i].default_photo+'" class="img-responsive" width="100">'); 
                    $('#photo-preview-' +i + ' div').append('<input type="checkbox" name="remove_photo_'+i+'" value="'+data[i].default_photo+'"/> Kaydederken fotoğrafı kaldır'); 
                }
                else
                {
                    $('#photo-preview-' +i + ' div').text('(Fotoğraf Yok)');
                }

                $.ajax({
                    url : base_url + 'admin/projectModule/project/ajax_image_get/' + data[i].id,
                    type: "GET",
                    dataType: "JSON",
                    success: function(dataImg)
                    {
                        if (dataImg != null) 
                        {
                            for(var x = 0; x < dataImg.length; x++)
                            {
                                var langID = dataImg[x].lang_id;
                                var imgClass = dataImg[x].image.split('.');
                                $('#images-area'+langID).append(
                                    '<div class="col-md-6 col-sm-6 col-xs-6 margin-bottom-10 image-area-class '+imgClass[0]+'">' +
                                        '<div class="img-area">' +
                                            '<div class="img-close close'+x+'" id="'+imgClass[0]+'" >X</div>' +
                                            '<div class="img-class">' +
                                                '<img src="'+base_url+upload_folder+dataImg[x].image+'" class="center-block" width="120" />' +
                                                '<input type="hidden" name="image_src['+langID+']['+imgClass[0]+']" value="'+dataImg[x].image+'" />' +
                                            '</div>' +
                                            '<div class="image-text">' +
                                                '<input type="text" name="image_text['+langID+']['+imgClass[0]+']" value="'+dataImg[x].title+'" placeholder="Görsel içeriği" class="form-control" />' +
                                            '</div>' +
                                            '<div class="image-sort">' +
                                                '<input type="text" name="image_sort['+langID+']['+imgClass[0]+']" value="'+dataImg[x].sort+'" placeholder="Sıra" class="form-control" />' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>'
                                );

                                $('.close'+x).click(function(){
                                    var imgId = $(this).attr('id');
                                    $('.'+imgId).remove();
                                });
                            }
                        }
                     
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error get data from ajax');
                    }
                });

                

            }


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false);
}

function save(preview = '')
{
    var url;

    $('#btnSave').text('kaydediyor...');
    $('#btnSave').attr('disabled',true);

    if (preview == 'preview' && save_method == 'add') 
    {
        $('.common-status').removeAttr('checked');
    }
    
    for (var i = 1; i <= languages.length; i++)
    {
        $('[name="detail['+i+']"]').val(CKEDITOR.instances['detail_'+i].getData());
    }

    if(save_method == 'add') 
    {
        url = base_url + 'admin/projectModule/project/ajax_add';
        notify_message = 'Kayıt başarılı bir şekilde eklendi.';
        notify_type = 'success';
    } 
    else
    {
        url = base_url + 'admin/projectModule/project/ajax_update';
        notify_message = 'Kayıt başarılı bir şekilde düzenlendi.';
        notify_type = 'info';
    }

    var formData = new FormData($('#form')[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) 
            {

                if (preview == 'preview') 
                {
                    $('#iframe-area').addClass('iframe-area-class');
                    $('#iframe-area').append('<a class="iframe-area-close btn btn-danger">Kapat</a>');
                    $('#iframe-area').append('<center class="iframe-center"><iframe src="'+base_url+'project/detail/'+data.content_id+'/0" width="90%" height="95%"></iframe></center>');

                    $('.iframe-area-close').click(function(){
                        $('#iframe-area').removeClass('iframe-area-class');
                        $('.iframe-center').remove();
                        $('.iframe-area-close').remove();
                        edit_project(data.content_id);
                    });
                }
                else
                {
                    reload_table();
                    $('#modal_form').modal('hide');
                    $('.nav-tabs li').removeClass('active');
                    $('.tab-pane').removeClass('active'); 
                    $('#activeID').addClass('active');
                    $('.defaultActive').addClass('in active');

                    $.notify({
                        icon: 'glyphicon glyphicon-ok',
                        message: notify_message
                    },{
                        type: notify_type,
                        offset: {
                            x: 60,
                            y: 120
                        },
                        animate: {
                            enter: 'animated fadeInRight',
                            exit: 'animated fadeOutRight'
                        }
                    });
                }

                $('#errorMessage').text('');
                
            }
            else
            {   
                
                for (var i = 0; i < data.inputerror.length; i++) 
                {   
                    $('[name="'+data.inputerror[i][data.lang[i]]+'['+data.lang[i]+']"]').parent().parent().addClass('has-error'); 
                    $('[name="'+data.inputerror[i][data.lang[i]]+'['+data.lang[i]+']"]').next().text(data.error_string[i][data.lang[i]]); 
                }
                
                $('#errorMessage').text('*Gerekli alanları doldurmadan işleme devam edemezsiniz.');
                $('#modal_form').stop().animate({
                    scrollTop:0
                });

            }
           
            $('#btnSave').text('Kaydet'); 
            $('#btnSave').attr('disabled',false);
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('Kaydet'); 
            $('#btnSave').attr('disabled',false);
        }
    });

}

function delete_project(id)
{
    $.ajax({
        url : base_url + 'admin/projectModule/project/ajax_delete/' + id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            $('#modal_form').modal('hide');
            reload_table();

            $.notify({
                icon: 'glyphicon glyphicon-remove',
                message: 'Kayıt silindi.' 
            },{
                type: 'danger',
                offset: {
                    x: 60,
                    y: 120
                },
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                }
            });

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error deleting data');
        }
    });
}

</script>

<div id="iframe-area"></div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Project Formu</h3>
            </div>
            <div class="modal-body form">

                <div id="errorMessage"></div>

                <ul class="nav nav-tabs" id="language-nav">
                    
                </ul>

                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">

                    <input type="hidden" value="" name="content_id"/> 

                    <div class="tab-content" id="form-content-lang">

                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <!-- <button type="button" id="btnPreview" onclick="save('preview')" class="btn btn-info pull-left">Önizleme</button> -->
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Kaydet</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">İptal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>