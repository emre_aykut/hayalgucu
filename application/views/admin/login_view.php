<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>ADMIN PANEL</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,vietnamese">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/css/font-awesome.css" />

    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/datatables/css/dataTables.bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.min.css')?>" rel="stylesheet">

    <!-- CSS Files -->
    <link href="<?php echo base_url(); ?>assets/admin/css/bootstrap-select.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/admin/css/panel-kit.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/admin/css/slick.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/admin/css/demo.css" rel="stylesheet" />

    <script src="<?php echo base_url('assets/plugins/jquery/jquery-2.1.4.min.js')?>"></script>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div class="main-wrap">

    <header class="panel-header">
        <div class="container-fluid">
            
            <div class="panel-logo">
                <h2><a href="#">LOGO</a></h2>
            </div>

        </div>
    </header>

    <div class="login-page">

        <div class="login-content">

            <form class="login-form" action="<?php echo base_url(); ?>admin/auth/login" method="post">

                <h2 class="navyblue-txt">Giriş Sayfası</h2>

                <?php if ($status == FALSE): ?>
                <div class="alert alert-danger ">
                    <button class="close" data-close="alert"></button>
                    <span>Veritabanında böyle bir kullanıcı yok</span>
                </div>
                <?php endif ?>

                <div class="form-group <?php echo form_error('username') ? 'has-error' : '' ?>">
                    
                    <label class="navyblue-txt" >Kullanıcı Adı</label>
                    
                    <span id="username-error" class="help-block"><?php echo form_error('username', '<span>', '</span>'); ?></span>

                    <input type="text" name="username" value="<?php echo set_value('username'); ?>" placeholder="Username" class="form-control focused">

                </div>

                <div class="form-group <?php echo form_error('password') ? 'has-error' : '' ?>">

                    <label class="navyblue-txt" >Password</label>

                    <span id="username-error" class="help-block"><?php echo form_error('password', '<span>', '</span>'); ?></span>

                    <input type="password" name="password" placeholder="Password" class="form-control focused">

                </div>

                <div>
                    <button type="submit" class="adminpanel-btn">
                        Login
                    </button>
                </div>

            </form>

        </div>

    </div>

</div>


</body>

<!--   Core JS Files   -->
<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/panel.js" type="text/javascript"></script>

</html>
