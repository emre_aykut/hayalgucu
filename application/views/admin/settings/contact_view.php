<?php include(dirname(__DIR__) . '/inc/header.php'); ?>
    
    <div class="form-content">   
        
        <div class="row margin-bottom-25">
            <div class="col-md-12">
                <h2 class="modul-title">İLETİŞİM FORMU</h2>
            </div>
        </div>
        
        <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>İsim Soyisim</th>
                        <th>Email</th>
                        <th class="width-150">İşlem</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>

                <tfoot>
                    <tr>
                        <th>İsim Soyisim</th>
                        <th>Email</th>
                        <th class="width-150">İşlem</th>
                    </tr>
                </tfoot>
            </table>
        </div>
 
    </div>


<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

<script type="text/javascript">

var save_method;
var table;

$(document).ready(function() {

    table = $('#table').DataTable({ 

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Turkish.json"
        },

        "processing": true,
        "serverSide": true, 
        "order": [],

        "ajax": {
            "url": base_url + 'admin/settings/contact/ajax_list',
            "type": "POST"
        },

        "columnDefs": [
            { 
                "targets": [ -1 ],
                "orderable": false,
            },
        ],

    });

    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});



function add_contact()
{
    save_method = 'add';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty(); 
    $('#modal_form').modal('show');
    $('.modal-title').text('İletişim Ekle');
}

function edit_contact(id)
{
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();


    $.ajax({
        url : base_url + 'admin/settings/contact/ajax_edit/' + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id"]').val(data.id);
            $('[name="fullname"]').val(data.fullname);
            $('[name="email"]').val(data.email);
            $('[name="phone"]').val(data.phone);
            $('[name="message"]').val(data.message);
            
            if (data.status == 0) {
                $('#status_0').attr('checked','checked');
                $('#status_1').removeAttr('checked');
            }else if(data.status == 1){
                $('#status_1').attr('checked','checked');
                $('#status_0').removeAttr('checked');
            }

            $('#modal_form').modal('show'); 
            $('.modal-title').text('İletişim Düzenle');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false);
}

function save()
{
    $('#btnSave').text('kaydediyor...');
    $('#btnSave').attr('disabled',true);
    var url, notify_message, notify_type;

    if(save_method == 'add') {
        url = base_url + 'admin/settings/contact/ajax_add';
        notify_message = 'Kayıt başarılı bir şekilde eklendi.';
        notify_type = 'success';
    } else {
        url = base_url + 'admin/settings/contact/ajax_update';
        notify_message = 'Kayıt başarılı bir şekilde düzenlendi.';
        notify_type = 'info';
    }

    var formData = new FormData($('#form')[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) 
            {
                $('#modal_form').modal('hide');
                reload_table();

                $.notify({
                    icon: 'glyphicon glyphicon-ok',
                    message: notify_message
                },{
                    type: notify_type,
                    offset: {
                        x: 60,
                        y: 120
                    },
                    animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                    }
                });
                
            } else{
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                }
            }
            $('#btnSave').text('Kaydet'); 
            $('#btnSave').attr('disabled',false);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('Kaydet'); 
            $('#btnSave').attr('disabled',false);

        }
    });
}

function delete_contact(id)
{
    if(confirm('İçerik silinsin mi?'))
    {
        $.ajax({
            url : base_url + 'admin/settings/contact/ajax_delete/' + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                $('#modal_form').modal('hide');
                reload_table();

                $.notify({
                    icon: 'glyphicon glyphicon-remove',
                    message: 'Kayıt silindi.' 
                },{
                    type: 'danger',
                    offset: {
                        x: 60,
                        y: 120
                    },
                    animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                    }
                });

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}



</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Kullanıcı Formu</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">İsim Soyisim</label>
                            <div class="col-md-9">
                                <input name="fullname" placeholder="İsim Soyisim" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-9">
                                <input name="email" placeholder="Email" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Telefon</label>
                            <div class="col-md-9">
                                <input name="phone" placeholder="Telefon" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Mesaj</label>
                            <div class="col-md-9">
                                <textarea name="message" id="message" placeholder="Mesaj" class="form-control"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Kaydet</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">İptal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>