<?php include(dirname(__DIR__) . '/inc/header.php'); ?>
    
    <div class="form-content">   
        
        <div class="row margin-bottom-25">
            <div class="col-md-12">
                <h2 class="modul-title">DİL MODÜLÜ</h2>
                <button class="btn btn-success" onclick="add_language()"><i class="glyphicon glyphicon-plus"></i> Dil Ekle</button>
                <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Yenile</button>
            </div>
        </div>
        
        <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Dil</th>
                        <th>Kod</th>
                        <th>Durumu</th>
                        <th>Kullanılan</th>
                        <th>Fotoğraf</th>
                        <th class="width-150">İşlem</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>

                <tfoot>
                    <tr>
                        <th>Dil</th>
                        <th>Kod</th>
                        <th>Durumu</th>
                        <th>Kullanılan</th>
                        <th>Fotoğraf</th>
                        <th class="width-150">İşlem</th>
                    </tr>
                </tfoot>
            </table>
        </div>
 
    </div>


<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

<script type="text/javascript">

var save_method;
var table;

$(document).ready(function() {

    table = $('#table').DataTable({ 

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Turkish.json"
        },

        "processing": true,
        "serverSide": true, 
        "order": [],

        "ajax": {
            "url": base_url + 'admin/settings/language/ajax_list',
            "type": "POST"
        },

        "columnDefs": [
            { 
                "targets": [ -1 ],
                "orderable": false,
            },
            { 
                "targets": [ -2 ], 
                "orderable": false,
            },
            {
			    "targets": [ -3 ],
			    "render": function ( data, type, row, meta ) {
			    	if (data == 1) {
			    		return '<span class="label label-success">Kullanılıyor</span>'
			    	}else{
			    		return '<span class="label label-danger">Kullanılmıyor</span>'
			    	}
			    }
		    },
		    {
			    "targets": [ -4 ],
			    "render": function ( data, type, row, meta ) {
			    	if (data == 1) {
			    		return '<span class="label label-success">Aktif</span>'
			    	}else{
			    		return '<span class="label label-danger">Pasif</span>'
			    	}
			    }
		    }

        ],

    });

    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});



function add_language()
{
    save_method = 'add';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty(); 
    $('#modal_form').modal('show');
    $('.modal-title').text('Add language');
    $('#photo-preview').hide();
    $('#label-photo').text('Upload Photo');
    $('#default_lang_1').removeAttr('checked');
    $('#default_lang_0').attr('checked','checked');
    $('#status_0').removeAttr('checked');
    $('#status_1').attr('checked','checked');
}

function edit_language(id)
{
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();

    $.ajax({
        url : base_url + 'admin/settings/language/ajax_edit/' + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id"]').val(data.id);
            $('[name="name"]').val(data.name);
            $('[name="code"]').val(data.code);
            
            if (data.status == 0) {
                $('#status_0').attr('checked','checked');
                $('#status_1').removeAttr('checked');
            }else if(data.status == 1){
                $('#status_1').attr('checked','checked');
                $('#status_0').removeAttr('checked');
            }

            if (data.default_lang == 0) {
            	$('#default_lang_0').attr('checked','checked');
                $('#default_lang_1').removeAttr('checked');
            }else if(data.default_lang == 1){
            	$('#default_lang_1').attr('checked','checked');
                $('#default_lang_0').removeAttr('checked');
            }

            $('#modal_form').modal('show'); 
            $('.modal-title').text('Edit language');

            $('#photo-preview').show(); 

            if(data.photo)
            {
                $('#label-photo').text('Change Photo');
                $('#photo-preview div').html('<img src="'+base_url+'upload/'+data.photo+'" class="img-responsive">'); 
                $('#photo-preview div').append('<input type="checkbox" name="remove_photo" value="'+data.photo+'"/> Kaydederken fotoğrafı kaldır'); 

            }
            else
            {
                $('#label-photo').text('Upload Photo');
                $('#photo-preview div').text('(No photo)');
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false);
}

function save()
{
    $('#btnSave').text('kaydediyor...');
    $('#btnSave').attr('disabled',true);
    var url;

    if(save_method == 'add') {
        url = base_url + 'admin/settings/language/ajax_add';
        notify_message = 'Kayıt başarılı bir şekilde eklendi.';
        notify_type = 'success';
    } else {
        url = base_url + 'admin/settings/language/ajax_update';
        notify_message = 'Kayıt başarılı bir şekilde düzenlendi.';
        notify_type = 'info';
    }

    var formData = new FormData($('#form')[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) 
            {
                $('#modal_form').modal('hide');
                reload_table();

                $.notify({
                    icon: 'glyphicon glyphicon-ok',
                    message: notify_message
                },{
                    type: notify_type,
                    offset: {
                        x: 60,
                        y: 120
                    },
                    animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                    }
                });
                
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                }
            }
            $('#btnSave').text('Kaydet'); 
            $('#btnSave').attr('disabled',false);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('Kaydet'); 
            $('#btnSave').attr('disabled',false);

        }
    });
}

function delete_language(id)
{
    if(confirm('İçerik silinsin mi?'))
    {
        $.ajax({
            url : base_url + 'admin/settings/language/ajax_delete/' + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                $('#modal_form').modal('hide');
                reload_table();

                $.notify({
                    icon: 'glyphicon glyphicon-remove',
                    message: 'Kayıt silindi.' 
                },{
                    type: 'danger',
                    offset: {
                        x: 60,
                        y: 120
                    },
                    animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                    }
                });
                
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}



</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Dil Formu</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Dil</label>
                            <div class="col-md-9">
                                <input name="name" placeholder="Dil" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kod</label>
                            <div class="col-md-9">
                                <input name="code" placeholder="Kod" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Durum</label>
                            <div class="col-md-9">
                            	<div class="radio">
								  <label><input type="radio" name="status" id="status_1" value="1" checked>Aktif</label>
								</div>
								<div class="radio">
								  <label><input type="radio" name="status" id="status_0" value="0">Pasif</label>
								</div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Başlangıç dili olsun mu?</label>
                            <div class="col-md-9">
                            	<div class="radio">
								  <label><input type="radio" name="default_lang" id="default_lang_1" value="1" checked>Evet</label>
								</div>
								<div class="radio">
								  <label><input type="radio" name="default_lang" id="default_lang_0" value="0">Hayır</label>
								</div>
                            </div>
                        </div>
                        <div class="form-group" id="photo-preview">
                            <label class="control-label col-md-3">Fotoğraf</label>
                            <div class="col-md-9">
                                (Fotoğraf Yok)
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" id="label-photo">Fotoğraf Yükle </label>
                            <div class="col-md-9">
                                <input name="photo" type="file">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Kaydet</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">İptal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>