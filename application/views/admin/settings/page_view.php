<?php include(dirname(__DIR__) . '/inc/header.php'); ?>

    <div class="form-content">   
        
        <div class="row margin-bottom-25">
            <div class="col-md-12">
                <h2 class="modul-title">SAYFA MODÜLÜ</h2>
            </div>
            <div class="col-md-8">
                <button class="btn btn-success" onclick="add_page()"><i class="glyphicon glyphicon-plus"></i> Sayfa Ekle</button>
                <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Yenile</button>
            </div>
            <div class="col-md-4">
                <select class="form-control font-bold" id="lang">
                   
                </select>
            </div>
        </div>
        
        <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Başlık</th>
                        <th>Link</th>
                        <th>Sıralama</th>
                        <th>Durumu</th>
                        <th>Sabit Mi?</th>
                        <th>Fotoğraf</th>
                        <th class="width-150">İşlem</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>

                <tfoot>
                <tr>
                    <th>Başlık</th> 
                    <th>Link</th>
                    <th>Sıralama</th>
                    <th>Durumu</th>
                    <th>Sabit Mi?</th>
                    <th>Fotoğraf</th>
                    <th class="width-150">İşlem</th>
                </tr>
                </tfoot>
            </table>
        </div>
 
    </div>


<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

<script type="text/javascript">

var save_method;
var table;
var language_count = 0;
var default_language;
var languages = [];
var TreeHtml = '';
ajax_languages();

var extraList = {
    treePagesList : function(activeParentID,columnName, langID, idNumber){

        if(typeof activeParentID == 'undefined'){
            activeParentID = -1;
        }

        $.getJSON(base_url + 'admin/settings/page/tree_pages_list/' + langID ,function(data){
            TreeHtml = '';
            treeRadio(data, 0, columnName, activeParentID, langID, idNumber);
            
            $('.page_li').change(function(){
                var parent_id = $(this).attr('parent_id');
                $('.parent_id_'+parent_id).attr('checked', 'checked');
                console.log(parent_id);
            });

        });     

    }
}


$(document).ready(function() {

    table = $('#table').DataTable({ 

        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Turkish.json"
        },

        "processing": true,
        "serverSide": true, 
        "order": [],

        "ajax": {
            "url": base_url + 'admin/settings/page/ajax_list',
            "type": "POST"
        },

        "columnDefs": [
            { 
                "targets": [ -1 ], 
                "orderable": false,
            },
            { 
                "targets": [ -2 ], 
                "orderable": false,
            },
            { 
                "targets": [ -3 ],
                "orderable": false,
            },
            {
                "targets": [ -3 ],
                "render": function ( data, type, row, meta ) {
                    if (data == 1) {
                        return '<span class="label label-success">Evet</span>'
                    }else{
                        return '<span class="label label-danger">Hayır</span>'
                    }
                }
            },
            {
                "targets": [ -4 ],
                "render": function ( data, type, row, meta ) {
                    if (data == 1) {
                        return '<span class="label label-success">Aktif</span>'
                    }else{
                        return '<span class="label label-danger">Pasif</span>'
                    }
                }
            }
        ],

    });

    for (var i = 1; i <= languages.length; i++)
    {
        var lang_selected = '';
        var lang_active = '';
        var lang_activeID = '';
        var lang_in_active_defaultActive = '';
        if (default_language == languages[language_count].id) 
        {
            lang_selected = 'selected';
            lang_active = 'active';
            lang_activeID = 'activeID';
            lang_in_active_defaultActive = 'in active defaultActive';
        }

        $('#lang').append('<option value="'+languages[language_count].id+'" '+lang_selected+'>'+languages[language_count].name+'</option>');
        //$('#lang_id').append('<option value="'+languages[language_count].id+'" '+lang_selected+'>'+languages[language_count].name+'</option>');
        
         $('#language-nav').append(
            '<li class="'+lang_active+'" id="'+lang_activeID+'">' +
                '<a data-toggle="tab" href="#tab-'+languages[language_count].code+'">'+languages[language_count].name+'</a>' +
            '</li>'
        );

        $('#form-content-lang').append(
            '<div id="tab-'+languages[language_count].code+'" class="tab-pane fade '+lang_in_active_defaultActive+' ">' +
                '<div class="panel-body">' +

                    '<div class="form-body">' +
                        
                        '<input type="hidden" value="" name="id['+i+']"/>' +
                        '<input type="hidden" name="lang_id['+i+']" value="'+languages[language_count].id+'">' +

                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Bağlı Olduğu Sayfa</label>' +
                            '<div class="col-md-9">' +
                                '<ul class="radio-select">' +
                                    '<li class="treeRadio" id="treeRadio'+i+'"></li>' +
                                '</ul>' +
                                '<input name="p_id['+i+']" placeholder="Başlık" class="form-control" type="hidden">' +
                                '<span class="help-block radio-error-text"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Başlık</label>' +
                            '<div class="col-md-9">' +
                                '<input name="title['+i+']" placeholder="Başlık" class="form-control title_class" data-id="'+i+'" type="text" id="title_'+i+'" >' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Link</label>' +
                            '<div class="col-md-9">' +
                                '<input name="link['+i+']" placeholder="Link" class="form-control link_class" type="text" id="link_'+i+'" >' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Açıklama</label>' +
                            '<div class="col-md-9">' +
                                '<textarea name="description['+i+']" placeholder="Açıklama" class="form-control"></textarea>' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Detay</label>' +
                            '<div class="col-md-9">' +
                                '<textarea name="detail['+i+']" placeholder="Detay" class="form-control"></textarea>' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Meta Title</label>' +
                            '<div class="col-md-9">' +
                                '<input name="meta_title['+i+']" placeholder="Meta Title" class="form-control" type="text">' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Meta Keywords</label>' +
                            '<div class="col-md-9">' +
                                '<input name="meta_keywords['+i+']" placeholder="Meta Keywords" class="form-control" type="text">' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Meta Description</label>' +
                            '<div class="col-md-9">' +
                                '<input name="meta_description['+i+']" placeholder="Meta Description" class="form-control" type="text">' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Sıralama</label>' +
                            '<div class="col-md-9">' +
                                '<input name="sort['+i+']" placeholder="Sort" class="form-control common-sort" type="text">' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3">Durum</label>' +
                            '<div class="col-md-9">' +
                                '<div class="radio">' +
                                  '<label><input type="radio" name="status['+i+']" class="common-status status_1" id="status_'+i+'_1" value="1" checked>Aktif</label>' +
                                '</div>' +
                                '<div class="radio">' +
                                  '<label><input type="radio" name="status['+i+']" class="common-status status_0" id="status_'+i+'_0" value="0">Pasif</label>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group photo-class" id="photo-preview-'+i+'">' +
                            '<label class="control-label col-md-3">Fotoğraf</label>' +
                            '<div class="col-md-9">' +
                                '(Fotoğraf Yok)' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="control-label col-md-3" id="label-photo">Fotoğraf Yükle </label>' +
                            '<div class="col-md-9">' +
                                '<input name="photo_'+i+'" type="file">' +
                                '<span class="help-block"></span>' +
                            '</div>' +
                        '</div>' +
                        
                    '</div>' +

                '</div>' +
            '</div>'
        );

        language_count++;
    }

    $(document).on('change','#lang',function(){
        var lang_id = $(this).val();
        table.ajax.url(base_url + 'admin/settings/page/ajax_list/' + lang_id).load();
        $('#lang_id option').removeAttr('selected')
        $('#lang_id #l_' + lang_id).attr('selected','selected');
    });

    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    

    // $('.parent_id_1').click(function(){
    //     $('.parent_id_1').val(0);
    // });

    $('.link_class').prop('readonly', true);
    $('.title_class').on('keyup', function() {
        var data_id = $(this).attr('data-id');
        var str = $(this).val();
        str = replace_special_chars(str);
        str = str.toLowerCase();
        str = str.replace( /\s\s+/g, ' ' ).replace(/[^a-z0-9\s]/gi, '').replace(/[^\w]/ig, '-');
        $('#link_'+data_id).val(str);
    });              

});

function add_page()
{
    save_method = 'add';
    var langVal = $('#lang').val();
    
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty(); 
    $('#modal_form').modal('show');
    $('.modal-title').text('Sayfa Ekle');
    $('#errorMessage').text('');
    $('#photo-preview').hide();
    $('#label-photo').text('Upload Photo');
    $('.link-class').css('display','block');

    $('.nav-tabs li').removeClass('active'); 
    $('#activeID').addClass('active');
    $('.tab-pane').removeClass('active'); 
    $('.defaultActive').addClass('in active');
    $('.status_0').removeAttr('checked');
    $('.status_1').attr('checked','checked');

    for (var i = 1; i <= languages.length; i++)
    {
        extraList.treePagesList('-1','parent_id', languages[i-1].id, i);
    }
}

function edit_page(id)
{
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('input').removeAttr('disabled');
    $('#errorMessage').text('');

    $.ajax({
        url : base_url + 'admin/settings/page/ajax_edit/' + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            for (var i = 1; i <= data.total_record; i++) 
            {   
                extraList.treePagesList(data[i].parent_id, 'parent_id', data[i].lang_id, i);
                $('[name="id['+i+']"]').val(data[i].id);
                $('[name="content_id"]').val(data[i].content_id);
                $('[name="parent_id"]').val(data[i].parent_id);
                $('[name="title['+i+']"]').val(data[i].title);
                $('[name="link['+i+']"]').val(data[i].link);
                $('[name="description['+i+']"]').val(data[i].description);
                $('[name="detail['+i+']"]').val(data[i].detail);
                $('[name="sort['+i+']"]').val(data[i].sort);
                $('[name="meta_title['+i+']"]').val(data[i].meta_title);
                $('[name="meta_description['+i+']"]').val(data[i].meta_description);
                $('[name="meta_keywords['+i+']"]').val(data[i].meta_keywords);
                $('[name="lang_id['+i+']"]').val(data[i].lang_id);
                $('[name="default_page"]').val(data.default_page);

                if (data[i].status == 0) 
                {
                    $('#status_'+ i +'_0').attr('checked','checked');
                    $('#status_'+ i +'_1').removeAttr('checked');
                }
                else if(data[i].status == 1)
                {
                    $('#status_'+ i +'_1').attr('checked','checked');
                    $('#status_'+ i +'_0').removeAttr('checked');
                }

                if (data[i].default_page == 1) 
                {
                    $('.link-class').css('display','none');
                }
                else
                {
                    $('.link-class').css('display','block');
                }

                $('#modal_form').modal('show'); 
                $('.modal-title').text('Project Düzenle');
                $('.nav-tabs li').removeClass('active'); 
                $('#activeID').addClass('active');
                $('.tab-pane').removeClass('active'); 
                $('.defaultActive').addClass('in active');
                $('.photo-class').show(); 

                if(data[i].default_photo)
                {
                    $('#photo-preview-' +i + ' div').html('<img src="'+base_url+upload_folder+data[i].default_photo+'" class="img-responsive" width="100">'); 
                    $('#photo-preview-' +i + ' div').append('<input type="checkbox" name="remove_photo_'+i+'" value="'+data[i].default_photo+'"/> Kaydederken fotoğrafı kaldır'); 
                }
                else
                {
                    $('#photo-preview-' +i + ' div').text('(Fotoğraf Yok)');
                } 

            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false);
}

function save()
{
    $('#btnSave').text('kaydediyor...');
    $('#btnSave').attr('disabled',true);
    var url;

    if(save_method == 'add') 
    {
        url = base_url + 'admin/settings/page/ajax_add';
        notify_message = 'Kayıt başarılı bir şekilde eklendi.';
        notify_type = 'success';
    } else {
        url = base_url + 'admin/settings/page/ajax_update';
        notify_message = 'Kayıt başarılı bir şekilde düzenlendi.';
        notify_type = 'info';
    }

    var formData = new FormData($('#form')[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) 
            {
                $('#modal_form').modal('hide');
                reload_table();
                $('.nav-tabs li').removeClass('active');
                $('.tab-pane').removeClass('active'); 
                $('#activeID').addClass('active');
                $('.defaultActive').addClass('in active');

                $.notify({
                    icon: 'glyphicon glyphicon-ok',
                    message: notify_message
                },{
                    type: notify_type,
                    offset: {
                        x: 60,
                        y: 120
                    },
                    animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                    }
                });

                $('#errorMessage').text('');
            }
            else
            {   
                $('.radio-select').parent().parent().removeClass('has-error');
                $('.radio-error-text').text('');

                for (var i = 0; i < data.inputerror.length; i++) 
                {   
                    $('[name="'+data.inputerror[i][data.lang[i]]+'['+data.lang[i]+']"]').parent().parent().addClass('has-error'); 
                    $('[name="'+data.inputerror[i][data.lang[i]]+'['+data.lang[i]+']"]').next().text(data.error_string[i][data.lang[i]]); 
                }
                
                $('#errorMessage').text('*Gerekli alanları doldurmadan işleme devam edemezsiniz.');
                $('#modal_form').stop().animate({
                    scrollTop:0
                });
            }

            $('#btnSave').text('Kaydet'); 
            $('#btnSave').attr('disabled',false);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('Kaydet'); 
            $('#btnSave').attr('disabled',false);

        }
    });
}

function delete_page(id)
{
    if(confirm('Bu kaydı silerken diğer tüm dillerdeki karşılığı da silinecek. Emin misiniz?'))
    {
        $.ajax({
            url : base_url + 'admin/settings/page/ajax_delete/' + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                $('#modal_form').modal('hide');
                reload_table();

                $.notify({
                    icon: 'glyphicon glyphicon-remove',
                    message: 'Kayıt silindi.' 
                },{
                    type: 'danger',
                    offset: {
                        x: 60,
                        y: 120
                    },
                    animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                    }
                });

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}

function treeRadio(dataArray, parentID, columnName, activeParentID, langID, idNumber)
{
    var is_checked = '';

    if(typeof activeParentID == "undefined"){
        activeParentID = -1;
    }
    
    if(dataArray[parentID]){
        TreeHtml += "<ul>";

        $.each(dataArray[parentID],function(index,value){
            var parent_id = index;
         
            if(activeParentID == parent_id){
               is_checked = 'checked="checked"';
            }else{
                is_checked = '';
            }


            if (langID == value['lang_id']) {

                TreeHtml += '<li>' +
                                '<label>' +    
                                    '<input class="page_li ' + columnName + '_' + parent_id + '" type="radio" parent_id="'+parent_id+'" '+ is_checked +' id="' + columnName + '_' + parent_id + '" name="' + columnName + '['+idNumber+']" value="' + parent_id + '" />' +
                                    value['title'] + 
                                '</label>';
                
                treeRadio(dataArray, parent_id, columnName, activeParentID, langID, idNumber);

                TreeHtml += '</li>';

            }

        });
        TreeHtml += '</ul>';    
    }
    
    $('#treeRadio'+idNumber).html(TreeHtml);
}

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Sayfa Formu</h3>
            </div>

            <div class="modal-body form">

                <div id="errorMessage"></div>

                <ul class="nav nav-tabs" id="language-nav">

                </ul>

                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">

                    <input type="hidden" value="" name="content_id"/>
                    <input type="hidden" value="0" name="default_page"/>

                    <div class="tab-content" id="form-content-lang">

                    </div>

                </form>

            </div>

            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Kaydet</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">İptal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>