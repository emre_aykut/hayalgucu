<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->autorun();
	}

	public function index()
	{
		$this->load->model('setting/page_model', 'page');

		$page_id = 2;
		$get_lang_id = $this->get_lang_id();
		$this->page->lang_id = $get_lang_id;

		$data['page_detail'] = $this->page->page_detail($page_id);

		$this->load->view('frontend/about_view', $data);
	}

}


?>