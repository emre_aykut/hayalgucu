<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->autorun();
	}

	public function index()
	{
		$this->load->model('setting/page_model', 'page');

		$page_id = 7;
		$get_lang_id = $this->get_lang_id();
		$this->page->lang_id = $get_lang_id;

		$data['page_detail'] = $this->page->page_detail($page_id);

		$this->load->view('frontend/contact_view', $data);
	}

	public function ajax_add()
	{
		$this->load->model('contact/contact_model', 'contact');

		$this->_validate();
		
		$data = array(
				'fullname' => $this->input->post('fullname'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'message' => $this->input->post('message'),
				'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
				'ip_address' => $this->input->ip_address(),
			);

		$insert = $this->contact->save($data);

		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('fullname') == '')
		{
			$data['inputerror'][] = 'fullname';
			$data['error_string'][] = 'İsim Soyisim alanı boş bırakılmaz';
			$data['status'] = FALSE;
		}

		if($this->input->post('email') == '')
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'E-posta alanı boş bırakılmaz';
			$data['status'] = FALSE;
		}

		if($this->input->post('message') == '')
		{
			$data['inputerror'][] = 'message';
			$data['error_string'][] = 'Mesaj alanı boş bırakılmaz';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}


?>