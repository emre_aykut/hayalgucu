<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('admin')) {
			redirect(base_url().'admin/auth/login');
			exit();
		}
		$this->load->model('article/article_model','article');
		$this->load->model('article/article_content_relation_model', 'article_content_relation');
	}

	public function index()
	{
		$this->load->view('admin/article/article_view');
	}

	public function ajax_list($lang_id = '')
	{
		if ($lang_id == '') 
		{
			$this->load->model('language/language_model', 'language');
			$lang = $this->language->get_lang_default();
			$lang_id = $lang->id;
		}

		$list = $this->article->get_datatables($lang_id);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $article) 
		{
			$no++;
			$row = array();
			$row[] = $article->title;
			$row[] = $article->author;
			$row[] = $article->publish_date;
			$row[] = $article->status;

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Düzenle" onclick="edit_article('."'".$article->content_id."'".')"><i class="glyphicon glyphicon-pencil"></i> Düzenle</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Sil" onclick="delete_article('."'".$article->content_id."'".')"><i class="glyphicon glyphicon-trash"></i> Sil</a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->article->count_all($lang_id),
						"recordsFiltered" => $this->article->count_filtered($lang_id),
						"data" => $data,
				);
		
		echo json_encode($output);
	}

	public function ajax_edit($content_id)
	{	
		$this->load->model('article/article_tag_relation_model', 'article_tag_relation');
		$this->load->model('article/article_tag_model', 'article_tag');

		$results = $this->article->get_by_content_id($content_id);
		$i = 0;

		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $this->article->get_by_id($value['id']);
			
			$tags = $this->article_tag_relation->get_tags($value['id']);
			$tag = '';
			foreach ($tags as $t) {
				$tag_result = $this->article_tag->get_by_id($t['tag_id']);
				$tag = $tag_result->title . ', ' . $tag;
			}
			$data['tags'][$i] = rtrim($tag,', ');
		}

		$data['total_record'] = $i;
		
		
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->load->library('file_upload');
		$this->load->model('language/language_model', 'language');
		$this->load->model('article/article_tag_model', 'article_tag');
		$this->load->model('article/article_tag_relation_model', 'article_tag_relation');

		$language_count = $this->language->get_count();
		$post = $this->input->post(null,true);

		$this->_validate($language_count);

		$content_id = $this->article_content_relation->save(array( 'modul' => 'article', 'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)));

		for ($i=1; $i <= $language_count; $i++) 
		{ 
			$data = array(
				'title' => $post['title'][$i],
				'description' => $post['description'][$i],
				'detail' => $post['detail'][$i],
				'link' => $post['link'][$i],
				'publish_date' => $post['publish_date'][$i],
				'author_id' => $post['author_id'][$i],
				'category_id' => $post['category_id'][$i],
				'lang_id' => $post['lang_id'][$i],
				'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
				'content_id' => $content_id
			);

			if (!isset($post['status'][$i])) 
			{
				$data['status'] = 0;
			}
			else
			{
				$data['status'] = $post['status'][$i];
			}

			if (!isset($post['common_img'])) 
			{
				$data['common_img'] = 0;
			}
			else
			{
				$data['common_img'] = $post['common_img'];
			}

			$file_name = 'photo_'.$i;
			if(!empty($_FILES[$file_name]['name']))
			{
				$upload = $this->file_upload->do_upload($file_name);
				$data['photo'] = $upload;
			}else{
				$data['photo'] = '';
			}
			
			$insert = $this->article->save($data);

			/*tag add start*/
			$tags = $post['tag'][$i];
			$tag_explode = explode(',', $tags);

			foreach ($tag_explode as $key => $value) 
			{
				if (!empty($value))
				{
					$tag_single = trim($value);

					$is_tag = $this->article_tag->check_tag($tag_single, $data['lang_id']);

					if ($is_tag == false) 
					{
						$data = array(
							'title' => $tag_single,
							'lang_id' => $data['lang_id'],
							'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
						);

						$tag_id = $this->article_tag->save($data);
					}else{
						$tag_id = $is_tag->id;
					}

					$tag_article_data = array(
						'tag_id' => $tag_id,
						'article_id' => $insert,
						'article_content_id' => $content_id,
						'lang_id' => $data['lang_id'],
						'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
					);

					$this->article_tag_relation->save($tag_article_data);
				}
			}
			/*tag add finish*/
			
		}

		echo json_encode(array("status" => TRUE, "content_id" => $content_id));

	}

	public function ajax_update()
	{
		$this->load->library('file_upload');
		$this->load->model('language/language_model', 'language');
		$this->load->model('article/article_tag_model', 'article_tag');
		$this->load->model('article/article_tag_relation_model', 'article_tag_relation');

		$language_count = $this->language->get_count();
		$post = $this->input->post(null,true);

		$this->_validate($language_count);

		$this->article_content_relation->update(array('id' => $post['content_id']), array('updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)));

		for ($i=1; $i <= $language_count; $i++) 
		{ 	
			$data = array(
				'title' => $post['title'][$i],
				'description' => $post['description'][$i],
				'detail' => $post['detail'][$i],
				'link' => $post['link'][$i],
				'publish_date' => $post['publish_date'][$i],
				'author_id' => $post['author_id'][$i],
				'category_id' => $post['category_id'][$i],
				'lang_id' => $post['lang_id'][$i],
				'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
				'content_id' => $post['content_id']
			);

			if (!isset($post['status'][$i])) 
			{
				$data['status'] = 0;
			}
			else
			{
				$data['status'] = $post['status'][$i];
			}

			if (!isset($post['common_img'])) 
			{
				$data['common_img'] = 0;
			}
			else
			{
				$data['common_img'] = $post['common_img'];
			}

			if($this->input->post('remove_photo_'.$i)) 
			{
				if(file_exists(UPLOAD_FOLDER.$this->input->post('remove_photo_'.$i)) && $this->input->post('remove_photo_'.$i))
					unlink(UPLOAD_FOLDER.$post['remove_photo_'.$i]);
				$data['photo'] = '';
			}

			$file_name = 'photo_'.$i;
			if(!empty($_FILES[$file_name]['name']))
			{
				$upload = $this->file_upload->do_upload($file_name);
				$data['photo'] = $upload;
			}

			$this->article->update(array('id' => $post['id'][$i]), $data);

			/*tag add start*/
			$this->article_tag_relation->delete_by_id($post['id'][$i]);

			$tags = $post['tag'][$i];
			$tag_explode = explode(',', $tags);

			foreach ($tag_explode as $key => $value) 
			{	
				if (!empty($value))
				{
					$tag_single = trim($value);

					$is_tag = $this->article_tag->check_tag($tag_single, $data['lang_id']);

					if ($is_tag == false) 
					{
						$data = array(
							'title' => $tag_single,
							'lang_id' => $data['lang_id'],
							'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
						);

						$tag_id = $this->article_tag->save($data);
					}else{
						$tag_id = $is_tag->id;
					}

					$tag_article_data = array(
						'tag_id' => $tag_id,
						'article_id' => $post['id'][$i],
						'article_content_id' => $post['content_id'],
						'lang_id' => $data['lang_id'],
						'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
					);

					$this->article_tag_relation->save($tag_article_data);
				}
			}
			/*tag add finish*/
		}
	

		echo json_encode(array("status" => TRUE, "content_id" => $post['content_id']));
	}

	public function ajax_delete($content_id)
	{	
		$this->load->model('article/article_tag_relation_model', 'article_tag_relation');

		$results = $this->article->get_by_content_id($content_id);

		foreach ($results as $key => $value) 
		{
			$article = $this->article->get_by_id($value['id']);
			if(file_exists(UPLOAD_FOLDER.$article->photo) && $article->photo)
				unlink(UPLOAD_FOLDER.$article->photo);	
		}

		$this->article->delete_by_id($content_id);
		$this->article_content_relation->delete_by_id($content_id);
		$this->article_tag_relation->delete_by_content_id($content_id);

		echo json_encode(array("status" => TRUE));
	}

	private function _validate($language_count = '')
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['lang'] = '';
		$data['status'] = TRUE;

		for ($i=1; $i <= $language_count; $i++) 
		{

			if($this->input->post('title')[$i] == '')
			{
				$data['inputerror'][][$i] = 'title';
				$data['error_string'][][$i] = 'Başlık alanı boş bırakılmaz';
				$data['lang'][] = $i;
				$data['status'] = FALSE;
			}

			if($this->input->post('description')[$i] == '')
			{
				$data['inputerror'][][$i] = 'description';
				$data['error_string'][][$i] = 'Açıklama alanı boş bırakılmaz';
				$data['lang'][] = $i;
				$data['status'] = FALSE;
			}

			if($this->input->post('publish_date')[$i] == '')
			{
				$data['inputerror'][][$i] = 'publish_date';
				$data['error_string'][][$i] = 'Tarih alanı boş bırakılmaz';
				$data['lang'][] = $i;
				$data['status'] = FALSE;
			}

			if($this->input->post('author_id')[$i] == 0)
			{
				$data['inputerror'][][$i] = 'author_id';
				$data['error_string'][][$i] = 'Yazar seçiniz';
				$data['lang'][] = $i;
				$data['status'] = FALSE;
			}

		}		

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
