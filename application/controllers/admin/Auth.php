<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		$data['status'] = TRUE;
		$this->load->view('admin/login_view', $data);
	}

	public function login()
	{
		$post = $this->input->post(null,true);

		if ($post) 
		{
			$this->load->model('setting/user_model', 'user');
			$this->load->library('form_validation');

			$data['status'] = TRUE;

			$this->form_validation->set_rules('username', 'Kullanıcı Adı', 'trim|required|min_length[3]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]');

			$this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
			$this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');

			if ($this->form_validation->run() == FALSE) {
			
				$this->load->view('admin/login_view', $data);			
			
			}
			else
			{
				$this->user->username = $post['username'];
				$this->user->password = md5($post['password']);
				$user = $this->user->validate();
				if($user){
				$this->session->set_userdata('admin',$user[0]);
					redirect(base_url().'admin/dashboard');
				}else{
					$data['status'] = FALSE;
					$this->load->view('admin/login_view', $data);
				}

			}

		}
		else
		{
			$data['status'] = TRUE;
			$this->load->view('admin/login_view', $data);

		}

	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url().'admin/auth/login');
	}


}



?>