<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('admin')) {
			redirect(base_url().'admin/auth/login');
			exit();
		}
		$this->load->model('language/language_model','language');
	}

	public function index()
	{
		$this->load->view('admin/settings/language_view');
	}

	public function ajax_list()
	{
		$list = $this->language->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $language) {
			$no++;
			$row = array();
			$row[] = $language->name;
			$row[] = $language->code;
			$row[] = $language->status;
			$row[] = $language->default_lang;
			if($language->photo)
				$row[] = '<a href="'.base_url(UPLOAD_FOLDER.$language->photo).'" target="_blank"><img src="'.base_url(UPLOAD_FOLDER.$language->photo).'" class="img-responsive" style="height:50px" /></a>';
			else
				$row[] = '(Fotoğraf Yok)';

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Düzenle" onclick="edit_language('."'".$language->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Düzenle</a>
				<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Sil" onclick="delete_language('."'".$language->id."'".')"><i class="glyphicon glyphicon-trash"></i> Sil</a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->language->count_all(),
						"recordsFiltered" => $this->language->count_filtered(),
						"data" => $data,
				);
		
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->language->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->load->library('file_upload');

		$this->_validate();
		
		$data = array(
				'name' => $this->input->post('name'),
				'code' => $this->input->post('code'),
				'status' => $this->input->post('status'),
				'default_lang' => $this->input->post('default_lang'),
				'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
			);

		if ($data['default_lang'] == 1) {
			$this->language->save_default_column_zero_all_language();
		}

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->file_upload->do_upload('photo');
			$data['photo'] = $upload;
		}

		$insert = $this->language->save($data);

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$this->load->library('file_upload');

		$this->_validate();
		
		$data = array(
				'name' => $this->input->post('name'),
				'code' => $this->input->post('code'),
				'status' => $this->input->post('status'),
				'default_lang' => $this->input->post('default_lang'),
				'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
			);

		if ($data['default_lang'] == 1) {
			$this->language->save_default_column_zero_all_language();
		}

		$result_data = $this->language->default_lang_find($this->input->post('id'));

		if ($result_data['default_lang'] == 1 && $data['default_lang'] == 0) {
			$this->language->default_lang_change_first();
		}

		if($this->input->post('remove_photo'))
		{
			if(file_exists(UPLOAD_FOLDER.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
				unlink(UPLOAD_FOLDER.$this->input->post('remove_photo'));
			$data['photo'] = '';
		}

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->file_upload->do_upload('photo');
			
			$language = $this->language->get_by_id($this->input->post('id'));
			if(file_exists(UPLOAD_FOLDER.$language->photo) && $language->photo)
				unlink(UPLOAD_FOLDER.$language->photo);

			$data['photo'] = $upload;
		}

		$this->language->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$language = $this->language->get_by_id($id);
		if(file_exists(UPLOAD_FOLDER.$language->photo) && $language->photo)
			unlink(UPLOAD_FOLDER.$language->photo);

		$result_data = $this->language->default_lang_change_first($id);

		$this->language->delete_by_id($id);

		if ($result_data['default_lang'] == 1) {
			$this->language->default_lang_change_random();
		}

		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('name') == '')
		{
			$data['inputerror'][] = 'name';
			$data['error_string'][] = 'Dilin ismi boş bırakılmaz';
			$data['status'] = FALSE;
		}

		if($this->input->post('code') == '')
		{
			$data['inputerror'][] = 'code';
			$data['error_string'][] = 'Kod alanı boş bırakılmaz';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function ajax_language_get()
	{
		$data['languages'] = $this->language->get_list_active();
		$data['default_language'] = $this->language->get_lang_default();

		echo json_encode($data);
	}

}
