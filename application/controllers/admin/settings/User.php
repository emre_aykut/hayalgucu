<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('admin')) {
			redirect(base_url().'admin/auth/login');
			exit();
		}
		$this->load->model('setting/user_model','user');
	}

	public function index()
	{
		$this->load->view('admin/settings/user_view');
	}

	public function ajax_list()
	{
		$list = $this->user->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $user) {
			if ($user->is_admin != 1) {
				$no++;
				$row = array();
				$row[] = $user->fullname;
				$row[] = $user->username;
				$row[] = $user->email;
				$row[] = $user->status;
				if($user->photo)
					$row[] = '<a href="'.base_url(UPLOAD_FOLDER.$user->photo).'" target="_blank"><img src="'.base_url(UPLOAD_FOLDER.$user->photo).'" class="img-responsive" style="height:50px" /></a>';
				else
					$row[] = '(Fotoğraf Yok)';

				$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Düzenle" onclick="edit_user('."'".$user->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Düzenle</a>
					  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Sil" onclick="delete_user('."'".$user->id."'".')"><i class="glyphicon glyphicon-trash"></i> Sil</a>';
			
				$data[] = $row;
			}
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->user->count_all(),
						"recordsFiltered" => $this->user->count_filtered(),
						"data" => $data,
				);
		
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->user->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->load->library('file_upload');

		$this->_validate();
		
		$data = array(
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password')),
				'email' => $this->input->post('email'),
				'fullname' => $this->input->post('fullname'),
				'status' => $this->input->post('status'),
				'is_admin' => 0,
				'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
			);

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->file_upload->do_upload('photo');
			$data['photo'] = $upload;
		}

		$insert = $this->user->save($data);

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$this->load->library('file_upload');

		$this->_validate();
		
		$data = array(
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password')),
				'email' => $this->input->post('email'),
				'fullname' => $this->input->post('fullname'),
				'status' => $this->input->post('status'),
				'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
			);

		if($this->input->post('remove_photo'))
		{
			if(file_exists(UPLOAD_FOLDER.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
				unlink(UPLOAD_FOLDER.$this->input->post('remove_photo'));
			$data['photo'] = '';
		}

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->file_upload->do_upload('photo');
			
			$user = $this->user->get_by_id($this->input->post('id'));
			if(file_exists(UPLOAD_FOLDER.$user->photo) && $user->photo)
				unlink(UPLOAD_FOLDER.$user->photo);

			$data['photo'] = $upload;
		}

		$this->user->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$user = $this->user->get_by_id($id);
		if(file_exists(UPLOAD_FOLDER.$user->photo) && $user->photo)
			unlink(UPLOAD_FOLDER.$user->photo);
		
		$this->user->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('username') == '')
		{
			$data['inputerror'][] = 'username';
			$data['error_string'][] = 'Kullanıcı Adı alanı boş bırakılmaz';
			$data['status'] = FALSE;
		}

		if($this->input->post('password') == '')
		{
			$data['inputerror'][] = 'password';
			$data['error_string'][] = 'Password alanı boş bırakılmaz';
			$data['status'] = FALSE;
		}

		if($this->input->post('email') == '')
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Email alanı boş bırakılmaz';
			$data['status'] = FALSE;
		}

		if($this->input->post('fullname') == '')
		{
			$data['inputerror'][] = 'fullname';
			$data['error_string'][] = 'Ad ve Soyad alanı boş bırakılmaz';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
