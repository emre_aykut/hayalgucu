<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Popup extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('admin')) {
			redirect(base_url().'admin/auth/login');
			exit();
		}
		$this->load->model('setting/popup_model','popup');
		$this->load->model('content_relation/content_relation_model', 'content_relation');
	}

	public function index()
	{
		$this->load->view('admin/settings/popup_view');
	}

	public function ajax_list($lang_id = '')
	{
		if ($lang_id == '') 
		{
			$this->load->model('language/language_model', 'language');
			$lang = $this->language->get_lang_default();
			$lang_id = $lang->id;
		}

		$list = $this->popup->get_datatables($lang_id);

		$data = array();
		$no = $_POST['start'];
		foreach ($list as $popup) 
		{
			$no++;
			$row = array();
			$row[] = $popup->title;
			$row[] = $popup->startDate;
			$row[] = $popup->finishDate;
			$row[] = $popup->status;
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Düzenle" onclick="edit_popup('."'".$popup->content_id."'".')"><i class="glyphicon glyphicon-pencil"></i> Düzenle</a> 
				<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Sil" onclick="delete_popup('."'".$popup->content_id."'".')"><i class="glyphicon glyphicon-trash"></i> Sil</a>';

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->popup->count_all($lang_id),
						"recordsFiltered" => $this->popup->count_filtered($lang_id),
						"data" => $data,
				);
		
		echo json_encode($output);
	}

	public function ajax_edit($content_id)
	{	
		$results = $this->popup->get_by_content_id($content_id);
		$i = 0;

		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $this->popup->get_by_id($value['id']);
		}

		$data['total_record'] = $i;
		
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->load->model('language/language_model', 'language');

		$language_count = $this->language->get_count();
		$post = $this->input->post(null,true);

		$this->_validate($language_count);

		$content_id = $this->content_relation->save(array( 'modul' => 'popup', 'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME) ));

		for ($i=1; $i <= $language_count; $i++) 
		{ 		
			$data = array(
				'title' => $post['title'][$i],
				'content' => $post['content'][$i],
				'refresh_time' => $post['refresh_time'][$i],
				'startDate' => $post['startDate'][$i],
				'finishDate' => $post['finishDate'][$i],
				'status' => $post['status'][$i],
				'lang_id' => $post['lang_id'][$i],
				'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
				'content_id' => $content_id
			);
			
			$insert = $this->popup->save($data);
		}

		echo json_encode(array("status" => TRUE));

	}

	public function ajax_update()
	{
		$this->load->model('language/language_model', 'language');

		$language_count = $this->language->get_count();
		$post = $this->input->post(null,true);

		$this->_validate($language_count);

		$this->content_relation->update(array('id' => $post['content_id']), array('updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)));

		for ($i=1; $i <= $language_count; $i++) 
		{ 	
			$data = array(
				'title' => $post['title'][$i],
				'content' => $post['content'][$i],
				'refresh_time' => $post['refresh_time'][$i],
				'startDate' => $post['startDate'][$i],
				'finishDate' => $post['finishDate'][$i],
				'status' => $post['status'][$i],
				'lang_id' => $post['lang_id'][$i],
				'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
				'content_id' => $post['content_id']
			);

			$this->popup->update(array('id' => $post['id'][$i]), $data);

		}

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($content_id)
	{	
		$this->popup->delete_by_id($content_id);
		$this->content_relation->delete_by_id($content_id);

		echo json_encode(array("status" => TRUE));
	}

	private function _validate($language_count = '')
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['lang'] = '';
		$data['status'] = TRUE;

		for ($i=1; $i <= $language_count; $i++) 
		{
			
			if($this->input->post('refresh_time')[$i] == '')
			{
				$data['inputerror'][][$i] = 'refresh_time';
				$data['error_string'][][$i] = 'Önbellek Süresi alanı boş bırakılmaz';
				$data['lang'][] = $i;
				$data['status'] = FALSE;
			}

			if($this->input->post('startDate')[$i] == '')
			{
				$data['inputerror'][][$i] = 'startDate';
				$data['error_string'][][$i] = 'Başlangıç tarihi alanı boş bırakılmaz';
				$data['lang'][] = $i;
				$data['status'] = FALSE;
			}

			if($this->input->post('finishDate')[$i] == '')
			{
				$data['inputerror'][][$i] = 'finishDate';
				$data['error_string'][][$i] = 'Bitiş tarihi alanı boş bırakılmaz';
				$data['lang'][] = $i;
				$data['status'] = FALSE;
			}

		}		

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
