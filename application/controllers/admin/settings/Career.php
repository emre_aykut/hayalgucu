<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('admin')) {
			redirect(base_url().'admin/auth/login');
			exit();
		}
		$this->load->model('career/career_model','career');
	}

	public function index()
	{
		$this->load->view('admin/settings/career_view');
	}

	public function ajax_list()
	{
		$list = $this->career->get_datatables();
		$data = array();
		foreach ($list as $career)
		{
			$row = array();
			$row[] = $career->fullname;
			$row[] = $career->email;
			$row[] = '
				<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Düzenle" onclick="edit_career('."'".$career->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Düzenle</a>
				<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Sil" onclick="delete_career('."'".$career->id."'".')"><i class="glyphicon glyphicon-trash"></i> Sil</a>';
		
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->career->count_all(),
			"recordsFiltered" => $this->career->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->career->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->load->library('file_upload');

		$this->_validate();
		
		$data = array(
			'fullname' => $this->input->post('fullname'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
		);

		$insert = $this->career->save($data);

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$this->load->library('file_upload');

		$this->_validate();
		
		$data = array(
			'fullname' => $this->input->post('fullname'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
		);

		$this->career->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$career = $this->career->get_by_id($id);
		if(file_exists(UPLOAD_FOLDER.$career->cv) && $career->cv)
			unlink(UPLOAD_FOLDER.$career->cv);
		
		$this->career->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('fullname') == '')
		{
			$data['inputerror'][] = 'fullname';
			$data['error_string'][] = 'İsim Soyisim alanı boş bırakılmaz';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
