<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('admin')) {
			redirect(base_url().'admin/auth/login');
			exit();
		}
		$this->load->model('setting/page_model','page');
		$this->load->model('setting/page_content_relation_model', 'page_content_relation');
	}

	public function index()
	{
		$this->load->view('admin/settings/page_view');
	}

	public function ajax_list($lang_id = '')
	{
		if ($lang_id == '') 
		{
			$this->load->model('language/language_model', 'language');
			$lang = $this->language->get_lang_default();
			$lang_id = $lang->id;
		}

		$list = $this->page->get_datatables($lang_id);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $page) 
		{
			$no++;
			$row = array();
			$row[] = $page->title;
			$row[] = $page->link;
			$row[] = $page->sort;
			$row[] = $page->status;
			$row[] = $page->default_page;
			if($page->photo)
				$row[] = '<a href="'.base_url(UPLOAD_FOLDER.$page->photo).'" target="_blank"><img src="'.base_url(UPLOAD_FOLDER.$page->photo).'" class="img-responsive" style="height:50px" /></a>';
			else
				$row[] = '(Fotoğraf Yok)';
	
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Düzenle" onclick="edit_page('."'".$page->content_id."'".')"><i class="glyphicon glyphicon-pencil"></i> Düzenle</a>
						<a class="btn btn-sm btn-danger '. ($page->default_page == 1 ? 'disabled' : '') .'" href="javascript:void(0)" title="Sil" onclick="delete_page('."'".$page->content_id."'".')"><i class="glyphicon glyphicon-trash"></i> Sil</a>';
			
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->page->count_all($lang_id),
						"recordsFiltered" => $this->page->count_filtered($lang_id),
						"data" => $data,
				);
		
		echo json_encode($output);
	}

	public function ajax_edit($content_id)
	{	
		$results = $this->page->get_by_content_id($content_id);
		$i = 0;

		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $this->page->get_by_id($value['id']);
		}

		$data['total_record'] = $i;
		
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->load->library('file_upload');
		$this->load->model('language/language_model', 'language');

		$language_count = $this->language->get_count();
		$post = $this->input->post(null,true);

		$this->_validate($language_count);

		$content_id = $this->page_content_relation->save(array( 'modul' => 'page', 'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME) ));

		for ($i=1; $i <= $language_count; $i++) 
		{ 	
			$upload = '';
			$file_name = 'photo_'.$i;

			$data = array(
				'parent_id' => $post['parent_id'][$i],
				'title' => $post['title'][$i],
				'link' => $post['link'][$i],
				'description' => $post['description'][$i],
				'detail' => $post['detail'][$i],
				'status' => $post['status'][$i],
				'meta_title' => $post['meta_title'][$i],
				'meta_description' => $post['meta_description'][$i],
				'meta_keywords' => $post['meta_keywords'][$i],
				'sort' => $post['sort'][$i],
				'default_page' => 0,
				'lang_id' => $post['lang_id'][$i],
				'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
				'content_id' => $content_id
			);

			if(!empty($_FILES[$file_name]['name']))
			{
				$upload = $this->file_upload->do_upload($file_name);
				$data['photo'] = $upload;
			}else{
				$data['photo'] = '';
			}
			
			$insert_id = $this->page->save($data);

		}

		echo json_encode(array("status" => TRUE));

	}

	public function ajax_update()
	{
		$this->load->library('file_upload');
		$this->load->model('language/language_model', 'language');

		$language_count = $this->language->get_count();
		$post = $this->input->post(null,true);

		$this->_validate($language_count);

		$this->page_content_relation->update(array('id' => $post['content_id']), array('updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)));

		for ($i=1; $i <= $language_count; $i++) 
		{ 	
			$upload = '';
			$file_name = 'photo_'.$i;

			$data = array(
				'parent_id' => $post['parent_id'][$i],
				'title' => $post['title'][$i],
				'link' => $post['link'][$i],
				'description' => $post['description'][$i],
				'detail' => $post['detail'][$i],
				'status' => $post['status'][$i],
				'meta_title' => $post['meta_title'][$i],
				'meta_description' => $post['meta_description'][$i],
				'meta_keywords' => $post['meta_keywords'][$i],
				'sort' => $post['sort'][$i],
				'lang_id' => $post['lang_id'][$i],
				'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
				'content_id' => $post['content_id']
			);

			if($this->input->post('remove_photo_'.$i))
			{
				if(file_exists(UPLOAD_FOLDER.$this->input->post('remove_photo_'.$i)) && $this->input->post('remove_photo_'.$i))
					unlink(UPLOAD_FOLDER.$post['remove_photo_'.$i]);
				$data['photo'] = '';
			}

			if(!empty($_FILES[$file_name]['name']))
			{
				$upload = $this->file_upload->do_upload($file_name);
				$data['photo'] = $upload;
			}

			$this->page->update(array('id' => $post['id'][$i]), $data);

		}

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($content_id)
	{
		$results = $this->page->get_by_content_id($content_id);

		foreach ($results as $key => $value) 
		{
			$page = $this->page->get_by_id($value['id']);
			if(file_exists(UPLOAD_FOLDER.$page->photo) && $page->photo)
				unlink(UPLOAD_FOLDER.$page->photo);	
		}

		$this->page->delete_by_id($content_id);
		$this->page_content_relation->delete_by_id($content_id);

		echo json_encode(array("status" => TRUE));
	}

	private function _validate($language_count = '')
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['lang'] = '';
		$data['status'] = TRUE;

		for ($i=1; $i <= $language_count; $i++) 
		{	
			if($this->input->post('content_id') == $this->input->post('parent_id')[$i])
			{
				$data['inputerror'][][$i] = 'p_id';
				$data['error_string'][][$i] = 'Bağlı olduğu sayfa kendi sayfası olamaz.';
				$data['lang'][] = $i;
				$data['status'] = FALSE;
			}

			if($this->input->post('parent_id')[$i] == '')
			{
				$data['inputerror'][][$i] = 'p_id';
				$data['error_string'][][$i] = 'Bağlı olduğu sayfa alanı boş bırakılmaz';
				$data['lang'][] = $i;
				$data['status'] = FALSE;
			}

			if($this->input->post('title')[$i] == '')
			{
				$data['inputerror'][][$i] = 'title';
				$data['error_string'][][$i] = 'Başlık alanı boş bırakılmaz';
				$data['lang'][] = $i;
				$data['status'] = FALSE;
			}

		}		

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function tree_pages_list($lang_id)
	{
		$parentArr = array();
		$data = $this->page->get_all($lang_id);

		foreach($data as $key => $val){
			$parentArr[$val['parent_id']][$val['content_id']] = array('title' => $val['title'], 'lang_id' => $val['lang_id'], 'pid' => $val['parent_id']);
		}

		echo json_encode($parentArr);
	}

}
