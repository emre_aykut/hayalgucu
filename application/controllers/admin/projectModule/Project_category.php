<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project_category extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('admin')) {
			redirect(base_url().'admin/auth/login');
			exit();
		}
		$this->load->model('project/project_category_model','project_category');
		$this->load->model('project/project_content_relation_model', 'project_content_relation');
	}

	public function index()
	{
		$this->load->view('admin/project/project_category_view');
	}

	public function ajax_list($lang_id = '')
	{
		if ($lang_id == '') 
		{
			$this->load->model('language/language_model', 'language');
			$lang = $this->language->get_lang_default();
			$lang_id = $lang->id;
		}

		$list = $this->project_category->get_datatables($lang_id);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $project_category) 
		{
			$no++;
			$row = array();
			$row[] = $project_category->title;
			if($project_category->photo)
				$row[] = '<a href="'.base_url(UPLOAD_FOLDER.$project_category->photo).'" target="_blank"><img src="'.base_url(UPLOAD_FOLDER.$project_category->photo).'" class="img-responsive" style="height:50px" /></a>';
			else
				$row[] = '(Fotoğraf Yok)';

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Düzenle" onclick="edit_project_category('."'".$project_category->content_id."'".')"><i class="glyphicon glyphicon-pencil"></i> Düzenle</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Sil" onclick="delete_project_category('."'".$project_category->content_id."'".')"><i class="glyphicon glyphicon-trash"></i> Sil</a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->project_category->count_all($lang_id),
						"recordsFiltered" => $this->project_category->count_filtered($lang_id),
						"data" => $data,
				);
		
		echo json_encode($output);
	}

	public function ajax_edit($content_id)
	{	
		$results = $this->project_category->get_by_content_id($content_id);
		$i = 0;

		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $this->project_category->get_by_id($value['id']);
		}

		$data['total_record'] = $i;
		
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->load->library('file_upload');
		$this->load->model('language/language_model', 'language');

		$language_count = $this->language->get_count();
		$post = $this->input->post(null,true);

		$this->_validate($language_count);

		$content_id = $this->project_content_relation->save(array( 'modul' => 'project_category', 'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)));

		for ($i=1; $i <= $language_count; $i++) 
		{ 
			
			$data = array(
				'title' => $post['title'][$i],
				'lang_id' => $post['lang_id'][$i],
				'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
				'content_id' => $content_id
			);

			$file_name = 'photo_'.$i;
			if(!empty($_FILES[$file_name]['name']))
			{
				$upload = $this->file_upload->do_upload($file_name);
				$data['photo'] = $upload;
			}else{
				$data['photo'] = '';
			}
			
			$insert = $this->project_category->save($data);
		}

		echo json_encode(array("status" => TRUE));

	}

	public function ajax_update()
	{
		$this->load->library('file_upload');
		$this->load->model('language/language_model', 'language');

		$language_count = $this->language->get_count();
		$post = $this->input->post(null,true);

		$this->_validate($language_count);

		$this->project_content_relation->update(array('id' => $post['content_id']), array('updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)));

		for ($i=1; $i <= $language_count; $i++) 
		{ 
			
			$data = array(
				'title' => $post['title'][$i],
				'lang_id' => $post['lang_id'][$i],
				'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
				'content_id' => $post['content_id']
			);

			if($this->input->post('remove_photo_'.$i)) 
			{
				if(file_exists(UPLOAD_FOLDER.$this->input->post('remove_photo_'.$i)) && $this->input->post('remove_photo_'.$i))
					unlink(UPLOAD_FOLDER.$post['remove_photo_'.$i]);
				$data['photo'] = '';
			}

			$file_name = 'photo_'.$i;
			if(!empty($_FILES[$file_name]['name']))
			{
				$upload = $this->file_upload->do_upload($file_name);
				$data['photo'] = $upload;
			}

			$this->project_category->update(array('id' => $post['id'][$i]), $data);
		}
	

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($content_id)
	{	
		$results = $this->project_category->get_by_content_id($content_id);

		foreach ($results as $key => $value) 
		{
			$project_category = $this->project_category->get_by_id($value['id']);
			if(file_exists(UPLOAD_FOLDER.$project_category->photo) && $project_category->photo)
				unlink(UPLOAD_FOLDER.$project_category->photo);	
		}
		
		$this->project_category->delete_by_id($content_id);
		$this->project_content_relation->delete_by_id($content_id);

		echo json_encode(array("status" => TRUE));
	}

	private function _validate($language_count = '')
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['lang'] = '';
		$data['status'] = TRUE;

		for ($i=1; $i <= $language_count; $i++) 
		{
			if($this->input->post('title')[$i] == '')
			{
				$data['inputerror'][][$i] = 'title';
				$data['error_string'][][$i] = 'Başlık alanı boş bırakılmaz';
				$data['lang'][] = $i;
				$data['status'] = FALSE;
			}
		}		

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function ajax_category_get()
	{
		$data['categories'] = $this->project_category->get_list();

		echo json_encode($data);
	}

}
