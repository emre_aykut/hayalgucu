<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('admin')) {
			redirect(base_url().'admin/auth/login');
			exit();
		}
		$this->load->model('project/project_model','project');
		$this->load->model('project/project_content_relation_model', 'project_content_relation');
		$this->load->model('project/project_images_model', 'project_images');
	}

	public function index()
	{
		$this->load->view('admin/project/project_view');
	}

	public function ajax_list($lang_id = '')
	{
		if ($lang_id == '') 
		{
			$this->load->model('language/language_model', 'language');
			$lang = $this->language->get_lang_default();
			$lang_id = $lang->id;
		}

		$list = $this->project->get_datatables($lang_id);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $project) 
		{
			$no++;
			$row = array();
			$row[] = $project->title;
			$row[] = $project->category;
			$row[] = $project->status;

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Düzenle" onclick="edit_project('."'".$project->content_id."'".')"><i class="glyphicon glyphicon-pencil"></i> Düzenle</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Sil" onclick="delete_project('."'".$project->content_id."'".')"><i class="glyphicon glyphicon-trash"></i> Sil</a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->project->count_all($lang_id),
						"recordsFiltered" => $this->project->count_filtered($lang_id),
						"data" => $data,
				);
		
		echo json_encode($output);
	}

	public function ajax_edit($content_id)
	{	
		$results = $this->project->get_by_content_id($content_id);
		$i = 0;

		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $this->project->get_by_id($value['id']);
		}

		$data['total_record'] = $i;
		
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->load->library('file_upload');
		$this->load->model('language/language_model', 'language');

		$language_count = $this->language->get_count();
		$post = $this->input->post(null,true);
		
		$this->_validate($language_count);

		$content_id = $this->project_content_relation->save(array( 'modul' => 'project', 'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME) ));

		for ($i=1; $i <= $language_count; $i++) 
		{ 	
			$upload = '';
			$file_name = 'default_photo_'.$i;

			$data = array(
				'title' => $post['title'][$i],
				'link' => $post['link'][$i],
				'description' => $post['description'][$i],
				'detail' => $post['detail'][$i],
				'meta_title' => $post['meta_title'][$i],
				'meta_description' => $post['meta_description'][$i],
				'meta_keywords' => $post['meta_keywords'][$i],
				'sort' => $post['sort'][$i],
				'category_id' => $post['category_id'][$i],
				'lang_id' => $post['lang_id'][$i],
				'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
				'content_id' => $content_id
			);

			if (!isset($post['status'][$i])) 
			{
				$data['status'] = 0;
			}
			else
			{
				$data['status'] = $post['status'][$i];
			}

			if (!isset($post['common_img'])) 
			{
				$data['common_img'] = 0;
			}
			else
			{
				$data['common_img'] = $post['common_img'];
			}

			if(!empty($_FILES[$file_name]['name']))
			{
				$upload = $this->file_upload->do_upload($file_name);
				$data['default_photo'] = $upload;
			}
			else
			{
				$data['default_photo'] = '';
			}
			
			$insert_id = $this->project->save($data);

			if (isset($post['image_src'][$i])) 
			{
				foreach ($post['image_src'][$i] as $key => $value) {
					$data_img = array(
						'image' => $value,
						'title' => $post['image_text'][$i][$key],
						'sort' => $post['image_sort'][$i][$key],
						'project_id' => $insert_id,
						'project_content_id' => $content_id,
						'lang_id' => $post['lang_id'][$i],
						'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
					);
					$this->project_images->save($data_img);
				}
			}

		}

		echo json_encode(array("status" => TRUE, "content_id" => $content_id));

	}

	public function ajax_update()
	{
		$this->load->library('file_upload');
		$this->load->model('language/language_model', 'language');

		$language_count = $this->language->get_count();
		$post = $this->input->post(null,true);

		$this->_validate($language_count);

		$this->project_content_relation->update(array('id' => $post['content_id']), array('updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)));

		for ($i=1; $i <= $language_count; $i++) 
		{ 	
			$upload = '';
			$file_name = 'default_photo_'.$i;

			$data = array(
				'title' => $post['title'][$i],
				'link' => $post['link'][$i],
				'description' => $post['description'][$i],
				'detail' => $post['detail'][$i],
				'meta_title' => $post['meta_title'][$i],
				'meta_description' => $post['meta_description'][$i],
				'meta_keywords' => $post['meta_keywords'][$i],
				'sort' => $post['sort'][$i],
				'category_id' => $post['category_id'][$i],
				'lang_id' => $post['lang_id'][$i],
				'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
				'content_id' => $post['content_id']
			);

			if (!isset($post['status'][$i])) 
			{
				$data['status'] = 0;
			}
			else
			{
				$data['status'] = $post['status'][$i];
			}

			if (!isset($post['common_img'])) 
			{
				$data['common_img'] = 0;
			}
			else
			{
				$data['common_img'] = $post['common_img'];
			}

			if($this->input->post('remove_photo_'.$i))
			{
				if(file_exists(UPLOAD_FOLDER.$this->input->post('remove_photo_'.$i)) && $this->input->post('remove_photo_'.$i))
					unlink(UPLOAD_FOLDER.$post['remove_photo_'.$i]);
				$data['default_photo'] = '';
			}

			if(!empty($_FILES[$file_name]['name']))
			{
				$upload = $this->file_upload->do_upload($file_name);
				$data['default_photo'] = $upload;
			}

			$this->project->update(array('id' => $post['id'][$i]), $data);

			$this->project_images->img_clear($post['id'][$i]);

			if (isset($post['image_src'][$i])) 
			{
				foreach ($post['image_src'][$i] as $key => $value) {
					$data_img = array(
						'image' => $value,
						'title' => $post['image_text'][$i][$key],
						'sort' => $post['image_sort'][$i][$key],
						'project_id' => $post['id'][$i],
						'project_content_id' => $post['content_id'],
						'lang_id' => $post['lang_id'][$i],
						'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)
					);
					$this->project_images->save($data_img);
				}
			}

		}

		echo json_encode(array("status" => TRUE, "content_id" => $post['content_id']));
	}

	public function ajax_image_get($project_id)
	{
		$data = $this->project_images->get_project_image($project_id);

		echo json_encode($data);
	}

	public function ajax_delete($content_id)
	{	
		$content_results = $this->project->get_by_content_id($content_id);
		$img_results = $this->project_images->get_by_project_content_id($content_id);

		foreach ($content_results as $key => $value) 
		{
			$project = $this->project->get_by_id($value['id']);
			if(file_exists(UPLOAD_FOLDER.$project->default_photo) && $project->default_photo)
				unlink(UPLOAD_FOLDER.$project->default_photo);	
		}

		if ($img_results) {
			foreach ($img_results as $key => $value) {
				if(file_exists(UPLOAD_FOLDER.$value['image']) && $value['image'])
					unlink(UPLOAD_FOLDER.$value['image']);		
			}
		}

		$this->project->delete_by_id($content_id);
		$this->project_content_relation->delete_by_id($content_id);
		$this->project_images->delete($content_id);

		echo json_encode(array("status" => TRUE));
	}

	private function _validate($language_count = '')
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['lang'] = '';
		$data['status'] = TRUE;

		for ($i=1; $i <= $language_count; $i++) 
		{

			if($this->input->post('title')[$i] == '')
			{
				$data['inputerror'][][$i] = 'title';
				$data['error_string'][][$i] = 'Başlık alanı boş bırakılmaz';
				$data['lang'][] = $i;
				$data['status'] = FALSE;
			}

			if($this->input->post('category_id')[$i] == 0)
			{
				$data['inputerror'][][$i] = 'category_id';
				$data['error_string'][][$i] = 'Kategori seçiniz';
				$data['lang'][] = $i;
				$data['status'] = FALSE;
			}

		}		

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
