<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image_upload_ajax extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if (!$this->session->userdata('admin')) {
			redirect(base_url().'admin/auth/login');
			exit();
		}
	}

	public function image_upload($file_name)
	{
		$this->load->library('file_upload');
		$upload = $this->file_upload->do_upload($file_name);
		
		echo json_encode($upload);
	}

	public function images_upload($files_name)
	{
		$this->load->library('file_upload');
		$uploads = $this->file_upload->do_upload_multiple($files_name);

		echo json_encode($uploads);	
	}

}