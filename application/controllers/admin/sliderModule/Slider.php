<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('admin')) {
			redirect(base_url().'admin/auth/login');
			exit();
		}
		$this->load->model('slider/slider_model','slider');
		$this->load->model('content_relation/content_relation_model', 'content_relation');
	}

	public function index()
	{
		$this->load->view('admin/slider/slider_view');
	}

	public function ajax_list($lang_id = '')
	{
		if ($lang_id == '') 
		{
			$this->load->model('language/language_model', 'language');
			$lang = $this->language->get_lang_default();
			$lang_id = $lang->id;
		}

		$list = $this->slider->get_datatables($lang_id);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $slider) 
		{
			$no++;
			$row = array();
			$row[] = $slider->title;
			$row[] = $slider->sort;
			if($slider->photo)
				$row[] = '<a href="'.base_url(UPLOAD_FOLDER.$slider->photo).'" target="_blank"><img src="'.base_url(UPLOAD_FOLDER.$slider->photo).'" class="img-responsive" style="height:50px" /></a>';
			else
				$row[] = '(Fotoğraf Yok)';

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Düzenle" onclick="edit_slider('."'".$slider->content_id."'".')"><i class="glyphicon glyphicon-pencil"></i> Düzenle</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Sil" onclick="delete_slider('."'".$slider->content_id."'".')"><i class="glyphicon glyphicon-trash"></i> Sil</a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->slider->count_all($lang_id),
						"recordsFiltered" => $this->slider->count_filtered($lang_id),
						"data" => $data,
				);
		
		echo json_encode($output);
	}

	public function ajax_edit($content_id)
	{	
		$results = $this->slider->get_by_content_id($content_id);
		$i = 0;

		foreach ($results as $key => $value) 
		{
			$i++;
			$data[$i] = $this->slider->get_by_id($value['id']);
		}

		$data['total_record'] = $i;
		
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->load->library('file_upload');
		$this->load->model('language/language_model', 'language');

		$language_count = $this->language->get_count();
		$post = $this->input->post(null,true);

		$this->_validate($language_count);

		$content_id = $this->content_relation->save(array( 'modul' => 'slider', 'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME) ));

		for ($i=1; $i <= $language_count; $i++) 
		{ 		
			$data = array(
				'title' => $post['title'][$i],
				'link' => $post['link'][$i],
				'sort' => $post['sort'][$i],
				'lang_id' => $post['lang_id'][$i],
				'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
				'content_id' => $content_id
			);

			$file_name = 'photo_'.$i;
			if(!empty($_FILES[$file_name]['name']))
			{
				$upload = $this->file_upload->do_upload($file_name);
				$data['photo'] = $upload;
			}else{
				$data['photo'] = '';
			}

			$file_name = 'mobile_photo_'.$i;
			if(!empty($_FILES[$file_name]['name']))
			{
				$upload = $this->file_upload->do_upload($file_name);
				$data['mobile_photo'] = $upload;
			}else{
				$data['mobile_photo'] = '';
			}
			
			$insert = $this->slider->save($data);
		}

		echo json_encode(array("status" => TRUE));

	}

	public function ajax_update()
	{
		$this->load->library('file_upload');
		$this->load->model('language/language_model', 'language');

		$language_count = $this->language->get_count();
		$post = $this->input->post(null,true);

		$this->_validate($language_count);

		$this->content_relation->update(array('id' => $post['content_id']), array('updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME)));

		for ($i=1; $i <= $language_count; $i++) 
		{ 	
			$data = array(
				'title' => $post['title'][$i],
				'link' => $post['link'][$i],
				'sort' => $post['sort'][$i],
				'lang_id' => $post['lang_id'][$i],
				'updated_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
				'content_id' => $post['content_id']
			);

			if($this->input->post('remove_photo_'.$i)) 
			{
				if(file_exists(UPLOAD_FOLDER.$this->input->post('remove_photo_'.$i)) && $this->input->post('remove_photo_'.$i))
					unlink(UPLOAD_FOLDER.$post['remove_photo_'.$i]);
				$data['photo'] = '';
			}

			if($this->input->post('remove_mobile_photo_'.$i)) 
			{
				if(file_exists(UPLOAD_FOLDER.$this->input->post('remove_mobile_photo_'.$i)) && $this->input->post('remove_mobile_photo_'.$i))
					unlink(UPLOAD_FOLDER.$post['remove_mobile_photo_'.$i]);
				$data['mobile_photo'] = '';
			}

			$file_name = 'photo_'.$i;
			if(!empty($_FILES[$file_name]['name']))
			{
				$upload = $this->file_upload->do_upload($file_name);
				$data['photo'] = $upload;
			}

			$file_name = 'mobile_photo_'.$i;
			if(!empty($_FILES[$file_name]['name']))
			{
				$upload = $this->file_upload->do_upload($file_name);
				$data['mobile_photo'] = $upload;
			}else{
				$data['mobile_photo'] = '';
			}

			$this->slider->update(array('id' => $post['id'][$i]), $data);

		}
	

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($content_id)
	{	
		$results = $this->slider->get_by_content_id($content_id);

		foreach ($results as $key => $value) 
		{
			$slider = $this->slider->get_by_id($value['id']);
			
			if(file_exists(UPLOAD_FOLDER.$slider->photo) && $slider->photo)
				unlink(UPLOAD_FOLDER.$slider->photo);

			if(file_exists(UPLOAD_FOLDER.$slider->mobile_photo) && $slider->mobile_photo)
				unlink(UPLOAD_FOLDER.$slider->mobile_photo);	
		}

		$this->slider->delete_by_id($content_id);
		$this->content_relation->delete_by_id($content_id);

		echo json_encode(array("status" => TRUE));
	}

	private function _validate($language_count = '')
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['lang'] = '';
		$data['status'] = TRUE;

		for ($i=1; $i <= $language_count; $i++) 
		{

			if($this->input->post('title')[$i] == '')
			{
				$data['inputerror'][][$i] = 'title';
				$data['error_string'][][$i] = 'Başlık alanı boş bırakılmaz';
				$data['lang'][] = $i;
				$data['status'] = FALSE;
			}

		}		

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
