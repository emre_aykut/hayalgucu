<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->autorun();
	}

	public function index()
	{
		$this->load->model('slider/slider_model', 'slider');
		$this->load->model('project/project_category_model', 'project_category');
		$this->load->model('article/article_model', 'article');

		$get_lang_id = $this->get_lang_id();
		$this->slider->lang_id = $get_lang_id;
		$this->project_category->lang_id = $get_lang_id;
		$this->article->lang_id = $get_lang_id;

		$data['sliders'] = $this->slider->get_all();
		$data['project_categories'] = $this->project_category->get_all();
		$data['articles'] = $this->article->get_list_limit(12, 0);

		$this->load->view('frontend/home_view', $data);
	}

}


?>