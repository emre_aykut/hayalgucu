<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->autorun();
	}

	public function index()
	{
		$this->load->model('setting/page_model', 'page');
		$this->load->model('team/team_model', 'team');

		$page_id = 4;
		$get_lang_id = $this->get_lang_id();
		$this->page->lang_id = $get_lang_id;
		$this->team->lang_id = $get_lang_id;

		$data['teams'] = $this->team->get_all();
		$data['page_detail'] = $this->page->page_detail($page_id);

		$this->load->view('frontend/team_view', $data);
	}

}


?>