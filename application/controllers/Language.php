<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends MY_Controller {

	public function change($lang = '')
	{
		$this->load->model('language/language_model', 'language');
		
		$default_lang = $this->language->get_lang_default();

		if (!$lang) 
		{	
			$lang_id = $default_lang->id;
		}
		else
		{	
			$this->language->code = $lang;
			$langs = $this->language->get_item();
			if (!$langs) 
			{
				$lang_id = $default_lang->id;
			}
			else
			{
				$lang_id = $langs->id;	
			}
		}

		$this->session->set_userdata('lang', $lang_id);
		
		$this->load->library('user_agent');
		if ($this->agent->is_referral() && !$this->agent->is_mobile())
		{
			header('location: ' . $this->agent->referrer());
		}
		else
		{
			header('location: ' . base_url());
		}		
		
	}

}