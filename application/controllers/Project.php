<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->autorun();
	}

	public function index()
	{
		$this->load->model('setting/page_model', 'page');
		$this->load->model('project/project_category_model', 'project_category');

		$page_id = 3;
		$get_lang_id = $this->get_lang_id();
		$this->page->lang_id = $get_lang_id;
		$this->project_category->lang_id = $get_lang_id;

		$data['page_detail'] = $this->page->page_detail($page_id);
		$data['project_categories'] = $this->project_category->get_all();
		$data['category_id'] = '';

		$this->load->view('frontend/project_view', $data);
	}

	public function home($category_title_and_category_id = '')
	{
		$this->load->model('setting/page_model', 'page');
		$this->load->model('project/project_category_model', 'project_category');

		$get_parameter = explode('-', $category_title_and_category_id);
		$category_id = end($get_parameter);

		$page_id = 3;
		$get_lang_id = $this->get_lang_id();
		$this->page->lang_id = $get_lang_id;
		$this->project_category->lang_id = $get_lang_id;

		$data['page_detail'] = $this->page->page_detail($page_id);
		$data['project_categories'] = $this->project_category->get_all();
		$data['category_id'] = $category_id;

		$this->load->view('frontend/project_view', $data);
	}

	public function detail($title_and_id = '')
	{
		$this->load->model('setting/page_model', 'page');
		$this->load->model('project/project_model', 'project');
		$this->load->model('project/project_images_model', 'project_images');

		$get_parameter = explode('-', $title_and_id);
		$id = end($get_parameter);

		$get_lang_id = $this->get_lang_id();
		$this->project->lang_id = $get_lang_id;
		$this->project_images->lang_id = $get_lang_id;

		$data['project'] = $this->project->get_detail($id, 1);
		$data['project_images'] = $this->project_images->get_all($id);

		if (!empty($data['project']))
		{
			$this->load->view('frontend/project_detail_view', $data);
		}
		else
		{
			redirect(base_url());		
		}
	}

	public function ajax_projects($limit = 8, $category_id = '')
	{
		$this->load->model('project/project_model', 'project');

		$get_lang_id = $this->get_lang_id();
		$this->project->lang_id = $get_lang_id;

		if(isset($_POST['page']) && $_POST['page'] != "")
		{
		  	$page = $_POST['page'];
		  	$offset = $limit * ($page-1);
		}
		else
		{
		  	$page = 1;
		  	$offset = 0;
		}

		$total_rows = $this->project->get_total_rows();
		$total_pages = ceil($total_rows/$limit);

		$projects = $this->project->get_list_limit($limit, $offset, $category_id);
		$base_url = base_url();
		$upload_folder = UPLOAD_FOLDER;

		$count = 0;
		if ($projects)
		{
			$results = "";
			$results .= '<input type="hidden" name="total_pages" id="total_pages" value="' . $total_pages . '">';
	  		$results .= '<input type="hidden" name="page" id="page" value="' . $page . '">';

	  		$count = 6;
			foreach ($projects as $row) 
			{
				$detail_url = seo_url($row['title']) . '-' . $row['content_id'];
				$title = text_limitation($row['title'], 80);
				$results .= "
						<div class=\"col-lg-3 col-md-6 col-12 float-left animated fadeIn delay-{$count}s\">
							<a href=\"{$base_url}project/detail/{$detail_url}\" class=\"box\">
								<div class=\"title\">{$title}</div>
								<div class=\"underside\">
									<div class=\"text\">{$row['category_title']}</div>
									<div class=\"icon\">
										<img src=\"{$base_url}{$upload_folder}{$row['category_photo']}\">
									</div>
								</div>
							</a>
						</div>
			            ";
	            $count += 2;
			}

			echo $results;
		}
	}

}


?>