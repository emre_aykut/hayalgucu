<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->autorun();
	}

	public function index()
	{
		$this->load->model('setting/page_model', 'page');

		$page_id = 5;
		$get_lang_id = $this->get_lang_id();
		$this->page->lang_id = $get_lang_id;
		
		$data['page_detail'] = $this->page->page_detail($page_id);

		$this->load->view('frontend/article_view', $data);
	}

	public function detail($title_and_id = '')
	{
		$this->load->model('setting/page_model', 'page');
		$this->load->model('article/article_model', 'article');
		$this->load->model('article/article_tag_relation_model', 'article_tag_relation');

		$get_parameter = explode('-', $title_and_id);
		$id = end($get_parameter);

		$get_lang_id = $this->get_lang_id();
		$this->article->lang_id = $get_lang_id;
		$this->article_tag_relation->lang_id = $get_lang_id;

		$tags = $this->article_tag_relation->get_tag($id);

		if ($tags)
		{
			$article_tags = '';
			foreach ($tags as $tag) 
			{
				$article_tags .= $tag['title'] . ', ';
			}
		}
		else
		{
			$article_tags = '';
		}
		
		$data['article_tags'] = rtrim($article_tags, ', ');
		$data['article'] = $this->article->get_detail($id, 1);

		if (!empty($data['article']))
		{
			$this->load->view('frontend/article_detail_view', $data);
		}
		else
		{
			redirect(base_url());		
		}

	}

	public function ajax_articles($limit = 8)
	{
		$this->load->model('article/article_model', 'article');

		$get_lang_id = $this->get_lang_id();
		$this->article->lang_id = $get_lang_id;

		if(isset($_POST['page']) && $_POST['page'] != "")
		{
		  	$page = $_POST['page'];
		  	$offset = $limit * ($page-1);
		}
		else
		{
		  	$page = 1;
		  	$offset = 0;
		}

		$total_rows = $this->article->get_total_rows();
		$total_pages = ceil($total_rows/$limit);

		$articles = $this->article->get_list_limit($limit, $offset);
		$base_url = base_url();

		if ($articles)
		{
			$results = "";
			$results .= '<input type="hidden" name="total_pages" id="total_pages" value="' . $total_pages . '">';
	  		$results .= '<input type="hidden" name="page" id="page" value="' . $page . '">';

	  		$count = 0;
			foreach ($articles as $row) 
			{
				$detail_url = seo_url($row['title']) . '-' . $row['content_id'];
				$publish_date = general_date_format($row['publish_date']);
				$title = text_limitation($row['title'], 80);
				$results .= "
						<div class=\"col-lg-3 col-md-6 col-12 float-left\">
							<a href=\"{$base_url}article/detail/{$detail_url}\" class=\"box animated fadeIn delay-{$count}s\">
								<div class=\"date\">{$publish_date}</div>
								<div class=\"title\">{$title}</div>
								<div class=\"author-area\">
									<div class=\"icon-area\"></div>
									<div class=\"text-area\">
										<div class=\"fullname\">{$row['author_fullname']}</div>
										<div class=\"sector\">{$row['author_title']}</div>
									</div>
								</div>
							</a>
						</div>
			            ";
	            $count += 2;
			}

			echo $results;
		}
	}

}


?>