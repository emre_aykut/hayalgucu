var pathArray = window.location.pathname.split( '/' );

if($('#preloader').length > 0) 
{
    if (pathArray[0] != '')
    {
        $(window).load(function() {
            $('#preloader').fadeOut(1000);
        });
    }
    else
    {
        $('#preloader').fadeOut(1000);
    }
}

$(document).ready(function(){

    // main menu (start)
    $('.mb-menu ul li a').removeClass('active');
    $('header right-area menu a').removeClass('active');

    $('.' + pathArray[1] + '-nav').addClass('active');
    // main menu (end)

});


var lastScrollTop = 0;
$(window).scroll(function(event)
{
    var st = $(this).scrollTop();
    
    if (st > lastScrollTop)
    {
        $('header').fadeOut();
        $('.burger-area').fadeOut();
    }
    else
    {
        $('header').css({'background':'#ffffff', 'border-bottom':'1px solid #efefef'});
        $('header').fadeIn();
        $('.burger-menu ~ nav').css({'background':'#ffffff'});
        $('.burger-area').fadeIn();
    }

    lastScrollTop = st;

    if($(window).scrollTop() == 0) 
    {
        $('header').css({'background':'transparent', 'border-bottom':'1px solid #ffffff'});
        $('.burger-menu ~ nav').css({'background':'transparent'});
    }
});






