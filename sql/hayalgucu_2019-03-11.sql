# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.5.38)
# Database: hayalgucu
# Generation Time: 2019-03-11 14:14:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table article_authors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `article_authors`;

CREATE TABLE `article_authors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `article_authors` WRITE;
/*!40000 ALTER TABLE `article_authors` DISABLE KEYS */;

INSERT INTO `article_authors` (`id`, `fullname`, `title`, `photo`, `lang_id`, `content_id`, `created_at`, `updated_at`)
VALUES
	(1,'Aktan Acar','Mimar','',1,1,'2019-03-11 10:03:47',NULL);

/*!40000 ALTER TABLE `article_authors` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table article_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `article_categories`;

CREATE TABLE `article_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table article_content_relations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `article_content_relations`;

CREATE TABLE `article_content_relations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `modul` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `article_content_relations` WRITE;
/*!40000 ALTER TABLE `article_content_relations` DISABLE KEYS */;

INSERT INTO `article_content_relations` (`id`, `modul`, `created_at`, `updated_at`)
VALUES
	(1,'article_author','2019-03-11 10:03:47',NULL),
	(2,'article','2019-03-11 10:04:39','2019-03-11 14:50:31');

/*!40000 ALTER TABLE `article_content_relations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table article_tag_relations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `article_tag_relations`;

CREATE TABLE `article_tag_relations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `article_content_id` int(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `article_tag_relations` WRITE;
/*!40000 ALTER TABLE `article_tag_relations` DISABLE KEYS */;

INSERT INTO `article_tag_relations` (`id`, `tag_id`, `article_id`, `article_content_id`, `lang_id`, `created_at`)
VALUES
	(2,2,1,2,1,'2019-03-11 14:50:31'),
	(3,3,1,2,1,'2019-03-11 14:50:31');

/*!40000 ALTER TABLE `article_tag_relations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table article_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `article_tags`;

CREATE TABLE `article_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `article_tags` WRITE;
/*!40000 ALTER TABLE `article_tags` DISABLE KEYS */;

INSERT INTO `article_tags` (`id`, `title`, `lang_id`, `created_at`)
VALUES
	(1,'sdds',1,'2019-03-11 10:14:44'),
	(2,'hayalgucu',1,'2019-03-11 14:45:22'),
	(3,'makale',1,'2019-03-11 14:50:31');

/*!40000 ALTER TABLE `article_tags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table articles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `detail` text,
  `link` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `common_img` tinyint(1) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;

INSERT INTO `articles` (`id`, `title`, `description`, `detail`, `link`, `photo`, `publish_date`, `author_id`, `category_id`, `status`, `common_img`, `lang_id`, `content_id`, `created_at`, `updated_at`)
VALUES
	(1,'İNSAN ZİHNİ DIŞINDA BİR ESTETİK ARAYIŞI ÜZERİNE…','Bir “bilme” biçiminin kapsamını, koşullarını ve olanakları araştıran ESTETİK için, insanın doğası ve evrenin yapısı ilgili her biri ön kabullere dayanan ayrı ayrı birçok tanım yapılabilir. Ancak olası tüm tanımlar için geçerli olabilecek bazı soruların va','<p>Bu sorular arasında belki en temel olanı, bu bilme biçimini neden anlamaya çalıştığımızdır. Felsefenin insan türünün yeryüzündeki varoluşu ve yokoluşu ile ilgili arketipik korku/kaygılarını anlamaya ve bunlara çare bulmaya aracılık ettiği söylenebilir. Estetik’i ise felsefe içindeki yeri ve işlevi açısından ele almakta fayda vardır. Bu işlevin, Estetik bir İşlev olmadığının da altını çizmek gerekir. Farklı Estetik tanımları, kapsamları ve süreçleri içinde bir takım kültürel, sanatsal, politik veya psikolojik işlevlerin varlığını içerebilirler.Yukarıda belirtilen işlev ile, bir alan olarak Estetik’i kurmak ve araştırmak üzerine bir kavrama işaret edildiği hatırlanmalıdır. Bu anlamda önemli başlıklardan bir tanesi, bu bilmenin tekrar edilebilirliğinin değil, gerçekleştiğinde tanınabilirliğinin sağlanmasıdır. Yani Estetik, araştırdığı bilme biçimine dair koşul ve ilkeleri tanımlamaya ve açıklamaya çalışırken, bir başkası tarafından tekrar edilebilmesinin olanağını yaratmaya çalışmamaktadır. Geçerli bir bilme hali olarak gerçekleştiğinde tanıyabilmenin olanaklarını araştırmaktadır. Bu “tanıma” eylemi, “bilme”yi araştırmanın işlevi kadar önemli ve onunla ilişkili bir başka soruyu daha gündeme getirmektedir. Tanıyan ve bunu ifade edebilen insan dışında bir Estetik mümkün müdür? Zira Estetik araştırma, yukarıda da ifade edildiği gibi, geçerli bir “bilme” ve “ifadeyi” tanımayı olanaklı kılmanın yollarını aramaktadır. Bu olanak ortadan kalktığında bir Estetik sözkonusu olabilir mi? Estetik araştırmanın işlevi ile “tanıma”nın olanaksızlığı sorununun ilişkisi bu noktada, akıllı varlıkların deneyim, eylem, söylem, üretimleri dışındaki alanlarda Estetik’i araştırmanın geçerliliğini tartışmaya açmaktadır.<br>\r\n                            <br>\r\n Aslında sözü edilen durum, sadece Estetik’in araştırdığı geçerli bir bilmenin gerçekleşmesini ve onun dışsallaştırılmasını tanımayı aşan, bu gerçekleşmede insanın kendi varlığını, duyulur dünyanın sağladığı veriler ve akıl yürütmeyle ulaştığı bilgiler dışında bir deneyimde, tanımasıdır. Peki insan “kendini” nasıl tanır? Kendisi olduğu bilmesini sağlayacak veriyi nereden alır? Bu noktada felsefeye geri dönmek ve insanın doğası ve evrenin yapısı ilgili ön kabullere dayanan çerçevede cevapları aramaktan başka çare yoktur. Bilim ve teknoloji sayesinde insanın tüm biyolojik ve psikolojik süreçleri, nörokimyasal ilişkilerle çözümlenmeye, hangi nöronların hangi tür veriyi sakladığına ve hangi koşullarda geri çağırdığına dair malumatımız hızla artıyor. Genetik yapımız, bilişsel süreçlerimiz, bu ikisine bağlı olarak tutumlarımızın, yatkınlıklarımızın haritaları ortaya çıkıyor. Bu koşullarda, Estetik’in araştırdığı “bilme”nin önemi giderek artıyor. İnsanın, niceliklerinin toplamından fazla (her ne kadar Antik Yunan’dan bu yana bilinse de ancak Gestalt Görsel Algı Kuramı ile kendine çağdaş bir kimlik edinmeyi başarabilen bir görüş olarak) bir bütün olduğunu bilmeye karşı duyduğu ihtiyaç, Estetik’in vaad ettikleri ile biraz olsun avunma fırsatı buluyor. Kısacası, Estetik bilme ve tanıma, insan zihninin, varlığını kurarken atfettiği, tanımladığı niteliklerdir. Bu nitelikleri kendisinde araştırması da varoluşu ile ilgili sorularına cevap arama sürecidir. Bu nitelikleri ve araştırmayı, sınırları genişleterek sürdürmesi felsefi bir zorunluluk olabilir. Kendi beden ve zihnine atfettiği değerlerin sınırlarını da, doğa ile ilişkisi üzerinden genişletmesi anlaşılabilir. Böylece bir anlam veya zihin olarak varlığının yansımaları ile kendini aramayı, buldukça da haklılığının hazzı ile varoluşunu sürdürmeyi başaracaktır. Ancak bunun dışında, doğanın kendi ilişkileri içinde bir bilme ve tanıma olanağı araştırmak, doğaya insani nitelikleri atamak, kendi yansımaları ile kendi kendini aldatmaktan başka sonuç vermeyecektir.</p>\r\n','insan-zihni-disinda-bir-estetik-arayisi-uzerine','','2019-03-11 12:04:00',1,0,1,0,1,2,'2019-03-11 10:04:39','2019-03-11 14:50:31');

/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ci_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table content_relations_for_small_modules
# ------------------------------------------------------------

DROP TABLE IF EXISTS `content_relations_for_small_modules`;

CREATE TABLE `content_relations_for_small_modules` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `modul` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `content_relations_for_small_modules` WRITE;
/*!40000 ALTER TABLE `content_relations_for_small_modules` DISABLE KEYS */;

INSERT INTO `content_relations_for_small_modules` (`id`, `modul`, `created_at`, `updated_at`)
VALUES
	(1,'slider','2019-03-11 09:17:27','2019-03-11 09:18:36'),
	(2,'team','2019-03-11 09:41:28',NULL);

/*!40000 ALTER TABLE `content_relations_for_small_modules` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table languages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(5) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `default_lang` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;

INSERT INTO `languages` (`id`, `name`, `code`, `photo`, `status`, `default_lang`, `created_at`, `updated_at`)
VALUES
	(1,'Türkçe','tr',NULL,1,1,NULL,NULL),
	(2,'İngilizce','en',NULL,0,0,NULL,NULL);

/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table page_content_relations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `page_content_relations`;

CREATE TABLE `page_content_relations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `modul` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `page_content_relations` WRITE;
/*!40000 ALTER TABLE `page_content_relations` DISABLE KEYS */;

INSERT INTO `page_content_relations` (`id`, `modul`, `created_at`, `updated_at`)
VALUES
	(1,'page',NULL,NULL),
	(2,'page','2019-03-11 12:12:31','2019-03-11 14:00:52'),
	(3,'page','2019-03-11 12:15:30','2019-03-11 13:59:14'),
	(4,'page','2019-03-11 12:16:10','2019-03-11 12:17:07'),
	(5,'page','2019-03-11 12:16:23','2019-03-11 12:17:11'),
	(6,'page','2019-03-11 12:16:35','2019-03-11 14:53:58'),
	(7,'page','2019-03-11 12:16:52','2019-03-11 14:55:42');

/*!40000 ALTER TABLE `page_content_relations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `detail` text,
  `photo` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `sort` int(11) DEFAULT NULL,
  `default_page` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `parent_id`, `title`, `link`, `description`, `detail`, `photo`, `meta_title`, `meta_keywords`, `meta_description`, `sort`, `default_page`, `status`, `content_id`, `lang_id`, `created_at`, `updated_at`)
VALUES
	(1,0,'Sayfa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,1,1,1,NULL,NULL),
	(2,0,'Page',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,1,1,2,NULL,NULL),
	(3,1,'HAKKIMIZDA','about','Değişik coğrafyalarda farklı ulusal ve uluslar arası norm, standart, yasa ve yönetmeliklere hakim, İngilizce, Fransızca, Rusça ve Arapça proje hazırlama ve sunma yeterliliğine, inisiyatif alma, hızlı karar verme ve uygulamaya geçebilme esnekliğine, disipl','Değişik coğrafyalarda farklı ulusal ve uluslar arası norm, standart, yasa ve yönetmeliklere hakim, İngilizce, Fransızca, Rusça ve Arapça proje hazırlama ve sunma yeterliliğine, inisiyatif alma, hızlı karar verme ve uygulamaya geçebilme esnekliğine, disiplinler arası işbirliği ve koordinasyon konusunda deneyime ve güçlü bir akademik – entelektüel arka plana sahip özelleşmiş tasarım ekiplerinin yarattığı sinerji ve motivasyon Hayalgücü Tasarım’ın ayırt edici özellikleri olarak sıralanabilir.','6ad80b3c40512acb5e6f24c202dfbc20_-_about-img_svg.svg','Hakkımızda','hakkimizda, hayalgucu','Hayalgucu hakkımızda sayfası',1,1,1,2,1,'2019-03-11 12:12:31','2019-03-11 14:00:52'),
	(4,1,'PROJELER','project','','','','Projeler','','',2,1,1,3,1,'2019-03-11 12:15:30','2019-03-11 13:59:14'),
	(5,1,'TAKIM','team','','','','Takım','','',3,1,1,4,1,'2019-03-11 12:16:10','2019-03-11 12:17:07'),
	(6,1,'MAKALELER','article','','','','Makaleler','','',4,1,1,5,1,'2019-03-11 12:16:23','2019-03-11 12:17:11'),
	(7,1,'KARİYER','career','','Değişik coğrafyalarda farklı ulusal ve uluslar arası norm, standart, yasa ve yönetmeliklere hakim, İngilizce, Fransızca, Rusça ve Arapça proje hazırlama ve sunma yeterliliğine, inisiyatif alma, hızlı karar verme ve uygulamaya geçebilme esnekliğine, disiplinler arası işbirliği ve koordinasyon konusunda deneyime ve güçlü bir akademik','','Kariyer','','',5,1,1,6,1,'2019-03-11 12:16:35','2019-03-11 14:53:58'),
	(8,1,'İLETİŞİM','contact','Ofis','Ümit Mahallesi 2546. Sokak No:21 Ümitköy - Ankara - Türkiye<br>\r\n+90 312 466 15 98 - 99<br>\r\ninfo@hayalgucu.com','','İletişim','','',6,1,1,7,1,'2019-03-11 12:16:52','2019-03-11 14:55:42');

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table popups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `popups`;

CREATE TABLE `popups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `refresh_time` int(11) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `finishDate` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `which_page` varchar(55) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table project_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_categories`;

CREATE TABLE `project_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `project_categories` WRITE;
/*!40000 ALTER TABLE `project_categories` DISABLE KEYS */;

INSERT INTO `project_categories` (`id`, `title`, `photo`, `lang_id`, `content_id`, `created_at`, `updated_at`)
VALUES
	(1,'Kentsel Tasarım + Dönüşüm','f8a0de192642a7e27040a4445185851e_-_projeler_otel-konaklama_svg.svg',1,7,'2019-03-11 12:04:01','2019-03-11 12:31:41');

/*!40000 ALTER TABLE `project_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table project_content_relations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_content_relations`;

CREATE TABLE `project_content_relations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `modul` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `project_content_relations` WRITE;
/*!40000 ALTER TABLE `project_content_relations` DISABLE KEYS */;

INSERT INTO `project_content_relations` (`id`, `modul`, `created_at`, `updated_at`)
VALUES
	(1,'project_category','2019-03-11 10:36:40',NULL),
	(2,'project_category','2019-03-11 10:38:07',NULL),
	(3,'project_category','2019-03-11 10:39:45',NULL),
	(4,'project_category','2019-03-11 10:42:03',NULL),
	(5,'project_category','2019-03-11 10:43:09',NULL),
	(6,'project_category','2019-03-11 10:43:52',NULL),
	(7,'project_category','2019-03-11 12:04:01','2019-03-11 12:31:41'),
	(8,'project','2019-03-11 12:06:10','2019-03-11 12:34:17');

/*!40000 ALTER TABLE `project_content_relations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table project_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_images`;

CREATE TABLE `project_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `project_content_id` int(11) DEFAULT NULL,
  `sort` varchar(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `project_images` WRITE;
/*!40000 ALTER TABLE `project_images` DISABLE KEYS */;

INSERT INTO `project_images` (`id`, `image`, `title`, `project_id`, `project_content_id`, `sort`, `lang_id`, `created_at`)
VALUES
	(6,'0dd00e33b6fc67b811ebe3177217d6c0_-_chicago_jpg.jpg','',1,8,'',1,'2019-03-11 12:34:17'),
	(7,'69c203a86f9e43bd85b9cb1664bf3fb7_-_la_jpg.jpg','',1,8,'',1,'2019-03-11 12:34:17');

/*!40000 ALTER TABLE `project_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table projects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `detail` text,
  `default_photo` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `common_img` tinyint(1) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;

INSERT INTO `projects` (`id`, `title`, `link`, `description`, `detail`, `default_photo`, `status`, `sort`, `meta_title`, `meta_description`, `meta_keywords`, `common_img`, `category_id`, `lang_id`, `content_id`, `created_at`, `updated_at`)
VALUES
	(1,'SELÇUKLU ESKİ SANAYİ KENTSEL DÖNÜŞÜM PROJESİ','selcuklu-eski-sanayi-kentsel-donusum-projesi','','<p>Değişik coğrafyalarda farklı ulusal ve uluslar arası norm, standart, yasa ve yönetmeliklere hakim, İngilizce, Fransızca, Rusça ve Arapça proje hazırlama ve sunma yeterliliğine, inisiyatif alma, hızlı karar verme ve uygulamaya geçebilme esnekliğine, disiplinler arası işbirliği ve koordinasyon konusunda deneyime ve güçlü bir akademik – entelektüel arka plana sahip özelleşmiş tasarım ekiplerinin yarattığı sinerji ve motivasyon Hayalgücü Tasarım’ın ayırt edici özellikleri olarak sıralanabilir.</p>\r\n','',1,1,'SELÇUKLU ESKİ SANAYİ KENTSEL DÖNÜŞÜM PROJESİ','Değişik coğrafyalarda farklı ulusal ve uluslar arası norm, standart, yasa ve yönetmeliklere hakim','kentsel dönüşüm, dönüşüm',0,7,1,8,'2019-03-11 12:06:10','2019-03-11 12:34:17');

/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sliders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sliders`;

CREATE TABLE `sliders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `sort` varchar(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sliders` WRITE;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;

INSERT INTO `sliders` (`id`, `title`, `link`, `sort`, `photo`, `content_id`, `lang_id`, `created_at`, `updated_at`)
VALUES
	(1,'Hayalgücü Tasarım Mimari Proje Sergisi ODTÜ’de','','1','b35e6d513c830c7794e2e359e4b0cf05_-_slider_jpg.jpg',1,1,'2019-03-11 09:17:27','2019-03-11 09:18:36');

/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sort` varchar(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;

INSERT INTO `teams` (`id`, `fullname`, `title`, `sort`, `photo`, `content_id`, `lang_id`, `created_at`, `updated_at`)
VALUES
	(1,'İLHAN KESMEZ','Kurucu, Mimar','1','',2,1,'2019-03-11 09:41:28',NULL);

/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `email`, `fullname`, `photo`, `status`, `is_admin`, `created_at`, `updated_at`)
VALUES
	(1,'admin','202cb962ac59075b964b07152d234b70','admin@admin.com','Admin','',1,1,'2017-11-28 14:38:48','2017-12-04 12:18:57');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Dumping routines (PROCEDURE) for database 'hayalgucu'
--
DELIMITER ;;

# Dump of PROCEDURE blog_count
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `blog_count` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `blog_count`(out total int)
BEGIN

Select count(*) as blog_count from blogs;

END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE blog_total
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `blog_total` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `blog_total`(out total int)
BEGIN

Select count(*) as blog_total from blogs;

END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE product_total
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `product_total` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `product_total`(out product_total int)
BEGIN

SELECT COUNT(*) FROM products;

END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE slider_total
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `slider_total` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `slider_total`(out total int)
BEGIN

Select count(*) as slider_total from sliders;

END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
DELIMITER ;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
